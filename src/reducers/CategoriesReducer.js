const initState = {
  categoriesSearch: '',
  loading: false,
  categoriesList: [],
};

export default (CategoriesReducer = (state = initState, action) => {
  switch (action.type) {
    case 'getCategoriesSuccess':
      return {
        ...state,
        loading: true,
        categoriesList: [],
      };
    case 'searchCategoriesTextBox':
      return {
        ...state,
        categoriesSearch: action.payload,
      };
    case 'searchCategories':
      return {
        ...state,
        categoriesList: action.payload,
      };
    case 'saveCategoriesData':
      return {
        ...state,
        loading: false,
        categoriesList: action.payload,
      };
    case 'getCategoriesFailure':
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
});
