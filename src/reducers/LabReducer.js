const initState = {
  labSearch: '',
  loading: false,
  labList: null,
};

export default (LabReducer = (state = initState, action) => {
  switch (action.type) {
    case 'getLabSuccess':
      return {
        ...state,
        loading: true,
      };
    case 'searchLabTextBox':
      return {
        ...state,
        labSearch: action.payload,
      };
    case 'searchLab':
      return {
        ...state,
        labList: action.payload,
      };
    case 'saveLabData':
      return {
        ...state,
        loading: false,
        labList: action.payload,
      };
    case 'getLabFailure':
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
});
