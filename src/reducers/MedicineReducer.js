const initState = {
  medicineSearch: '',
};

export default (medicineSearchReducer = (state = initState, action) => {
  switch (action.type) {
    case 'searchMedicineTextBox':
      return {
        ...state,
        medicineSearch: action.payload,
      };
    default:
      return state;
  }
});
