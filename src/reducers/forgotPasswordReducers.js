const initialState = {
  email: '',
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'emailForgotTextBox':
      return {...state, email: action.payload};
    case 'saveForgotDataRequest':
      return {...state, loading: true};
    case 'saveForgotDataSuccess':
      return {...state, loading: false, email: ''};
    case 'saveForgotDataError':
      return {...state, loading: false};
    default:
      return state;
  }
};
