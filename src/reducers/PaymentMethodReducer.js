const initialState = {
  checked1: false,
  checked2: false,
  checked3: false,
  email: '',
  emailEditValue: false,
  emailLoading: false,
  locationError: false,
  loading: false,
  esewaCheckoutData: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case METHOD_SELECTION:
      return {
        ...state,
        checked1: action.payload.checked1,
        checked2: action.payload.checked2,
        checked3: action.payload.checked3,
      };
    case LOCATION_ERROR_STATUS:
      return {...state, locationError: action.payload};
    case CREATE_ORDER_REQUEST:
      return {...state, loading: true};
    case CREATE_ORDER_SUCCESS:
      return {...state, loading: false};
    case CREATE_ORDER_FAILURE_ERROR:
      return {...state, loading: false};
    case PAYMENT_METHOD_LOADING_STATE:
      return {...state, loading: action.payload};
    default:
      return state;
  }
};
