const initialState = {
  email: '',
  password: '',
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'emailLoginTextBox':
      return {...state, email: action.payload};
    case 'passwordLoginTextBox':
      return {...state, password: action.payload};
    case 'saveLoginDataRequest':
      return {...state, loading: true};
    case 'saveLoginDataSuccessError':
      return {...state, loading: false};
    default:
      return state;
  }
};
