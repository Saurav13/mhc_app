const initialState = {
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'fetchAllSplashDataRequest':
      return {...state, loading: true};
    case 'fetchAllSplashDataSuccess':
      return {...state, loading: false};
    case 'fetchAllSplashDataError':
      return {...state, loading: false};
    default:
      return state;
  }
};
