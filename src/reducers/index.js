import {combineReducers} from 'redux';

import Login from './loginReducers';
import Signup from './signupReducers';
import DrawerContent from './drawerContentReducers';
import Shop from './shopReducers';
import SplashScreen from './splashScreenReducers';
import CategoriesReducer from './CategoriesReducer';
import ProductsReducer from './ProductsReducer';
import CartReducer from './CartReducer';
import LabReducer from './LabReducer';
import PaynowReducer from './PaynowReducer';
import AddLocationMapReducer from './AddLocationMapReducer';
import ForgotPasswordReducers from './forgotPasswordReducers';
import MedicineReducer from './MedicineReducer';
import MedicineSearchReducer from './MediceSearchReducer';

export default combineReducers({
  login: Login,
  signup: Signup,
  drawerContent: DrawerContent,
  shop: Shop,
  splashScreen: SplashScreen,
  categories: CategoriesReducer,
  products: ProductsReducer,
  cart: CartReducer,
  labs: LabReducer,
  paynow: PaynowReducer,
  addLocationmap: AddLocationMapReducer,
  forgotPassword: ForgotPasswordReducers,
  medicine: MedicineReducer,
  medicineSearch: MedicineSearchReducer,
});
