const initialState = {
  name: '',
  dob: '',
  gender: [true, false, false],
  phone: '',
  address: '',
  email: '',
  password: '',
  confirmPassword: '',
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'NameSignupTextBox':
      return {...state, name: action.payload};
    case 'genderSignupTextBox':
      return {...state, gender: action.payload};
    case 'addressSignupTextBox':
      return {...state, address: action.payload};
    case 'dobSignupTextBox':
      return {...state, dob: action.payload};
    case 'phoneSignupTextBox':
      return {...state, phone: action.payload};
    case 'emailSignupTextBox':
      return {...state, email: action.payload};
    case 'passwordSignupTextBox':
      return {...state, password: action.payload};
    case 'confirmPasswordSignupTextBox':
      return {...state, confirmPassword: action.payload};
    case 'saveSignupData':
      return {...state, loading: false};
    case 'saveSignupDataRequest':
      return {...state, loading: true};
    case 'saveSignupDataSuccess':
      return {
        ...state,
        loading: false,
        name: '',
        dob: '',
        gender: [true, false, false],
        phone: '',
        address: '',
        email: '',
        password: '',
        confirmPassword: '',
      };
    case 'saveSignupDataError':
      return {...state, loading: false};
    default:
      return state;
  }
};
