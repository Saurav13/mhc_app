const initState = {
  productsSearch: '',
  loading: false,
  productsList: '',
  searchResult: [],
  searchLoading: false,
};

export default (ProductsReducer = (state = initState, action) => {
  switch (action.type) {
    case 'getProductsRequest':
      return {
        ...state,
        loading: true,
        productsList: [],
      };
    case 'getProductsSuccess':
      return {
        ...state,
        loading: false,
        productsList: action.payload,
      };
    case 'getProductsError':
      return {
        ...state,
        loading: false,
      };
    case 'searchProductsTextBox':
      return {
        ...state,
        productsSearch: action.payload,
      };
    case 'searchProductsRequest':
      return {
        ...state,
        searchLoading: true,
        searchResult: [],
      };
    case 'searchProductsSuccess':
      return {
        ...state,
        searchLoading: false,
        searchResult: action.payload,
      };
    case 'searchProductsError':
      return {
        ...state,
        searchLoading: false,
      };

    default:
      return state;
  }
});
