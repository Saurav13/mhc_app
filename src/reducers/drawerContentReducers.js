const initialState = {
  logoutDialog: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'logoutDialog':
      return {...state, logoutDialog: action.payload};
    default:
      return state;
  }
};
