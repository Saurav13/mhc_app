const initialState = {
  searchedText: '',
  loading: false,
  refreshing: false,
  searchLoading: false,
  searchResult: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'searchTextbox':
      return {...state, searchedText: action.payload};
    case 'shopDataRequest':
      return {...state, refreshing: true};
    case 'shopDataSuccess':
      return {...state, loading: false, refreshing: false};
    case 'shopDataError':
      return {...state, loading: false, refreshing: false};
    case 'searchProductsRequest':
      return {...state, searchLoading: true, searchResult: []};
    case 'searchProductsSuccess':
      return {...state, searchLoading: false, searchResult: action.payload};
    case 'searchProductsError':
      return {...state, searchLoading: false};
    case 'clearSearch':
      return {...state, searchedText: '', searchResult: []};
    default:
      return state;
  }
};
