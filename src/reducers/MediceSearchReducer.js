const initState = {
  medicineList: [],
  medicineSearch: '',
  loading: false,
};

export default (medicineSearchReducer = (state = initState, action) => {
  switch (action.type) {
    case 'searchMedicineTextBox':
      return {
        ...state,
        medicineSearch: action.payload,
      };
    case 'getMedicineSuccess':
      return {
        ...state,
        loading: true,
        medicineList: [],
      };
    case 'getMedicineFailure':
      return {
        ...state,
        loading: false,
      };
    case 'searchMedicine':
      return {
        ...state,
        medicineList: action.payload,
        loading: false,
      };
    default:
      return state;
  }
});
