const initialState = {
  loading: false,
  userSelectedLocation: '',
  locationDataList: [],
  createLocationDialog: false,
  selectedLocationName: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'fetchLocationNameRequest':
      return {...state, loading: true};
    case 'addSingleLocation':
      return {
        ...state,
        userSelectedLocation: action.payload,
      };
    case 'fetchLocationNameSuccess':
      return {
        ...state,
        loading: false,
        selectedLocationName: action.payload,
        createLocationDialog: true,
      };
    case 'getLocationData':
      return {
        ...state,
        loading: false,
        locationDataList: action.payload,
      };
    case 'fetchLocationNameFailureError':
      return {...state, loading: false};
    case 'createLocationDataRequest':
      return {...state, loading: true};
    case 'createLocationDataSuccess':
      return {
        ...state,
        loading: false,

        createLocationDialog: true,
      };
    case 'createLocationDataFailureError':
      return {...state, loading: false};
    case 'createLocationDialog':
      return {...state, createLocationDialog: action.payload};

    default:
      return state;
  }
};
