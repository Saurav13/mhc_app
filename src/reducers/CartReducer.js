const initState = {
  cartList: [],
  cartCount: 0,
  loading: false,
};

export default (CartReducer = (state = initState, action) => {
  switch (action.type) {
    case 'addToCart':
      return {
        ...state,
        cartList: action.payload,
        cartCount: action.payload.length !== null ? action.payload.length : 0,
      };

    case 'getCartData':
      return {
        ...state,
        cartList: action.payload,
        cartCount: action.payload.length !== null ? action.payload.length : 0,
      };
    case 'removeFromCart':
      return {
        ...state,
        cartList: action.payload,
        cartCount: action.payload.length !== null ? action.payload.length : 0,
      };
    case 'paynowRequest':
      return {
        ...state,
        loading: true,
      };
    case 'paynowSuccess':
      return {
        ...state,
        loading: false,
      };
    case 'paynowFailed':
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
});
