const initState = {
  loading: false,
};

export default (PaynowReducer = (state = initState, action) => {
  switch (action.type) {
    case 'createOrderRequest':
      return {
        ...state,
        loading: true,
      };
    case 'createOrderSuccess':
      return {
        ...state,
        loading: false,
      };
    case 'createOrderFailed':
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
});
