import Axios from 'axios';
import AlertToast from '../components/alertToast';

export const searchLabTextBox = search => {
  return {
    type: 'searchLabTextBox',
    payload: search,
  };
};
export const getLabSuccess = () => dispatch => {
  dispatch({
    type: 'getLabSuccess',
  });
};

export const getLabFailure = () => dispatch => {
  dispatch({
    type: 'getLabFailure',
  });
};

export const saveLabData = () => dispatch => {
  dispatch(getLabSuccess());
  Axios({
    method: 'get',
    url: `${global.host}/lab/home`,
  })
    .then(response => {
      dispatch({
        type: 'saveLabData',
        payload: response.data,
      });
    })
    .catch(error => {
      dispatch(getLabFailure());
      AlertToast(error.response.data.message);
    });
};
