import axios from 'axios';
import AlertToast from '../components/alertToast';
import NavigationService from '../route/navigationService';

export const NameSignupTextBox = value => {
  return {
    type: 'NameSignupTextBox',
    payload: value,
  };
};
export const genderSignupTextBox = value => {
  return {
    type: 'genderSignupTextBox',
    payload: value,
  };
};
export const addressSignupTextBox = value => {
  return {
    type: 'addressSignupTextBox',
    payload: value,
  };
};
export const dobSignupTextBox = value => {
  return {
    type: 'dobSignupTextBox',
    payload: value,
  };
};

export const phoneSignupTextBox = value => {
  return {
    type: 'phoneSignupTextBox',
    payload: value,
  };
};

export const emailSignupTextBox = value => {
  return {
    type: 'emailSignupTextBox',
    payload: value,
  };
};

export const passwordSignupTextBox = value => {
  return {
    type: 'passwordSignupTextBox',
    payload: value,
  };
};

export const confirmPasswordSignupTextBox = value => {
  return {
    type: 'confirmPasswordSignupTextBox',
    payload: value,
  };
};

export const saveSignupDataRequest = () => {
  return {
    type: 'saveSignupDataRequest',
  };
};

export const saveSignupDataSuccess = () => {
  return {
    type: 'saveSignupDataSuccess',
  };
};

export const saveSignupDataError = () => {
  return {
    type: 'saveSignupDataError',
  };
};

export function saveSignupData(
  name,
  dob,
  gender,
  phone,
  address,
  email,
  password,
  confirmPassword,
) {
  return function action(dispatch) {
    dispatch(saveSignupDataRequest());

    const formData = new FormData();
    formData.append('name', name);
    formData.append('dob', dob);
    formData.append('gender', gender);
    formData.append('phone', phone);
    formData.append('address', address);
    formData.append('email', email);
    formData.append('password', password);
    formData.append('password_confirmation', confirmPassword);

    console.log(formData);

    const request = axios({
      method: 'POST',
      url: global.host + '/register',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      data: formData,
    });

    return request.then(
      response => {
        console.log(response);
        if (response.status == 201) {
          AlertToast(response.data.message);
          dispatch(saveSignupDataSuccess());

          NavigationService.navigate('Login');
        } else {
          AlertToast("Error: Couldn't connect with the server.");
          dispatch(saveSignupDataError());
        }
      },
      error => {
        console.log(error);
        dispatch(saveSignupDataError());
        if (JSON.parse(error.request._response).error) {
          AlertToast(JSON.parse(error.request._response).error);
        } else {
          AlertToast(error.toString());
        }
      },
    );
  };
}
