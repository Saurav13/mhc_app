import Axios from 'axios';
import {Alert, ToastAndroid, Platform} from 'react-native';
import AlertToast from '../components/alertToast';

export const searchCategoriesTextBox = search => {
  return {
    type: 'searchCategoriesTextBox',
    payload: search,
  };
};
export const getCategoriesSuccess = () => dispatch => {
  dispatch({
    type: 'getCategoriesSuccess',
  });
};

export const getCategoriesFailure = () => dispatch => {
  dispatch({
    type: 'getCategoriesFailure',
  });
};

export const searchCategories = categoryText => dispatch => {
  Axios({
    method: 'get',
    url: `${global.host}/search/${categoryText}`,
  })
    .then(response => {
      dispatch({
        type: 'searchCategories',
        payload: response.data,
      });
    })
    .catch(error => {
      dispatch(getCategoriesFailure());
      AlertToast(error.response.data.message);
    });
};

export const saveCategoriesData = slugId => dispatch => {
  dispatch(getCategoriesSuccess());
  Axios({
    method: 'get',
    url: `${global.host}/ecommerce/category/${slugId}`,
  })
    .then(response => {
      dispatch({
        type: 'saveCategoriesData',
        payload: response.data,
      });
    })
    .catch(error => {
      dispatch(getCategoriesFailure());
      AlertToast(error.response.data.message);
    });
};
