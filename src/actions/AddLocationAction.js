import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import NavigationService from '../route/navigationService';
import AlertToast from '../components/alertToast';

export const fetchLocationNameRequest = () => {
  return {
    type: 'fetchLocationNameRequest',
  };
};
export const fetchLocationNameSuccess = selectedLocationName => {
  return {
    type: 'fetchLocationNameSuccess',
    payload: selectedLocationName,
  };
};
export const fetchLocationNameFailureError = () => {
  return {
    type: 'fetchLocationNameFailureError',
  };
};

export const createLocationDataRequest = () => {
  return {
    type: 'createLocationDataRequest',
  };
};
export const createLocationDataSuccess = () => {
  return {
    type: 'createLocationDataSuccess',
  };
};
export const createLocationDataFailureError = () => {
  return {
    type: 'createLocationDataFailureError',
  };
};

export const createLocationDialog = status => {
  return {
    type: 'createLocationDialog',
    payload: status,
  };
};

export function fetchLocationName(latitude, longitude) {
  return function action(dispatch, getState) {
    dispatch(fetchLocationNameRequest());

    fetch(
      'https://maps.googleapis.com/maps/api/geocode/json?address=' +
        latitude +
        ',' +
        longitude +
        '&key=' +
        global.googleApiKey,
    )
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.results) {
          var postalCode = '';
          var addAddress = 0;
          var nepalTrue = false;
          var selectedLocationName = '';

          for (
            var i = responseJson.results[0].address_components.length - 1;
            i >= 0;
            i--
          ) {
            ++addAddress;

            if (
              responseJson.results[0].address_components[i].long_name == 'Nepal'
            ) {
              nepalTrue = true;
              addAddress = 50;
            } else if (
              responseJson.results[0].address_components[i].types ==
              'postal_code'
            ) {
              postalCode =
                responseJson.results[0].address_components[i].long_name;
            }

            if (addAddress >= 52) {
              selectedLocationName =
                responseJson.results[0].address_components[i].long_name +
                ', ' +
                selectedLocationName;
            }
          }

          selectedLocationName = selectedLocationName.slice(
            0,
            selectedLocationName.length - 2,
          );

          if (!nepalTrue) {
            selectedLocationName = responseJson.results[0].formatted_address.replace(
              postalCode,
              '',
            );
          }

          selectedLocationName = selectedLocationName
            .split(',')
            .filter(function(item, j, allItems) {
              return j == allItems.indexOf(item);
            })
            .join(', ');

          dispatch(fetchLocationNameSuccess(selectedLocationName));
        } else {
          AlertToast('Error: ' + responseJson.message);

          dispatch(fetchLocationNameFailureError());
        }
      });
  };
}

export function postLocationData(
  title,
  name,
  mobile,
  email,
  locationName,
  latitude,
  longitude,
) {
  return function action(dispatch, getState) {
    const userLocationObj = {
      id: Math.floor(Math.random() * 1000 + 1),
      title,
      name,
      mobile,
      email,
      locationName,
      latitude,
      longitude,
    };

    global.userLocation.push(userLocationObj);

    dispatch(addSingleLocation(userLocationObj));

    AsyncStorage.setItem(
      'userLocationInfo',
      JSON.stringify(global.userLocation),
    );

    NavigationService.navigate('Cart');
  };
}

//get localstoragecart location data

export const getLocationData = locationData => {
  return {
    type: 'getLocationData',
    payload: locationData,
  };
};

export const addSingleLocation = selectedData => {
  return {
    type: 'addSingleLocation',
    payload: selectedData,
  };
};
