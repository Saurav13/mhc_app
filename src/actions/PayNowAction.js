import Axios from 'axios';
import AlertToast from '../components/alertToast';
import NavigationService from '../route/navigationService';

export const createOrderRequest = () => dispatch => {
  dispatch({
    type: 'createOrderRequest',
  });
};
export const createOrderSuccess = () => dispatch => {
  dispatch({
    type: 'createOrderSuccess',
  });
};
export const createOrderFailed = () => dispatch => {
  dispatch({
    type: 'createOrderFailed',
  });
};

export const createOrder = orderId => dispatch => {
  dispatch(createOrderRequest());
  const request = Axios({
    method: 'POST',
    url: global.host + `/checkout/${orderId}/gateway/cod`,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });

  return request.then(
    response => {
      console.log(response.status);
      dispatch(createOrderSuccess());

      if (response.status == 200) {
        NavigationService.navigate('OrderScreen');

        dispatch({
          type: 'createOrderSuccess',
          payload: response.data,
        });
      } else {
        AlertToast("Error: Couldn't connect with the server.");
      }
    },
    error => {
      console.log(error);
      AlertToast(error.toString());
      dispatch(createOrderFailed());
    },
  );
};
