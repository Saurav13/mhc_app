export const logoutDialog = status => {
  return {
    type: 'logoutDialog',
    payload: status,
  };
};
