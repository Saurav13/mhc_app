import axios from 'axios';
import AlertToast from '../components/alertToast';
import NavigationService from '../route/navigationService';

export const emailForgotTextBox = value => {
  return {
    type: 'emailForgotTextBox',
    payload: value,
  };
};

export const saveForgotDataRequest = () => {
  return {
    type: 'saveForgotDataRequest',
  };
};

export const saveForgotDataSuccess = () => {
  return {
    type: 'saveForgotDataSuccess',
  };
};

export const saveForgotDataError = () => {
  return {
    type: 'saveForgotDataError',
  };
};

export function saveForgotData(email) {
  return function action(dispatch) {
    dispatch(saveForgotDataRequest());

    const request = axios({
      method: 'POST',
      url: global.host + '/password/reset',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      data: {
        email: email,
      },
    });

    return request.then(
      response => {
        console.log(response);
        if (response.status == 201) {
          AlertToast(response.data.message);
          dispatch(saveForgotDataSuccess());
        } else {
          AlertToast("Error: Couldn't connect with the server.");
          dispatch(saveForgotDataError());
        }
      },
      error => {
        console.log(error);
        dispatch(saveForgotDataError());
        if (JSON.parse(error.request._response).error) {
          AlertToast(JSON.parse(error.request._response).error);
        } else {
          AlertToast(error.toString());
        }
      },
    );
  };
}
