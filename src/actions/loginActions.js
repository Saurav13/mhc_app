import AsyncStorage from '@react-native-community/async-storage';
import AlertToast from '../components/alertToast';
import axios from 'axios';

import NavigationService from '../route/navigationService';

function showToastMessage(message) {
  if (Platform.OS === 'android') {
    ToastAndroid.showWithGravityAndOffset(
      message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      0,
      50,
    );
  } else {
    Alert.alert(message);
  }
}

export const emailLoginTextBox = value => {
  return {
    type: 'emailLoginTextBox',
    payload: value,
  };
};

export const passwordLoginTextBox = userPassword => {
  return {
    type: 'passwordLoginTextBox',
    payload: userPassword,
  };
};

export const saveLoginDataRequest = () => {
  return {
    type: 'saveLoginDataRequest',
  };
};

export const saveLoginDataSuccessError = () => {
  return {
    type: 'saveLoginDataSuccessError',
  };
};

export function saveLoginData(email, password) {
  return function action(dispatch) {
    dispatch(saveLoginDataRequest());

    const request = axios({
      method: 'POST',
      url: global.host + '/login',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      data: {
        email: email,
        password: password,
      },
    });

    return request.then(
      response => {
        dispatch(
          fetchAllSplashData(
            response.data.user,
            response.data.access_token,
            response.data.expires_at,
          ),
        );
      },
      error => {
        dispatch(saveLoginDataSuccessError());
        AlertToast(error.response.data.message);
      },
    );
  };
}

export function fetchAllSplashData(user, accessToken, expiresIn) {
  return function action(dispatch) {
    global.accessToken = accessToken;
    axios.defaults.headers.common.Authorization =
      'Bearer ' + global.accessToken;

    const request = axios({
      method: 'GET',
      url: global.host + '/ecommerce/home',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    return request.then(
      response => {
        if (response.status == 200) {
          global.userInfo = user;
          global.expiresIn = expiresIn;
          global.cartData = [];
          global.userLocation = [];

          AsyncStorage.setItem('KeyUserInfo', JSON.stringify(global.userInfo));
          AsyncStorage.setItem('KeyAccessToken', global.accessToken);
          AsyncStorage.setItem('KeyExpiresIn', global.expiresIn);
          AsyncStorage.setItem('cart', JSON.stringify(global.cartData));
          AsyncStorage.setItem(
            'userLocationInfo',
            JSON.stringify(global.userLocation),
          );

          global.homeData = response.data;
          NavigationService.navigateReset('AllScreens');

          dispatch(saveLoginDataSuccessError());
        } else {
          AlertToast("Error: Couldn't connect with the server.");

          dispatch(saveLoginDataSuccessError());
        }
      },
      error => {
        dispatch(saveLoginDataSuccessError());
        AlertToast(error.toString());
        ToastAndroid.showWithGravityAndOffset(
          error.toString(),
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          0,
          50,
        );
      },
    );
  };
}
