import Axios from 'axios';
import AlertToast from '../components/alertToast';

export const getProductsRequest = () => {
  return {
    type: 'getProductsRequest',
  };
};

export const getProductsSuccess = data => {
  return {
    type: 'getProductsSuccess',
    payload: data,
  };
};

export const getProductsError = () => {
  return {
    type: 'getProductsError',
  };
};

export const searchProductsRequest = () => {
  return {
    type: 'searchProductsRequest',
  };
};

export const searchProductsSuccess = data => {
  return {
    type: 'searchProductsSuccess',
    payload: data,
  };
};

export const searchProductsError = () => {
  return {
    type: 'searchProductsError',
  };
};

export const searchProductsTextBox = search => {
  return {
    type: 'searchProductsTextBox',
    payload: search,
  };
};

export const getProducts = slugId => dispatch => {
  dispatch(getProductsRequest());
  Axios({
    method: 'get',
    url: `${global.host}/ecommerce/products/${slugId}`,
  })
    .then(response => {
      dispatch(getProductsSuccess(response.data));
    })
    .catch(error => {
      dispatch(getProductsError());
      AlertToast(error.response.data.message);
    });
};

export const searchProducts = productText => dispatch => {
  dispatch(searchProductsRequest());

  Axios({
    method: 'get',
    url: `${global.host}/ecommerce/search/${productText}`,
  })
    .then(response => {
      dispatch(searchProductsSuccess(response.data.sproducts));
    })
    .catch(error => {
      dispatch(searchProductsError());
      AlertToast(error.response.data.message);
    });
};
