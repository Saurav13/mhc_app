import AsyncStorage from '@react-native-community/async-storage';
import AlertToast from '../components/alertToast';
import NavigationService from '../route/navigationService';
import Axios from 'axios';

export const addToCart = (product, cartQuantity) => {
  var cartItemData = {...product, cartQuantity: cartQuantity};

  var itemIndex = global.cartData.findIndex(item => item.id == product.id);

  if (itemIndex == -1) {
    global.cartData.push(cartItemData);
    AlertToast('Successfully added to cart.', 120);
  } else {
    AlertToast('Successfully updated the cart.', 120);
    global.cartData.splice(itemIndex, 1, cartItemData);
  }

  AsyncStorage.setItem('cart', JSON.stringify(global.cartData));
  return {
    type: 'addToCart',
    payload: global.cartData,
  };
};

export const removeFromCart = productId => {
  global.cartData = global.cartData.filter(cd => cd.id != productId);
  AsyncStorage.setItem('cart', JSON.stringify(global.cartData));
  return {
    type: 'removeFromCart',
    payload: global.cartData,
  };
};

export const getCartData = cartData => {
  return {
    type: 'getCartData',
    payload: cartData,
  };
};

export const paynowRequest = () => dispatch => {
  dispatch({
    type: 'paynowRequest',
  });
};
export const paynowSuccess = () => dispatch => {
  dispatch({
    type: 'paynowSuccess',
  });
};
export const paynowFailed = () => dispatch => {
  dispatch({
    type: 'paynowFailed',
  });
};

export const savePaynow = (userData, cartData) => dispatch => {
  dispatch(paynowRequest());

  var cart = {};
  var modified = {};
  for (var i = 0; i < cartData.length; i++) {
    modified[cartData[i].id.toString()] = {};
    modified[cartData[i].id.toString()]['qty'] = cartData[i].cartQuantity;
    modified[cartData[i].id.toString()]['stock'] = cartData[i].sale_stock;
    modified[cartData[i].id.toString()]['price'] = cartData[i].cprice;
    modified[cartData[i].id.toString()]['item'] = cartData[i];
  }
  cart['items'] = modified;

  const request = Axios({
    method: 'POST',
    url: global.host + '/ecommerce/checkout',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    data: {
      cart: JSON.stringify(cart),
      shipping: 'shipto',
      email: global.userInfo.email,
      name: global.userInfo.name,
      phone: global.userInfo.phone,
      address: global.userInfo.address,
      address_type: 'Home',
      customer_country: 'Nepal',
      customer_latlong: global.userInfo.latlong,
      customer_pan_number: global.userInfo.pan_number,
      coupon: '',
      filename: [],
      shipping_email: userData.email,
      shipping_name: userData.name,
      shipping_address: userData.locationName,
      shipping_latlong: {
        lat: userData.latitude.toString(),
        lng: userData.longitude.toString(),
      },
      shipping_address_type: userData.title,
      shipping_country: 'Nepal',
      shipping_pan_number: '',
      order_note: '',
    },
  });

  return request.then(
    response => {
      console.log(response);
      if (response.status == 200) {
        NavigationService.navigate('Checkout', {order: response.data});

        dispatch({
          type: 'paynowSuccess',
        });
      } else {
        AlertToast("Error: Couldn't connect with the server.");
      }
    },
    error => {
      console.log(error);
      dispatch(paynowFailed());
    },
  );
};
