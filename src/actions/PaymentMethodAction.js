import {ToastAndroid, Alert, Platform} from 'react-native';
import axios from 'axios';

import NavigationService from '../route/navigationService';

export const methodSelection = (checked1, checked2, checked3) => {
  return {
    type: METHOD_SELECTION,
    payload: {checked1: checked1, checked2: checked2, checked3: checked3},
  };
};

export const locationErrorStatus = data => {
  return {
    type: LOCATION_ERROR_STATUS,
    payload: data,
  };
};

export const createOrderRequest = () => {
  return {
    type: CREATE_ORDER_REQUEST,
  };
};
export const createOrderSuccess = () => {
  return {
    type: CREATE_ORDER_SUCCESS,
  };
};
export const createOrderFailureError = () => {
  return {
    type: CREATE_ORDER_FAILURE_ERROR,
  };
};

export const loadingState = status => {
  return {
    type: PAYMENT_METHOD_LOADING_STATE,
    payload: status,
  };
};

export function createOrder(paymentMethodId, token, amount) {
  return function action(dispatch) {
    dispatch(createOrderRequest());

    const request = axios({
      method: 'POST',
      url:
        'https://live.sajilomeds.com/api/checkout?shipping_address=' +
        global.userInfo.default_address.id +
        '&payment_method_id=' +
        paymentMethodId +
        '&payment_token=' +
        token +
        '&amount=' +
        amount,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + global.accessToken,
      },
    });

    return request.then(
      response => {
        if (response.data.success) {
          ToastAndroid.showWithGravityAndOffset(
            'Order has been created.',
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            0,
            50,
          );

          dispatch(createOrderSuccess());
          NavigationService.navigate('OrderPlaced');
        } else {
          if (Platform.OS === 'android') {
            ToastAndroid.showWithGravityAndOffset(
              'Error: ' + response.data.message,
              ToastAndroid.LONG,
              ToastAndroid.BOTTOM,
              0,
              50,
            );
          } else {
            Alert.alert('Error: ' + response.data.message);
          }

          dispatch(createOrderFailureError());
        }
      },
      error => {
        dispatch(createOrderFailureError());
        ToastAndroid.showWithGravityAndOffset(
          error,
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          0,
          50,
        );
      },
    );
  };
}
