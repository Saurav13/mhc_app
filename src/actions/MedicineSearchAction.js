import Axios from 'axios';

export const getMedicineSuccess = () => dispatch => {
  dispatch({
    type: 'getMedicineSuccess',
  });
};

export const getMedicineFailure = () => dispatch => {
  dispatch({
    type: 'getMedicineFailure',
  });
};

export const searchMedicine = search => dispatch => {
  dispatch(getMedicineSuccess());
  Axios({
    method: 'get',
    url: `${global.host}/ecommerce/medicine-search/${search}`,
  })
    .then(response => {
      console.log(response.data.products);
      dispatch({
        type: 'searchMedicine',
        payload: response.data.products,
      });
    })
    .catch(error => {
      dispatch(getMedicineFailure());
    });
};
