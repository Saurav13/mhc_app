import AlertToast from '../components/alertToast';
import axios from 'axios';

import NavigationService from '../route/navigationService';

export const shopDataRequest = () => {
  return {
    type: 'shopDataRequest',
  };
};

export const shopDataSuccess = data => {
  return {
    type: 'shopDataSuccess',
    payload: data,
  };
};

export const shopDataError = () => {
  return {
    type: 'shopDataError',
  };
};

export const searchProductsRequest = () => {
  return {
    type: 'searchProductsRequest',
  };
};

export const searchProductsSuccess = data => {
  return {
    type: 'searchProductsSuccess',
    payload: data,
  };
};

export const searchProductsError = () => {
  return {
    type: 'searchProductsError',
  };
};

export const searchTextbox = value => {
  return {
    type: 'searchTextbox',
    payload: value,
  };
};

export const clearSearch = () => {
  return {
    type: 'clearSearch',
  };
};

export function fetchShopData() {
  return function action(dispatch) {
    dispatch(shopDataRequest());

    const request = axios({
      method: 'GET',
      url: global.host + '/ecommerce/home',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    return request.then(
      response => {
        console.log(response.data.featured_products[0]);

        global.homeData = response.data;
        dispatch(shopDataSuccess());
      },
      error => {
        dispatch(shopDataError());
        AlertToast(error.toString());
      },
    );
  };
}

export function searchProducts(searchedText) {
  return function action(dispatch) {
    dispatch(searchProductsRequest());

    const request = axios({
      method: 'GET',
      url: 'https://merohealthcare.com/api/ecommerce/search/' + searchedText,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    return request.then(
      response => {
        if (response.status == 200) {
          dispatch(searchProductsSuccess(response.data.sproducts));
        } else {
          AlertToast("Error: Couldn't connect with the server.");
          dispatch(searchProductsError());
        }
      },
      error => {
        console.log(error);
        AlertToast(error.toString());
        dispatch(searchProductsError());
      },
    );
  };
}
