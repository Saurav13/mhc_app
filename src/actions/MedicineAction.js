export const searchMedicineTextBox = search => {
  return {
    type: 'searchMedicineTextBox',
    payload: search,
  };
};
