import AsyncStorage from '@react-native-community/async-storage';
import AlertToast from '../components/alertToast';
import axios from 'axios';

import NavigationService from '../route/navigationService';

export const fetchAllSplashDataRequest = () => {
  return {
    type: 'fetchAllSplashDataRequest',
  };
};

export const fetchAllSplashDataSuccess = () => {
  return {
    type: 'fetchAllSplashDataSuccess',
  };
};

export const fetchAllSplashDataError = () => {
  return {
    type: 'fetchAllSplashDataError',
  };
};

export function fetchAllSplashData() {
  return function action(dispatch) {
    dispatch(fetchAllSplashDataRequest());


    const request = axios({
      method: 'GET',
      url: global.host + '/ecommerce/home',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    return request.then(
      response => {
        console.log(response.data);

        global.homeData = response.data;
        NavigationService.navigateReset('AllScreens');
        dispatch(fetchAllSplashDataSuccess());
      },
      error => {
        dispatch(fetchAllSplashDataError());
        AlertToast(error.toString());
      },
    );
  };
}
