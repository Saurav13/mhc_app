const Images = {
  logo: require('./logo.png'),
  splashMeds: require('./splashMeds.png'),
  slide1: require('./slide1.png'),
  slide2: require('./slide2.png'),
  slide3: require('./slide3.png'),
  slide4: require('./slide4.png'),
  slide5: require('./slide5.png'),
  banner: require('./banner.png'),
  khalti: require('./khalti.png'),
  esewa: require('./esewa.png'),
  cod: require('./cod.png'),
  deliveryMarker: require('./deliveryMarker.png'),
};

export default Images;
