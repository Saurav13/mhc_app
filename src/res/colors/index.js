const colors = {
  grayDarkest: 'rgba(94, 94, 94, 1)',
  grayDark: 'rgba(112, 112, 112, 1)',
  grayMedium: 'rgba(148, 148, 148, 1)',
  grayLight: 'rgba(220, 220, 220, 1))',
  grayLightest: 'rgba(240, 240, 240, 1))',

  white: 'rgba(255, 255, 255, 1)',
  whiteLight: 'rgba(255,255,255,0.5)',
  black: 'rgba(20, 20, 20, 1)',
  border: 'rgba(204,204,204,1)',
  redDark: 'rgba(220,20,60,1)',

  primaryDark: 'rgba(1,56,147,1)',
  primaryMedium: 'rgba(35,133,170,1)',
  primaryLight: 'rgba(152,197,248,1)',
  primaryLightest: 'rgba(203,241,255,1)',

  greenDarkest: 'rgba(0,96,82,1)',
  greenDark: 'rgba(46,162,81,1)',
  greenLight: 'rgba(6,197,101,1)',

  blueLight: 'rgba(164,230,250,1)',

  yellowDark: 'rgba(255,170,0,1)',
  yellowLight: 'rgba(255,228,165,1)',

  secondaryDark: 'rgba(244,72,129,1)',
  secondaryMedium: 'rgba(236,69,79,1)',
};

export default colors;
