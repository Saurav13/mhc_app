'use strict';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator, TransitionPresets} from 'react-navigation-stack';

import SplashScreen from '../screens/splashScreen';
import Login from '../screens/login';
import ForgotPassword from '../screens/forgotPassword';
import Signup from '../screens/signup';
import Walkthrough from '../screens/walkthrough';
import Welcome from '../screens/welcome';
import DashboardRoute from './dashboardRoute';

const AppNavigator = createStackNavigator(
  {
    SplashScreen: {
      screen: SplashScreen,
    },
    Walkthrough: {
      screen: Walkthrough,
    },
    Welcome: {
      screen: Welcome,
    },
    Login: {
      screen: Login,
    },
    ForgotPassword: {
      screen: ForgotPassword,
    },
    Signup: {
      screen: Signup,
    },
    DashboardRoute: {
      screen: DashboardRoute,
    },
  },
  {
    initialRouteName: 'SplashScreen',
    defaultNavigationOptions: {
      ...TransitionPresets.SlideFromRightIOS,
      headerShown: false,
    },
  },
);

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
