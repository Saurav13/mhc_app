'use strict';
import React from 'react';
import {View, Text} from 'react-native';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator, TransitionPresets} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {
  createMaterialTopTabNavigator,
  createBottomTabNavigator,
} from 'react-navigation-tabs';

import Shop from '../screens/shop';
import Category from '../screens/category';
import Products from '../screens/products';
import LocationListScreen from '../screens/LocationListScreen';
import ShopList from '../screens/shop';
import Cart from '../screens/cart';
import Checkout from '../screens/checkout';
import PayNow from '../screens/payNow';
import DrawerContent from '../components/drawerContent';
import LabCategories from '../screens/labCategories';
import LabCart from '../screens/labCart';
import SelectLabTest from '../screens/selectLabTest';
import LabResult from '../screens/labResult';
import Medicine from '../screens/medicine';
import MedicineSearch from '../screens/medicineSearch';
import AddLocation from '../screens/AddLocationMap';
import Colors from '../res/colors';
import Icon from '../res/icons';
import ProductDetails from '../screens/productDetail';
import LabCheckOut from '../screens/labCheckOut';

import SplashScreen from '../screens/splashScreen';
import Login from '../screens/login';
import ForgotPassword from '../screens/forgotPassword';
import Signup from '../screens/signup';
import Walkthrough from '../screens/walkthrough';
import Welcome from '../screens/welcome';

const AuthScreens = createStackNavigator(
  {
    Welcome: {
      screen: Welcome,
    },
    Login: {
      screen: Login,
    },
    ForgotPassword: {
      screen: ForgotPassword,
    },
    Signup: {
      screen: Signup,
    },
  },
  {
    initialRouteName: 'Welcome',
    defaultNavigationOptions: {
      ...TransitionPresets.SlideFromRightIOS,
      headerShown: false,
    },
  },
);

const ShopStackNavigator = createStackNavigator(
  {
    Shop: {screen: Shop},
    Category: {screen: Category},
    Products: {screen: Products},
    LabCategories: {screen: LabCategories},
    LabResult: {screen: LabResult},
    SelectLabTest: {screen: SelectLabTest},
    Medicine: {screen: Medicine},
  },
  {
    initialRouteName: 'Shop',
    defaultNavigationOptions: {
      ...TransitionPresets.SlideFromRightIOS,
      headerShown: false,
    },
  },
);
// const MedicineStackNavigator = createStackNavigator(
//   {
//     Shop: {
//       screen: Shop,
//       navigationOptions: {
//         headerShown: false,
//       },
//     },
//     CombinedList: {
//       screen: ShopList,
//       navigationOptions: {
//         headerShown: false,
//       },
//     },
//   },
//   {
//     initialRouteName: 'Shop',
//     defaultNavigationOptions: {
//       ...TransitionPresets.RevealFromBottomAndroid,
//     },
//   },
// );
// const MeaningStackNavigator = createStackNavigator(
//   {
//     Shop: {
//       screen: Shop,
//       navigationOptions: {
//         headerShown: false,
//       },
//     },
//   },
//   {
//     initialRouteName: 'Shop',
//   },
// );

const MedicineStackNavigator = createStackNavigator(
  {
    Medicine: {screen: Medicine},
    MedicineSearch: {screen: MedicineSearch},
  },
  {
    initialRouteName: 'Medicine',
    defaultNavigationOptions: {
      ...TransitionPresets.SlideFromRightIOS,
      headerShown: false,
    },
  },
);

// const LabStackNavigator = createStackNavigator(
//   {
//     LabCategories: {
//       screen: LabCategories,
//       navigationOptions: {
//         headerShown: false,
//       },
//     },
//     LabResult: {
//       screen: LabResult,
//       navigationOptions: {
//         headerShown: false,
//       },
//     },
//     SelectLabTest: {
//       screen: SelectLabTest,
//       navigationOptions: {
//         headerShown: false,
//       },
//     },
//   },
//   {
//     initialRouteName: 'LabCategories',
//   },
// );

const DashboardTabScreen = createBottomTabNavigator(
  {
    ShopStack: {
      screen: ShopStackNavigator,
      navigationOptions: {
        tabBarIcon: ({focused, tintColor}) => {
          var color = '';
          if (focused) {
            color = Colors.white;
          } else {
            color = Colors.whiteLight;
          }
          return <Icon.FontAwesome5 name="store-alt" size={18} color={color} />;
        },
        tabBarLabel: ({tintColor, focused}) => {
          return (
            <View>
              <Text
                style={{
                  fontFamily: 'Nunito-Regular',
                  color: `${focused ? Colors.white : Colors.whiteLight}`,
                  alignSelf: 'center',
                  paddingBottom: 5,
                  fontSize: 10,
                  letterSpacing: 0.5,
                }}>
                SHOP
              </Text>
            </View>
          );
        },
      },
    },
    MedicineStack: {
      screen: MedicineStackNavigator,
      navigationOptions: {
        tabBarIcon: ({focused, tintColor}) => {
          var color = '';
          if (focused) {
            color = Colors.white;
          } else {
            color = Colors.whiteLight;
          }
          return <Icon.FontAwesome5 name="pills" size={18} color={color} />;
        },
        tabBarLabel: ({tintColor, focused}) => {
          return (
            <View>
              <Text
                style={{
                  fontFamily: 'Nunito-Regular',
                  color: `${focused ? Colors.white : Colors.whiteLight}`,
                  alignSelf: 'center',
                  paddingBottom: 5,
                  fontSize: 10,
                  letterSpacing: 0.5,
                }}>
                MEDICINE
              </Text>
            </View>
          );
        },
      },
    },
    // MeaningStackNavigator: {
    //   screen: MeaningStackNavigator,
    //   navigationOptions: {
    //     tabBarIcon: ({ focused, tintColor }) => {
    //       var color = '';
    //       if (focused) {
    //         color = Colors.white;
    //       } else {
    //         color = Colors.whiteLight;
    //       }
    //       return (
    //         <Icon.MaterialIcons name="library-books" size={18} color={color} />
    //       );
    //     },
    //     tabBarLabel: ({ tintColor, focused }) => {
    //       return (
    //         <View>
    //           <Text
    //             style={{
    //               fontFamily: 'Nunito-Regular',
    //               color: `${focused ? Colors.white : Colors.whiteLight}`,
    //               alignSelf: 'center',
    //               paddingBottom: 5,
    //               fontSize: 10,
    //               letterSpacing: 0.5,
    //             }}>
    //             APPOINTMENT
    //           </Text>
    //         </View>
    //       );
    //     },
    //   },
    // },
    // LabStackNavigator: {
    //   screen: LabStackNavigator,
    //   navigationOptions: {
    //     tabBarIcon: ({ focused, tintColor }) => {
    //       var color = '';
    //       if (focused) {
    //         color = Colors.white;
    //       } else {
    //         color = Colors.whiteLight;
    //       }
    //       return (
    //         <Icon.Entypo name="lab-flask" size={18} color={color} />
    //       );
    //     },
    //     tabBarLabel: ({ tintColor, focused }) => {
    //       return (
    //         <View>
    //           <Text
    //             style={{
    //               fontFamily: 'Nunito-Regular',
    //               color: `${focused ? Colors.white : Colors.whiteLight}`,
    //               alignSelf: 'center',
    //               paddingBottom: 5,
    //               fontSize: 10,
    //               letterSpacing: 0.5,
    //             }}>
    //             LAB
    //           </Text>
    //         </View>
    //       );
    //     },
    //   },
    // },
  },
  {
    swipeEnabled: true,
    animationEnabled: true,
    initialRouteName: 'ShopStack',
    defaultNavigationOptions: {
      ...TransitionPresets.SlideFromRightIOS,
    },
    backBehavior: 'initialRoute',
    tabBarOptions: {
      keyboardHidesTabBar: true,
      activeTintColor: Colors.white,
      activeBackgroundColor: 'transparent',
      allowFontScaling: true,
      style: {
        backgroundColor: Colors.primaryMedium,
        height: 55,
        borderTopWidth: 1,
        borderTopColor: Colors.border,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderTopColor: 'transparent',
      },
      showIcon: true,
    },
  },
);

const DashboardTabsSearchNavigator = createStackNavigator(
  {
    DashboardScreen: {
      screen: DashboardTabScreen,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    initialRouteName: 'DashboardScreen',
    defaultNavigationOptions: {
      ...TransitionPresets.RevealFromBottomAndroid,
    },
  },
);

const DrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: DashboardTabsSearchNavigator,
      navigationOptions: {
        drawerLabel: 'Home',
      },
    },
    Cart: {
      screen: Cart,
      navigationOptions: {
        drawerLabel: 'My Cart',
      },
    },
    CombineScreen: {
      screen: DashboardTabsSearchNavigator,
      navigationOptions: {
        drawerLabel: 'My Orders',
      },
    },
    // MeaningScreen: {
    //   screen: DashboardTabsSearchNavigator,
    //   navigationOptions: {
    //     drawerLabel: 'My Prescriptions',
    //   },
    // },
    // SavedItemsScreen: {
    //   screen: DashboardTabsSearchNavigator,
    //   navigationOptions: {
    //     drawerLabel: 'Ask Doctor',
    //   },
    // },
    // MyFamilyScreen: {
    //   screen: DashboardTabsSearchNavigator,
    //   navigationOptions: {
    //     drawerLabel: 'My Family',
    //   },
    // },
    // MyAppointmentsScreen: {
    //   screen: DashboardTabsSearchNavigator,
    //   navigationOptions: {
    //     drawerLabel: 'My Appointments',
    //   },
    // },
    // MyLabTestScreen: {
    //   screen: DashboardTabsSearchNavigator,
    //   navigationOptions: {
    //     drawerLabel: 'My Lab Test',
    //   },
    // },
    SettingsScreen: {
      screen: DashboardTabsSearchNavigator,
      navigationOptions: {
        drawerLabel: 'Settings',
      },
    },
  },

  {
    contentComponent: DrawerContent,
    initialRouteName: 'Home',
    edgeWidth: 50,
    drawerWidth: '80%',
    drawerPosition: 'left',
    gesturesEnabled: false,
    headerMode: 'float',
    contentOptions: {
      activeBackgroundColor: Colors.white,
      itemsContainerStyle: {
        paddingTop: 0,
      },
    },
  },
);

const EntireStackNavigator = createStackNavigator(
  {
    SplashScreen: {screen: SplashScreen},
    Walkthrough: {screen: Walkthrough},
    AllScreens: {screen: DrawerNavigator},
    Cart: {screen: Cart},
    LocationList: {screen: LocationListScreen},
    Checkout: {screen: Checkout},
    PayNow: {screen: PayNow},
    ProductDetail: {screen: ProductDetails},
    LabCheckOut: {screen: LabCheckOut},
    LabCart: {screen: LabCart},
    Map: {screen: AddLocation},
    AuthScreens: {screen: AuthScreens},
  },
  {
    initialRouteName: 'SplashScreen',
    defaultNavigationOptions: {
      ...TransitionPresets.SlideFromRightIOS,
      headerShown: false,
    },
  },
);

ShopStackNavigator.navigationOptions = ({navigation}) => {
  var {routeName} = navigation.state.routes[navigation.state.index];
  var navigationOptions = {};
  if (
    routeName === 'Category' ||
    routeName === 'Products' ||
    routeName === 'LabCategories' ||
    routeName === 'LabResult' ||
    routeName === 'SelectLabTest'
  ) {
    navigationOptions.tabBarVisible = false;
  }
  return navigationOptions;
};

MedicineStackNavigator.navigationOptions = ({navigation}) => {
  var {routeName} = navigation.state.routes[navigation.state.index];
  var navigationOptions = {};
  if (routeName === 'MedicineSearch') {
    navigationOptions.tabBarVisible = false;
  }
  return navigationOptions;
};

EntireStackNavigator.navigationOptions = ({navigation}) => {
  var {routeName} = navigation.state.routes[navigation.state.index];
  var navigationOptions = {};
  if (routeName === 'Map') {
    navigationOptions.tabBarVisible = false;
  }
  return navigationOptions;
};

const AppContainer = createAppContainer(EntireStackNavigator);

export default AppContainer;
