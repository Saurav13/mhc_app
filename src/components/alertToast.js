import React from 'react';
import {ToastAndroid, Alert, Platform} from 'react-native';

export default function showMessage(message, bottomMargin) {
  if (Platform.OS === 'android') {
    ToastAndroid.showWithGravityAndOffset(
      message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      0,
      bottomMargin ? bottomMargin : 50,
    );
  } else {
    Alert.alert(message);
  }
}
