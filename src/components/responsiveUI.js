import {Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

const guidelineBaseWidth = 360;
const guidelineBaseHeight = 760;

const scaleHorizontal = width / guidelineBaseWidth;
const scaleVertical = height / guidelineBaseHeight;
const scaleAvg = (scaleHorizontal + scaleVertical) / 2;
const scaledWidth = size => (width / guidelineBaseWidth) * size;
const scaledHeight = size => (height / guidelineBaseHeight) * size;
const moderateScale = (size, factor) =>
  size + (scaledWidth(size) - size) * factor;

export {
  width,
  height,
  scaleHorizontal,
  scaleVertical,
  scaleAvg,
  scaledWidth,
  scaledHeight,
  moderateScale,
};
