import React from 'react';
import {ToastAndroid, Alert, Platform} from 'react-native';
import Moment from 'moment';

export default function showMessage(saleTo) {
  var duration = '';
  var diff = '';
  if (saleTo) {
    diff = Moment(saleTo, 'YYYY/MM/DD hh:mm:ss A').diff(Moment(new Date()));

    var d = Moment.duration(diff);
    var days = Math.floor(d.asDays());
    if (days.toString().length != 2) {
      days = '0' + days;
    }

    var hours = Math.floor(d.asHours() - days * 24);
    if (hours.toString().length != 2) {
      hours = '0' + hours;
    }

    var duration =
      days + ' : ' + hours + ' : ' + Moment.utc(diff).format('mm : ss');
  } else {
    duration = 0;
  }

  return duration;
}
