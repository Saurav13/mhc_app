import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  ToastAndroid,
  Alert,
} from 'react-native';

import NavigationService from '../route/navigationService';
import Colors from '../res/colors';
import Icons from '../res/icons';
import Images from '../res/images';

class HeaderAuth extends React.Component {
  render() {
    return (
      <View
        style={{
          backgroundColor: Colors.primaryMedium,
          height: 170,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        {!this.props.noBack && (
          <TouchableOpacity
            activeOpacity={0.75}
            onPress={() => {
              NavigationService.back();
            }}
            style={{width: 80, paddingHorizontal: 28, paddingVertical: 35}}>
            <Icons.Ionicons
              name="md-arrow-round-back"
              size={26}
              color={Colors.white}
            />
          </TouchableOpacity>
        )}
        {!this.props.noLogo && (
          <Image
            style={styles.logo}
            source={Images.logo}
            resizeMode="contain"
          />
        )}
        <View style={{width: 80}} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    width: 140,
    height: 80,
    alignSelf: 'center',
    marginBottom: 24,
  },
});

export default HeaderAuth;
