import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Animated,
  TouchableOpacity,
  FlatList,
  Image,
} from 'react-native';
import {TextField} from 'react-native-material-textfield';
import {BarIndicator} from 'react-native-indicators';

import {connect} from 'react-redux';
import {
  fetchShopData,
  searchTextbox,
  searchProducts,
  clearSearch,
} from '../actions/shopActions';
import {calculatePricePercentage} from '../utlis/calculatePricePercentage';
import {addToCart} from '../actions/CartAction';

import Colors from '../res/colors';
import Icons from '../res/icons';
import Images from '../res/images';
import NavigationService from '../route/navigationService';
import CustomButton from '../components/button';

class SearchProductsDropdown extends Component {
  render() {
    const {placeholder, style} = this.props;
    return (
      <View>
        <View
          style={{
            borderTopLeftRadius: 25,
            borderTopRightRadius: 25,
            marginTop: -20,
            backgroundColor: Colors.white,
          }}>
          <View
            style={[
              {
                height: 48,
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor: Colors.grayLightest,
                borderRadius: 20,
                marginVertical: 16,
                marginHorizontal: 16,
                paddingHorizontal: 16,
              },
              style,
            ]}>
            <Icons.FontAwesome
              name="search"
              size={18}
              color={Colors.grayMedium}
            />

            <TextInput
              placeholder={placeholder || 'Search medicine, healthcare'}
              placeholderTextColor={Colors.grayMedium}
              style={styles.searchTextInput}
              value={this.props.shop.searchedText}
              onChangeText={text => {
                this.props.searchTextbox(text);
              }}
              onSubmitEditing={() => {
                this.props.searchProducts(this.props.shop.searchedText);
              }}
              keyboardType="default"
              autoFocus={false}
              autoCapitalize="none"
              autoCorrect={false}
              returnKeyType="search"
            />

            {this.props.shop.searchedText != '' && (
              <TouchableOpacity
                activeOpacity={0.75}
                onPress={() => {
                  this.props.clearSearch();
                }}
                style={{
                  position: 'absolute',
                  alignItems: 'center',
                  justifyContent: 'center',
                  right: 0,
                  top: 0,
                  bottom: 0,
                  paddingHorizontal: 12,
                }}>
                <Icons.Entypo
                  name="cross"
                  size={20}
                  color={Colors.redDark}
                  style={styles.icon}
                />
              </TouchableOpacity>
            )}
          </View>
        </View>
        <View
          style={{
            maxHeight: global.screenHeight - 250,
            marginHorizontal: 16,
            backgroundColor: Colors.white,
            elevation: 5,
            borderRadius: 8,
            marginTop: -10,
          }}>
          {this.props.shop.searchLoading && (
            <View style={{height: 50}}>
              <BarIndicator color={Colors.black} size={18} count={7} />
            </View>
          )}

          <FlatList
            data={this.props.shop.searchResult}
            contentContainerStyle={{
              paddingHorizontal: 16,
            }}
            renderItem={({item, index}) => {
              return (
                <TouchableOpacity
                  activeOpacity={0.75}
                  onPress={() => {
                    NavigationService.navigate('ProductDetail', {
                      productDetail: item,
                    });
                    this.props.clearSearch();
                  }}
                  style={{
                    flex: 1,
                    marginVertical: 4,
                    backgroundColor: Colors.white,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={{
                        uri: `https://merohealthcare.com/assets/images/${
                          item.photo
                        }`,
                      }}
                      style={{
                        width: 80,
                        height: 80,
                        paddingHorizontal: 12,
                        borderRadius: 12,
                        marginRight: 16,
                      }}
                      resizeMode="contain"
                    />

                    <View style={{flex: 1}}>
                      <Text
                        style={{
                          fontFamily: 'Nunito-SemiBold',
                          fontSize: 12,
                          color: Colors.black,
                        }}>
                        {item.name}
                      </Text>

                      <Text
                        style={{
                          fontFamily: 'Nunito-SemiBold',
                          fontSize: 11,
                          color: Colors.greenDarkest,
                          marginTop: 4,
                        }}>
                        {item.company_name}
                      </Text>
                      <Text
                        numberOfLines={3}
                        style={{
                          fontFamily: 'Nunito-Bold',
                          fontSize: 15,
                          color: Colors.primaryMedium,
                          marginTop: 4,
                        }}>
                        Rs {item.cprice}
                      </Text>
                      <CustomButton.SecondaryButton
                        style={{
                          height: 25,
                          paddingHorizontal: 18,
                          paddingVertical: 7,
                          backgroundColor: Colors.primaryMedium,
                          marginTop: 8,
                          width: 100,
                        }}
                        textStyle={{
                          color: Colors.white,
                          fontFamily: 'Nunito-Regular',
                          fontSize: 10,
                          paddingBottom: 0,
                        }}
                        title="Add to Cart"
                        onPress={() => {
                          this.props.addToCart(item, 1);
                        }}
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      height: 0.5,
                      backgroundColor: Colors.border,
                      marginTop: 12,
                    }}
                  />
                </TouchableOpacity>
              );
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    shop: state.shop,
  };
}

export default connect(
  mapStateToProps,
  {fetchShopData, searchTextbox, searchProducts, clearSearch, addToCart},
)(SearchProductsDropdown);

const styles = StyleSheet.create({
  searchTextInput: {
    flex: 1,
    fontSize: 14,
    color: Colors.black,
    fontFamily: 'Nunito-SemiBold',
    fontWeight: 'normal',
    marginLeft: 8,
  },
});
