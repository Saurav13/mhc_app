import React from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import NavigationService from '../route/navigationService';
import Images from '../res/images';
import Icon from '../res/icons';
import Colors from '../res/colors';
import {connect} from 'react-redux';

class HeaderDashboard extends React.Component {
  render() {
    return (
      <View
        style={{
          height: 130,
          backgroundColor: Colors.primaryMedium,
        }}>
        {this.props.title ? (
          <View
            style={{
              height: 130,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              activeOpacity={0.75}
              onPress={() => {
                NavigationService.back();
              }}
              style={{padding: 16}}>
              <Icon.Ionicons
                name="md-arrow-round-back"
                size={26}
                color={Colors.white}
              />
            </TouchableOpacity>

            <Text
              numberOfLines={2}
              style={{
                fontFamily: 'Nunito-SemiBold',
                fontSize: 20,
                color: Colors.white,
              }}>
              {this.props.title}
            </Text>
            {this.props.noCart ? (
              <View style={{width: 42, height: 42}} />
            ) : (
              <TouchableOpacity
                activeOpacity={0.75}
                onPress={() => {
                  NavigationService.navigate('Cart');
                }}
                style={{paddingHorizontal: 16}}>
                <Icon.FontAwesome5
                  name="shopping-cart"
                  size={22}
                  color={Colors.white}
                />
                <View
                  style={{
                    position: 'absolute',
                    height: 18,
                    width: 18,
                    top: -8,
                    right: 6,
                    borderRadius: 9,
                    backgroundColor: Colors.redDark,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Nunito-Regular',
                      fontSize: 12,
                      color: Colors.white,
                    }}>
                    {global.cartData.length}
                  </Text>
                </View>
              </TouchableOpacity>
            )}
          </View>
        ) : (
          <View
            style={{
              width: '100%',
              height: 130,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              activeOpacity={0.75}
              onPress={() => {
                NavigationService.toggleDrawerBar();
              }}
              style={{paddingHorizontal: 16}}>
              <Icon.SimpleLineIcons
                name="grid"
                size={22}
                color={Colors.white}
              />
            </TouchableOpacity>
            <Image
              style={{width: 110}}
              source={Images.logo}
              resizeMode="contain"
            />
            <TouchableOpacity
              activeOpacity={0.75}
              onPress={() => {
                NavigationService.navigate('Cart');
              }}
              style={{paddingHorizontal: 16}}>
              <Icon.FontAwesome5
                name="shopping-cart"
                size={22}
                color={Colors.white}
              />
              <View
                style={{
                  position: 'absolute',
                  height: 18,
                  width: 18,
                  top: -8,
                  right: 6,
                  borderRadius: 9,
                  backgroundColor: Colors.redDark,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'Nunito-Regular',
                    fontSize: 12,
                    color: Colors.white,
                  }}>
                  {global.cartData.length}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    cartCount: state.cart.cartCount,
  };
};
export default connect(mapStateToProps)(HeaderDashboard);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
});
