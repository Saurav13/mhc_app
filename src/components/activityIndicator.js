import React from 'react';
import {StyleSheet, View} from 'react-native';
import Modal from 'react-native-modal';
import {BarIndicator} from 'react-native-indicators';
import PropTypes from 'prop-types';

import colors from '../res/colors';

class LoadingIndicator extends React.Component {
  render() {
    const {isVisible} = this.props;
    return (
      <Modal
        isVisible={isVisible}
        animationIn={'zoomIn'}
        animationOut={'zoomOut'}
        backdropOpacity={0.5}
        useNativeDriver={true}>
        <View style={styles.loading}>
          <BarIndicator color={colors.white} size={22} count={7} />
        </View>
      </Modal>
    );
  }
}

LoadingIndicator.propTypes = {
  isVisible: PropTypes.bool.isRequired,
};

export default LoadingIndicator;

const styles = StyleSheet.create({
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
