import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, Text, TextInput, StyleSheet, Animated} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';
import {TextField} from 'react-native-material-textfield';

import Colors from '../res/colors';
import Icon from '../res/icons';
import Images from '../res/images';

class MainTextInput extends Component {
  render() {
    const {
      label,
      value,
      onChangeText,
      typePassword,
      keyboardType,
      onSubmitEditing,
      editable,
      disabled,
    } = this.props;
    return (
      <TextField
        label={label}
        fontSize={12}
        labelFontSize={10}
        keyboardType={keyboardType}
        autoCapitalize="none"
        autoCorrect={false}
        secureTextEntry={typePassword}
        tintColor={Colors.primaryMedium}
        baseColor={Colors.grayDark}
        value={value}
        editable={editable}
        lineWidth={1}
        disabled={disabled}
        style={styles.textInput}
        labelTextStyle={{fontFamily: 'Nunito-Regular', paddingTop: 0.5}}
        onChangeText={onChangeText}
        onSubmitEditing={onSubmitEditing}
      />
    );
  }
}

class SecondaryTextInput extends Component {
  render() {
    const {
      label,
      value,
      onChangeText,
      typePassword,
      keyboardType,
      onSubmitEditing,
      editable,
    } = this.props;
    return (
      <TextField
        label={label}
        fontSize={14}
        labelFontSize={13}
        color={Colors.primaryMedium}
        keyboardType={keyboardType}
        autoCapitalize="none"
        autoCorrect={false}
        secureTextEntry={typePassword}
        tintColor={Colors.primaryMedium}
        baseColor={Colors.grayDark}
        value={value}
        editable={editable}
        lineWidth={1}
        style={{
          fontSize: 14,
          fontFamily: 'Nunito-SemiBold',
          color: Colors.grayDarkest,
        }}
        onChangeText={onChangeText}
        onSubmitEditing={onSubmitEditing}
        onFocus={() => {}}
      />
    );
  }
}

class AnimatedTextInput extends Component {
  state = {
    isFocused: false,
    placeholder: this.props.placeholder,
    animation: new Animated.Value(0),
  };

  onFocus = () => {
    this.setState({isFocused: true});
    Animated.timing(this.state.animation, {
      duration: 500,
      toValue: 1,
      useNativeDriver: true,
    }).start();
  };

  onBlur = () => {
    this.setState({isFocused: false});
    if (this.props.value === '') {
      Animated.timing(this.state.animation, {
        duration: 500,
        toValue: 0,
        useNativeDriver: true,
      }).start();
    }
  };
  render() {
    const {value, label, onChangeText, style} = this.props;
    const borderBottomColor = this.state.isFocused
      ? Colors.primaryMedium
      : Colors.grayDarkest;
    const borderBottomWidth = this.state.isFocused ? 2 : 1;
    const viewMarginTop = this.state.isFocused ? 20 : 10;
    const fontStyle = this.state.isFocused
      ? {
          fontFamily: 'Nunito-Bold',
          fontSize: 13,
          color: Colors.black,
        }
      : {fontFamily: 'Nunito-Regular', fontSize: 14, color: Colors.grayDarkest};
    const startingOpacity = this.props.value === '' ? 1 : 0;
    const translateY = this.state.animation.interpolate({
      inputRange: [0, 1],
      outputRange: [10, -10],
    });
    const opacity = this.state.animation.interpolate({
      inputRange: [0, 1],
      outputRange: [startingOpacity, 1],
    });
    return (
      <View
        style={[
          {
            height: 40,
            backgroundColor: Colors.white,
            marginTop: viewMarginTop, // (40-14) / 2 = 13 => (20-13) = 7 ~ 10
            borderBottomColor,
            borderBottomWidth,
          },
          style,
        ]}>
        {this.state.isFocused || (!this.state.isFocused && value === '') ? (
          <Animated.Text
            style={{
              ...fontStyle,
              position: 'absolute',
              opacity,
              transform: [
                {
                  translateY,
                },
              ],
            }}>
            {label}
          </Animated.Text>
        ) : null}

        <TextInput
          style={{
            flex: 1,
            fontSize: 14,
            color: Colors.grayDarkest,
            fontFamily: 'Nunito-Regular',
          }}
          value={value}
          onChangeText={onChangeText}
          keyboardType="default"
          autoFocus={false}
          autoCapitalize="none"
          autoCorrect={false}
          editable={true}
          onFocus={() => {
            this.onFocus();
          }}
          onBlur={() => {
            this.onBlur();
          }}
        />
      </View>
    );
  }
}

class SearchTextInput extends Component {
  render() {
    const {
      value,
      placeholder,
      onChangeText,
      onSubmitEditing,
      style,
      returnKeyType,
      editable,
    } = this.props;
    return (
      <View
        style={[
          {
            height: 48,
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: Colors.grayLightest,
            borderRadius: 20,
            marginTop: 16,
            marginHorizontal: 16,
            paddingHorizontal: 16,
          },
          style,
        ]}>
        <Icon.FontAwesome name="search" size={18} color={Colors.grayMedium} />

        <TextInput
          placeholder={placeholder}
          placeholderTextColor={Colors.grayMedium}
          style={styles.searchTextInput}
          value={value}
          onChangeText={onChangeText}
          onSubmitEditing={onSubmitEditing}
          keyboardType="default"
          autoFocus={false}
          autoCapitalize="none"
          autoCorrect={false}
          editable={editable}
          returnKeyType={returnKeyType}
        />
      </View>
    );
  }
}

class MaskedTextInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      underlineColorAndroid: Colors.border,
    };
  }
  render() {
    const {value, onChangeText, editable} = this.props;
    return (
      <TextInputMask
        placeholder="(___) ___  -  ____"
        keyboardType="number-pad"
        type={'custom'}
        maxLength={16}
        underlineColorAndroid={this.state.underlineColorAndroid}
        placeholderTextColor="rgba(102,118,133,0.5)"
        value={value}
        onChangeText={onChangeText}
        editable={editable}
        style={editable ? styles.mainTextInput : styles.mainTextInputDisabled}
        options={{
          mask: '(999) 999 - 9999',
        }}
        onFocus={() => {
          this.setState({underlineColorAndroid: Colors.primaryDark});
        }}
        onBlur={() => {
          this.setState({underlineColorAndroid: Colors.border});
        }}
      />
    );
  }
}

MainTextInput.propTypes = {
  value: PropTypes.string.isRequired,
  onChangeText: PropTypes.func.isRequired,
  typePassword: PropTypes.bool,
  keyboardType: PropTypes.string,
  autoFocus: PropTypes.bool,
  returnKeyType: PropTypes.string,
  onSubmitEditing: PropTypes.func,
  fieldName: PropTypes.string,
};

MainTextInput.defaultProps = {
  editable: true,
  typePassword: false,
};

MaskedTextInput.propTypes = {
  value: PropTypes.string.isRequired,
  onChangeText: PropTypes.func.isRequired,
  editable: PropTypes.bool.isRequired,
};

MaskedTextInput.defaultProps = {
  editable: true,
};

const styles = StyleSheet.create({
  mainTextInput: {
    fontSize: 12,
    color: Colors.black,
    fontFamily: 'Nunito-Bold',
    marginHorizontal: -4,
    fontWeight: 'normal',
  },
  searchTextInput: {
    flex: 1,
    fontSize: 14,
    color: Colors.black,
    fontFamily: 'Nunito-SemiBold',
    fontWeight: 'normal',
    marginLeft: 8,
  },
  mainTextInputDisabled: {
    fontSize: 12,
    color: 'rgba(102,118,133,1)',
    fontFamily: 'Nunito-Bold',
    marginHorizontal: -4,
    fontWeight: 'normal',
  },
  textInput: {
    fontSize: 12,
    color: Colors.grayDarkest,
    fontFamily: 'Nunito-SemiBold',
    fontWeight: 'normal',
  },
});

export default {
  MainTextInput,
  MaskedTextInput,
  SearchTextInput,
  SecondaryTextInput,
  AnimatedTextInput,
};
