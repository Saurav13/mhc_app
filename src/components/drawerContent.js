'use strict';
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ToastAndroid,
  Platform,
  Alert,
  Image,
} from 'react-native';
import {DrawerItems} from 'react-navigation-drawer';
import AsyncStorage from '@react-native-community/async-storage';

import {connect} from 'react-redux';
import {logoutDialog} from '../actions/drawerContentActions';

import NavigationService from '../route/navigationService';
import AlertModal from '../components/alertModal';
import Colors from '../res/colors';
import Icon from '../res/icons';
import Images from '../res/images';

class DrawerContent extends React.Component {
  nameInitial(text) {
    var names = text.split(' '),
      initials = names[0].substring(0, 1).toUpperCase();

    if (names.length > 0) {
      if (names.length === 1) {
        initials = names[names.length - 1].substring(0, 2).toUpperCase();
      } else {
        initials += names[names.length - 1].substring(0, 1).toUpperCase();
      }
    }
    return initials;
  }
  render() {
    return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
        <AlertModal
          isVisible={this.props.drawerContent.logoutDialog}
          type="logout"
          title="Logout"
          body="Are you sure you want to logout of Mero Healthcare?"
          but1="CANCEL"
          but2="LOGOUT"
          animationIn="zoomIn"
          animationOut="zoomOut"
          but1Action={() => {
            this.props.logoutDialog(false);
          }}
          but2Action={() => {
            this.props.logoutDialog(false);
            global.accessToken = null;
            global.expiresIn = null;
            global.refreshToken = null;
            global.cartData = [];
            global.userLocation = [];
            global.userInfo = {};

            AsyncStorage.clear();

            if (Platform.OS === 'android') {
              ToastAndroid.showWithGravityAndOffset(
                'You are now logged out.',
                ToastAndroid.SHORT,
                ToastAndroid.BOTTOM,
                0,
                50,
              );
            } else {
              Alert.alert('You are now logged out.');
            }

            NavigationService.navigate('AuthScreens');
          }}
        />

        <View style={{flex: 1}}>
          <View
            style={{
              backgroundColor: Colors.primaryMedium,
              height: 130,
              alignItems: 'center',
              justifyContent: 'center',
              paddingTop: 20,
            }}>
            <Image
              style={{width: 115, height: 60, marginBottom: 20}}
              source={Images.logo}
              // resizeMode="contain"
            />
          </View>
          {global.accessToken ? (
            <TouchableOpacity
              onPress={() => {
                NavigationService.toggleDrawerBar();
                NavigationService.navigate('UserProfileScreen');
              }}
              style={{
                backgroundColor: Colors.white,
                marginTop: -20,
                borderRadius: 20,
              }}
              activeOpacity={1}>
              <View
                style={{
                  flexDirection: 'row',
                  paddingVertical: 20,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  borderBottomWidth: 1,
                  borderColor: Colors.border,
                  marginHorizontal: 16,
                }}>
                <View
                  style={{
                    backgroundColor: Colors.primaryMedium,
                    height: 36,
                    width: 36,
                    borderRadius: 8,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Nunito-Bold',
                      fontSize: 15,
                      color: Colors.white,
                      letterSpacing: 0.8,
                    }}>
                    {this.nameInitial(global.userInfo.name)}
                  </Text>
                </View>
                <View style={{flex: 1, marginLeft: 10}}>
                  <Text
                    style={{
                      fontFamily: 'Nunito-Bold',
                      fontSize: 15,
                      color: '#121619',
                    }}>
                    {global.userInfo.name}
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'Nunito-Regular',
                      fontSize: 12,
                      color: '#667685',
                    }}>
                    {global.userInfo.email}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => {
                NavigationService.toggleDrawerBar();
                NavigationService.navigate('AuthScreens');
              }}
              style={{
                backgroundColor: Colors.white,
                marginTop: -20,
                borderRadius: 20,
                padding: 16,
                borderBottomWidth: 1,
                borderColor: Colors.border,
              }}
              activeOpacity={1}>
              <Text
                style={{
                  fontFamily: 'Nunito-Bold',
                  fontSize: 15,
                  color: '#121619',
                }}>
                Login / Signup
              </Text>
            </TouchableOpacity>
          )}

          <DrawerItems
            {...this.props}
            getLabel={scene => {
              if (scene.focused) {
                var color = Colors.primaryDark;
              } else {
                var color = '#667685';
              }

              return (
                <View style={{flex: 1}}>
                  {scene.index === 0 && (
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: 16,
                        paddingVertical: 12,
                      }}>
                      <View style={{width: 20, alignItems: 'center'}}>
                        <Icon.FontAwesome5
                          name="home"
                          size={16}
                          color={
                            scene.focused ? Colors.primaryDark : Colors.grayDark
                          }
                        />
                      </View>
                      <Text
                        style={
                          scene.focused
                            ? styles.buttonFocusedText
                            : styles.buttonText
                        }>
                        {this.props.getLabel(scene)}
                      </Text>
                    </View>
                  )}
                  {scene.index === 1 && (
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: 16,
                        paddingVertical: 12,
                      }}>
                      <View style={{width: 20, alignItems: 'center'}}>
                        <Icon.FontAwesome5
                          name="shopping-cart"
                          size={18}
                          color={
                            scene.focused ? Colors.primaryDark : Colors.grayDark
                          }
                        />
                      </View>
                      <Text
                        style={
                          scene.focused
                            ? styles.buttonFocusedText
                            : styles.buttonText
                        }>
                        {this.props.getLabel(scene)}
                      </Text>
                    </View>
                  )}
                  {scene.index === 2 && (
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: 16,
                        paddingVertical: 12,
                      }}>
                      <View style={{width: 20, alignItems: 'center'}}>
                        <Icon.Foundation
                          name="clipboard-notes"
                          size={18}
                          color={
                            scene.focused ? Colors.primaryDark : Colors.grayDark
                          }
                        />
                      </View>
                      <Text
                        style={
                          scene.focused
                            ? styles.buttonFocusedText
                            : styles.buttonText
                        }>
                        {this.props.getLabel(scene)}
                      </Text>
                    </View>
                  )}
                  {scene.index === 3 && (
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: 16,
                        paddingVertical: 12,
                      }}>
                      <View style={{width: 20, alignItems: 'center'}}>
                        <Icon.MaterialIcons
                          name="settings"
                          size={18}
                          color={
                            scene.focused ? Colors.primaryDark : Colors.grayDark
                          }
                        />
                      </View>
                      <Text
                        style={
                          scene.focused
                            ? styles.buttonFocusedText
                            : styles.buttonText
                        }>
                        {this.props.getLabel(scene)}
                      </Text>
                    </View>
                  )}
                  {scene.index === 4 && (
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: 16,
                        paddingVertical: 12,
                      }}>
                      <View style={{width: 20, alignItems: 'center'}}>
                        <Icon.FontAwesome5
                          name="user-friends"
                          size={16}
                          color={
                            scene.focused ? Colors.primaryDark : Colors.grayDark
                          }
                        />
                      </View>
                      <Text
                        style={
                          scene.focused
                            ? styles.buttonFocusedText
                            : styles.buttonText
                        }>
                        {this.props.getLabel(scene)}
                      </Text>
                    </View>
                  )}
                  {scene.index === 5 && (
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: 16,
                        paddingVertical: 12,
                      }}>
                      <View style={{width: 20, alignItems: 'center'}}>
                        <Icon.SimpleLineIcons
                          name="notebook"
                          size={18}
                          color={
                            scene.focused ? Colors.primaryDark : Colors.grayDark
                          }
                        />
                      </View>
                      <Text
                        style={
                          scene.focused
                            ? styles.buttonFocusedText
                            : styles.buttonText
                        }>
                        {this.props.getLabel(scene)}
                      </Text>
                    </View>
                  )}
                  {scene.index === 6 && (
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: 16,
                        paddingVertical: 12,
                      }}>
                      <View style={{width: 20, alignItems: 'center'}}>
                        <Icon.Fontisto
                          name="laboratory"
                          size={18}
                          color={
                            scene.focused ? Colors.primaryDark : Colors.grayDark
                          }
                        />
                      </View>
                      <Text
                        style={
                          scene.focused
                            ? styles.buttonFocusedText
                            : styles.buttonText
                        }>
                        {this.props.getLabel(scene)}
                      </Text>
                    </View>
                  )}
                  {scene.index === 7 && (
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: 16,
                        paddingVertical: 12,
                      }}>
                      <View style={{width: 20, alignItems: 'center'}}>
                        <Icon.MaterialIcons
                          name="settings"
                          size={18}
                          color={
                            scene.focused ? Colors.primaryDark : Colors.grayDark
                          }
                        />
                      </View>
                      <Text
                        style={
                          scene.focused
                            ? styles.buttonFocusedText
                            : styles.buttonText
                        }>
                        {this.props.getLabel(scene)}
                      </Text>
                    </View>
                  )}
                </View>
              );
            }}
            onItemPress={({route, focused}) => {
              this.props.onItemPress({route, focused});
              if (route.routeName === 'DashboardScreen') {
                NavigationService.navigate('HomeTab');
              } else if (route.routeName === 'LeadsScreen') {
                NavigationService.navigate('LeadsTab');
              } else if (route.routeName === 'PartnersScreen') {
                NavigationService.navigate('PartnersTab');
              } else if (route.routeName === 'CalendarScreen') {
                NavigationService.navigate('CalendarTab');
              } else if (route.routeName === 'SmsScreen') {
                NavigationService.navigate('SmsTab');
              } else if (route.routeName === 'CalculatorScreen') {
                NavigationService.navigate('Calculator');
              } else if (route.routeName === 'UserProfileScreen') {
                NavigationService.navigate('Profile');
              }
            }}
          />
        </View>

        <View style={{justifyContent: 'flex-end'}}>
          <View style={{marginLeft: 16, height: 60, justifyContent: 'center'}}>
            <Text
              style={{
                fontFamily: 'Nunito-Regular',
                color: '#667685',
                fontSize: 8,
              }}>
              APP VERSION
            </Text>
            <Text
              style={{
                fontFamily: 'Nunito-Regular',
                color: 'rgba(18,22,25,1)',
                fontSize: 12,
              }}>
              {global.appVersion}
            </Text>
          </View>
          {global.accessToken && (
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.closeDrawer();
                this.props.logoutDialog(true);
              }}
              activeOpacity={0.7}>
              <View
                style={{
                  height: 60,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  flexDirection: 'row',
                  borderTopWidth: 1,
                  borderColor: '#F0EEEE',
                  paddingHorizontal: 16,
                }}>
                <Text
                  style={{
                    fontFamily: 'Nunito-Regular',
                    color: Colors.grayDark,
                    fontSize: 12,
                  }}>
                  Logout
                </Text>

                <Icon.MaterialCommunityIcons
                  name="logout"
                  size={22}
                  color={Colors.redDark}
                />
              </View>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    drawerContent: state.drawerContent,
  };
}

export default connect(
  mapStateToProps,
  {logoutDialog},
)(DrawerContent);

const styles = StyleSheet.create({
  profileImage: {
    width: 70,
    height: 70,
    borderRadius: 35,
  },
  profileImageEditView: {
    flexGrow: 1,
    width: 70,
    height: 70,
    borderRadius: 35,
    opacity: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.7)',
    width: 70,
    height: 70,
    borderRadius: 35,
  },
  alertTitleStyle: {
    fontFamily: 'Nunito-Bold',
    color: Colors.grayDark,
    textAlign: 'center',
    fontSize: 14,
    marginVertical: 10,
  },
  alertBodyStyle: {
    fontFamily: 'Nunito-SemiBold',
    color: Colors.grayDark,
    textAlign: 'center',
    fontSize: 14,
    marginVertical: 10,
  },
  alertButtonStyle: {
    fontFamily: 'Nunito-SemiBold',
    color: Colors.grayDark,
    textAlign: 'center',
    fontSize: 14,
    marginLeft: 10,
  },
  verticalLineLocationDialogue: {
    height: 35,
    width: 0.5,
    backgroundColor: Colors.grayDark,
    alignSelf: 'center',
  },
  buttonText: {
    fontFamily: 'Nunito-Regular',
    color: '#121619',
    fontSize: 12,
    marginLeft: 17,
  },
  buttonFocusedText: {
    fontFamily: 'Nunito-Regular',
    color: Colors.primaryDark,
    fontSize: 12,
    marginLeft: 17,
  },
  logo: {
    marginTop: 10,
    width: 50,
  },
});
