import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Text, StyleSheet, View} from 'react-native';

import colors from '../res/colors';
import Icon from '../res/icons';

class PrimaryButton extends Component {
  render() {
    const {title, onPress} = this.props;
    return (
      <TouchableOpacity
        onPress={onPress}
        activeOpacity={0.7}
        style={styles.primaryButtonStyle}>
        <Text style={styles.textStyle}>{title}</Text>
        {title == 'Next' && (
          <View style={{marginLeft: 10}}>
            <Icon.FontAwesome
              name="chevron-right"
              size={18}
              color={colors.white}
            />
          </View>
        )}
        {title == 'Finish' && (
          <View style={{marginLeft: 10}}>
            <Icon.MaterialCommunityIcons
              name="check-circle"
              size={20}
              color={colors.white}
            />
          </View>
        )}
      </TouchableOpacity>
    );
  }
}

class SecondaryButton extends Component {
  render() {
    const {title, onPress, style, textStyle} = this.props;
    return (
      <TouchableOpacity
        onPress={onPress}
        activeOpacity={0.7}
        style={[styles.secondaryButtonStyle, style]}>
        <Text style={[styles.textStyle, textStyle]}>{title}</Text>
      </TouchableOpacity>
    );
  }
}

PrimaryButton.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

SecondaryButton.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  textStyle: {
    color: colors.white,
    fontFamily: 'Nunito-SemiBold',
    fontSize: 18,
    paddingBottom: 5,
  },
  primaryButtonStyle: {
    height: 48,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.primaryMedium,
    borderRadius: 24,
    paddingHorizontal: 40,
    elevation: 10,
  },
  secondaryButtonStyle: {
    height: 32,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.primaryMedium,
    borderRadius: 16,
    paddingHorizontal: 30,
    elevation: 4,
  },
});

export default {
  PrimaryButton,
  SecondaryButton,
};
