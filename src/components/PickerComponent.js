import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import colors from '../res/colors';
import Icons from '../res/icons';
import * as Animatable from 'react-native-animatable';

class PickerComponent extends Component {
  static defaultProps = {
    multiple: false,
    data: [],
    selectedData: [],
    selectedText: '',
    show: false,
    showDropdown: () => {},
    hideDropdown: () => {},
    renderItems: () => {},
    placeholder: '',
    deleteSingleItem: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      testList: this.props.selectedData,
    };
  }

  render() {
    return (
      <View>
        <View style={styles.textFieldViewStyle}>
          {this.props.multiple ? (
            this.props.selectedData.length > 0 ? (
              this.props.selectedData.map((select, index) => {
                return (
                  <View
                    key={index}
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'flex-start',
                      paddingHorizontal: 10,
                      marginBottom: 5,
                      backgroundColor: colors.primaryMedium,
                      borderRadius: 10,
                      paddingVertical: 3,
                    }}>
                    <Text numberOfLines={2} style={styles.singleTextStyle}>
                      {select}
                    </Text>
                    <Icons.Entypo
                      name="cross"
                      color={colors.white}
                      size={15}
                      onPress={() => this.props.deleteSingleItem(index)}
                      style={{alignSelf: 'center'}}
                    />
                  </View>
                );
              })
            ) : (
              <Text style={{paddingVertical: 10, color: colors.grayMedium}}>
                Please select text
              </Text>
            )
          ) : (
            <TextInput
              multiline={true}
              style={styles.textInputStyle}
              editable={false}
              placeholder={this.props.placeholder}
              value={this.props.selectedText}
            />
          )}

          {!this.props.show ? (
            <Icons.Entypo
              onPress={this.props.showDropdown}
              name="chevron-down"
              color={colors.grayDarkest}
              style={styles.dropDownIconStyle}
              size={20}
            />
          ) : (
            <Icons.Entypo
              onPress={this.props.hideDropdown}
              name="chevron-up"
              color={colors.grayDarkest}
              style={styles.dropDownIconStyle}
              size={20}
            />
          )}
        </View>
        {this.props.show && (
          <Animatable.View
            animation="fadeInDown"
            duration={1000}
            style={styles.dropDownStyle}>
            <FlatList
              style={{flex: 1}}
              data={this.props.data}
              ListEmptyComponent={() => <Text>No Lists to Render</Text>}
              keyExtractor={(item, index) => index + ''}
              listKey={(item, index) => 'D' + index.toString()}
              renderItem={({item}) => {
                return this.props.renderItems(item);
              }}
            />
          </Animatable.View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textFieldViewStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  textInputStyle: {
    color: colors.black,
    fontFamily: 'Nunito-black',
  },

  singleTextStyle: {
    fontSize: 12,
    color: colors.white,
  },

  dropDownIconStyle: {
    alignSelf: 'center',
  },

  dropDownStyle: {
    flex: 1,
    height: 600,
    backgroundColor: colors.white,
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: colors.grayDarkest,
  },
});

export default PickerComponent;
