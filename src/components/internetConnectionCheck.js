import React from 'react';
import NetInfo from '@react-native-community/netinfo';
import AlertModal from '../components/alertModal';

import {connect} from 'react-redux';
import {fetchAllSplashData} from '../actions/splashScreenActions';

class InternetConnectionCheck extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      internetConnectionAlert: true,
    };
  }

  componentDidMount() {
    NetInfo.addEventListener(this.handleConnectivityChange);
  }

  handleConnectivityChange = state => {
    global.internetConnectionStatus = state.isConnected;
    this.setState({internetConnectionAlert: state.isConnected});
    if (global.spalshScreenNoConnection && state.isConnected) {
      this.props.fetchAllSplashData();
      global.spalshScreenNoConnection = false;
    }
  };

  render() {
    return (
      <AlertModal
        isVisible={!this.state.internetConnectionAlert}
        type="internet"
        title="NO INTERNET"
        body="Sorry, we couldn't find your internet connection"
        but1="CANCEL"
        but2="TRY AGAIN"
        animationIn="zoomIn"
        animationOut="zoomOut"
        but1Action={() => {
          this.setState({internetConnectionAlert: true});
        }}
        but2Action={() => {
          this.setState({internetConnectionAlert: true});
        }}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    splashScreen: state.splashScreen,
  };
}

export default connect(
  mapStateToProps,
  {fetchAllSplashData},
)(InternetConnectionCheck);
