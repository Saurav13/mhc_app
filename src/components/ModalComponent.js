import React, {Component} from 'react';
import Modal from 'react-native-modal';
import Colors from '../res/colors';
import {
  TouchableOpacity,
  TextInput,
  Text,
  View,
  StyleSheet,
} from 'react-native';
class ModalPrimary extends Component {
  render() {
    return (
      <Modal
        animationIn="slideInUp"
        animationInTiming={500}
        animationOut="slideInDown"
        isVisible={this.props.isVisible}
        animationOutTiming={800}
        onBackdropPress={this.props.onBackdropPress}
        style={{
          maxHeight: 300,
          bottom: 0,
          position: 'absolute',
          left: 0,
          width: '90%',
          borderRadius: 20,
          paddingHorizontal: 16,
          paddingVertical: 16,
        }}>
        {this.props.children}
      </Modal>
    );
  }
}

class ModalWithInput extends Component {
  render() {
    return (
      <Modal
        animationIn="slideInUp"
        animationInTiming={500}
        animationOut="slideInDown"
        isVisible={this.props.isVisible}
        animationOutTiming={800}
        onBackdropPress={this.props.onBackdropPress}
        style={{
          maxHeight: 200,
          width: '90%',
          backgroundColor: Colors.white,
          borderRadius: 20,
          paddingHorizontal: 16,
          paddingVertical: 16,
        }}>
        <Text
          style={{
            paddingHorizontal: 16,
            textAlign: 'center',
            fontFamily: 'Nunito-Regular',
            fontSize: 16,
            color: Colors.grayDark,
          }}>
          Enter A Coupon
        </Text>
        <TextInput
          placeholder={this.props.placeholder}
          value={this.props.value}
          onChangeText={this.props.onChangeText}
          keyboardType="default"
          autoFocus={false}
          autoCapitalize="none"
          autoCorrect={false}
          editable={true}
          style={[
            {
              height: 40,
              backgroundColor: Colors.grayLightest,
              borderRadius: 20,
              marginTop: 16,
              marginHorizontal: 16,
              paddingHorizontal: 16,
            },
            this.props.style,
          ]}
        />
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={this.props.onOKPress}
            style={{
              padding: 8,
              borderRadius: 20,
              width: 100,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: Colors.redDark,
            }}>
            <Text
              style={{
                fontFamily: 'Nunito-Black',
                color: Colors.white,
                fontSize: 14,
              }}>
              OK
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={this.props.onCancelPress}
            style={{
              padding: 8,
              borderRadius: 20,
              width: 100,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: Colors.grayDark,
            }}>
            <Text
              style={{
                fontFamily: 'Nunito-Black',
                color: Colors.white,
                fontSize: 14,
              }}>
              Cancel
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: 16,
    marginTop: 14,
  },
});
export default {
  ModalPrimary,
  ModalWithInput,
};
