import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';

import NavigationService from '../route/navigationService';
import Icon from '../res/icons';

class TitleBarView extends React.Component {
  toggleDrawer = () => {
    NavigationService.toggleDrawerBar();
  };

  render() {
    const {title, type, showSave, onBack, save, edit} = this.props;

    return (
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: '#00AEEF',
          height: 52,
          zIndex: 2,
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          {title !== 'Edit Profile' &&
          title !== 'Add Note' &&
          title !== 'Edit Note' &&
          title !== 'Create New Lead' &&
          type !== 'SmsChat' &&
          title !== 'Edit Lead' ? (
            <TouchableOpacity
              onPress={this.toggleDrawer}
              activeOpacity={0.75}
              style={{
                height: 52,
                paddingLeft: 16,
                paddingRight: 20,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Icon.Feather name="menu" size={24} color="black" />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={onBack}
              activeOpacity={0.75}
              style={{
                height: 52,
                paddingLeft: 16,
                paddingRight: 20,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Icon.FontAwesome5 name="arrow-left" size={24} color="black" />
            </TouchableOpacity>
          )}

          <Text
            numberOfLines={1}
            style={{
              fontSize: 18,
              color: 'white',
              fontFamily: 'Nunito-Bold',
            }}>
            {title}
          </Text>
        </View>
        {title !== 'Profile' &&
          title !== 'Edit Profile' &&
          title !== 'Lead Detail' &&
          title !== 'Add Note' &&
          title !== 'Edit Note' &&
          title !== 'Create New Lead' &&
          type !== 'SmsChat' &&
          title !== 'SMS' &&
          title !== 'Edit Lead' && (
            <TouchableOpacity
              onPress={() => NavigationService.navigate('Search')}
              activeOpacity={0.75}
              style={{
                height: 52,
                paddingLeft: 20,
                paddingRight: 16,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Icon.FontAwesome5 name="arrow-left" size={24} color="black" />
            </TouchableOpacity>
          )}

        {(title === 'Profile' || title === 'Lead Detail') && (
          <TouchableOpacity
            onPress={edit}
            activeOpacity={0.75}
            style={{
              height: 52,
              paddingLeft: 20,
              paddingRight: 16,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Icon.FontAwesome5 name="arrow-left" size={24} color="black" />
          </TouchableOpacity>
        )}
        {(title === 'Edit Profile' ||
          title === 'Add Note' ||
          title === 'Edit Note' ||
          title === 'Create New Lead' ||
          title === 'Edit Lead') && [
          showSave ? (
            <TouchableOpacity
              onPress={save}
              activeOpacity={0.75}
              style={{
                height: 52,
                paddingLeft: 20,
                paddingRight: 16,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: 14,
                  color: 'white',
                  fontFamily: 'Nunito-SemiBold',
                }}>
                SAVE
              </Text>
            </TouchableOpacity>
          ) : (
            <Text
              style={{
                fontSize: 14,
                color: 'white',
                fontFamily: 'Nunito-SemiBold',
                paddingLeft: 20,
                paddingRight: 16,
                opacity: 0.5,
                alignSelf: 'center',
              }}>
              SAVE
            </Text>
          ),
        ]}
        {/*    {type === 'SmsChat' && (
          <View
            style={{
              height: 52,
              paddingLeft: 20,
              paddingRight: 16,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <SvgImages.DotMenu />
          </View>
        )} */}
      </View>
    );
  }
}

TitleBarView.propTypes = {
  title: PropTypes.string.isRequired,
  showSave: PropTypes.bool,
  onBack: PropTypes.func,
  save: PropTypes.func,
  edit: PropTypes.func,
};

TitleBarView.defaultProps = {
  save: () => {
    NavigationService.back();
  },
  onBack: () => {
    NavigationService.back();
  },
};

export default TitleBarView;
