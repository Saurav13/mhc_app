export const calculatePricePercentage = (newPrice, oldPrice) => {
  if (oldPrice !== null) {
    if (oldPrice > newPrice) {
      const percentage = ((oldPrice - newPrice) / oldPrice) * 100;
      return percentage.toFixed(0);
    }
  } else {
    return null;
  }
};
