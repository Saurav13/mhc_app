import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  ToastAndroid,
  Alert,
} from 'react-native';

import CustomButton from '../components/button';
import CustomTextInput from '../components/textInput';
import NavigationService from '../route/navigationService';
import LoadingIndicator from '../components/activityIndicator';
import HeaderAuth from '../components/headerAuth';
import AlertToast from '../components/alertToast';

import {connect} from 'react-redux';
import {
  emailForgotTextBox,
  saveForgotData,
} from '../actions/forgotPasswordActions';

import Colors from '../res/colors';
import Icon from '../res/icons';
import Images from '../res/images';

class Login extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <LoadingIndicator isVisible={this.props.forgotPassword.loading} />

        <ScrollView
          keyboardShouldPersistTaps="handled"
          style={{backgroundColor: 'white'}}>
          <HeaderAuth />

          <View
            style={{
              backgroundColor: Colors.white,
              borderTopLeftRadius: 50,
              borderTopRightRadius: 50,
              marginTop: -50,
              paddingHorizontal: 20,
              paddingTop: 32,
            }}>
            <Text
              style={{
                fontFamily: 'Nunito-Bold',
                fontSize: 26,
                color: Colors.blackDark,
                marginVertical: 15,
                textAlign: 'center',
              }}>
              Forgot Password?
            </Text>

            <Text
              style={{
                fontSize: 14,
                color: Colors.grayDark,
                fontFamily: 'Nunito-Regular',
                textAlign: 'center',
                marginTop: 16,
                lineHeight: 20,
              }}>
              Please check the mail. The password reset link would be sent to
              the email adddres.
            </Text>
            <CustomTextInput.MainTextInput
              label="Email"
              value={this.props.forgotPassword.email}
              onChangeText={text => this.props.emailForgotTextBox(text)}
              keyboardType="default"
            />
            <View style={{marginTop: 32, alignItems: 'center'}}>
              <CustomButton.PrimaryButton
                title="Submit"
                onPress={() => {
                  var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                  if (this.props.forgotPassword.email != '') {
                    if (reg.test(this.props.forgotPassword.email) === true) {
                      this.props.saveForgotData(
                        this.props.forgotPassword.email,
                      );
                    } else {
                      AlertToast('Please enter the valid email.');
                    }
                  } else {
                    AlertToast('Please enter the email.');
                  }
                }}
              />
            </View>

            <View>
              <Text
                style={{
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 16,
                  color: Colors.grayDark,
                  marginTop: 30,
                  textAlign: 'center',
                }}>
                Already Have account ?
              </Text>

              <TouchableOpacity
                onPress={() => {
                  NavigationService.navigate('Login');
                }}
                activeOpacity={0.7}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginBottom: 30,
                }}>
                <Text
                  style={{
                    fontFamily: 'Nunito-Bold',
                    fontSize: 16,
                    color: Colors.primaryDark,
                    marginTop: 12,
                    textAlign: 'center',
                  }}>
                  Login Now
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    forgotPassword: state.forgotPassword,
  };
}

export default connect(
  mapStateToProps,
  {
    emailForgotTextBox,
    saveForgotData,
  },
)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo: {
    marginTop: 10,
    width: 140,
    height: 80,
  },
});
