import React from 'react';
import {
  StyleSheet,
  FlatList,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  Platform,
  StatusBar,
  I18nManager,
  Image,
} from 'react-native';

import NavigationService from '../route/navigationService';
import CustomButton from '../components/button';
import Images from '../res/images';
import Colors from '../res/colors';
import Icon from '../res/icons';

const isIphoneX =
  Platform.OS === 'ios' &&
  !Platform.isPad &&
  !Platform.isTVOS &&
  (global.screenHeight === 812 || global.screenWidth === 812);

const isAndroidRTL = I18nManager.isRTL && Platform.OS === 'android';

class Walkthrough extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      width: global.screenWidth,
      height: global.screenHeight,
      activeIndex: 0,
    };

    this.slides = [
      {
        title: 'Online Health Care',
        body:
          'It helps patients to take the appointment and also track details of patient',
      },
      {
        title: 'Clinic Appointment',
        body:
          'It helps patients to take the appointment and also track details of patient',
      },
      {
        title: 'Price Comparision',
        body:
          'It helps to compare the price of same product with the other are offering for it.',
      },
      {
        title: 'Lab Booking',
        body:
          'We assure you the best service on your precious health with best lab equipment through bookings.',
      },
      {
        title: 'Prescriptions Upload',
        body:
          'Patient can upload their medical prescription for their proper guidance for medicines.',
      },
    ];
  }

  goToSlide = pageNum => {
    this.setState({activeIndex: pageNum});
    this.flatList.scrollToOffset({
      animated: true,
      duration: 1000,
      offset: this.rtlSafeIndex(pageNum) * this.state.width,
    });
  };

  renderItem = ({item, index}) => {
    if (index == 0) {
      this.image = Images.slide1;
      this.butText = 'Next';
      this.circle = [Colors.primaryDark, 'white', 'white', 'white', 'white'];
    } else if (index == 1) {
      this.image = Images.slide2;
      this.butText = 'Next';
      this.circle = ['white', Colors.primaryDark, 'white', 'white', 'white'];
    } else if (index == 2) {
      this.image = Images.slide3;
      this.butText = 'Next';
      this.circle = ['white', 'white', Colors.primaryDark, 'white', 'white'];
    } else if (index == 3) {
      this.image = Images.slide4;
      this.butText = 'Next';
      this.circle = ['white', 'white', 'white', Colors.primaryDark, 'white'];
    } else if (index == 4) {
      this.image = Images.slide5;
      this.butText = 'Finish';
      this.circle = ['white', 'white', 'white', 'white', Colors.primaryDark];
    }
    return (
      <View
        style={{
          flex: 1,
          width: global.screenWidth,
          borderRightWidth: 1,
          borderLeftWidth: 1,
          borderColor: Colors.primaryMedium,
        }}>
        <View
          style={{
            backgroundColor: Colors.primaryMedium,
            height: 170,
            alignItems: 'center',
            paddingTop: 20,
          }}>
          <Image
            style={styles.logo}
            source={Images.logo}
            resizeMode="contain"
          />
        </View>
        <View
          style={{
            flex: 1,
            backgroundColor: Colors.white,
            borderTopLeftRadius: 50,
            borderTopRightRadius: 50,
            marginTop: -50,
            paddingHorizontal: 20,
            paddingVertical: 32,
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <Text
            style={{
              fontSize: 26,
              color: Colors.blackDark,
              fontFamily: 'Nunito-Bold',
            }}>
            {item.title}
          </Text>

          <Image
            style={styles.slideImage}
            source={this.image}
            resizeMode="contain"
          />

          <View>
            <Text
              style={{
                fontSize: 16,
                color: Colors.grayDark,
                fontFamily: 'Nunito-SemiBold',
                textAlign: 'center',
                lineHeight: 25,
              }}>
              {item.body}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 25,
              }}>
              <View
                style={{
                  width: 12,
                  height: 12,
                  borderRadius: 6,
                  borderWidth: 1,
                  marginHorizontal: 4,
                  borderColor: Colors.primaryDark,
                  backgroundColor: this.circle[0],
                }}
              />
              <View
                style={{
                  width: 12,
                  height: 12,
                  borderRadius: 6,
                  borderWidth: 1,
                  marginHorizontal: 4,
                  borderColor: Colors.primaryDark,
                  backgroundColor: this.circle[1],
                }}
              />
              <View
                style={{
                  width: 12,
                  height: 12,
                  borderRadius: 6,
                  borderWidth: 1,
                  marginHorizontal: 4,
                  borderColor: Colors.primaryDark,
                  backgroundColor: this.circle[2],
                }}
              />
              <View
                style={{
                  width: 12,
                  height: 12,
                  borderRadius: 6,
                  borderWidth: 1,
                  marginHorizontal: 4,
                  borderColor: Colors.primaryDark,
                  backgroundColor: this.circle[3],
                }}
              />
              <View
                style={{
                  width: 12,
                  height: 12,
                  borderRadius: 6,
                  borderWidth: 1,
                  marginHorizontal: 4,
                  borderColor: Colors.primaryDark,
                  backgroundColor: this.circle[4],
                }}
              />
            </View>
          </View>
          <CustomButton.PrimaryButton
            title={this.butText}
            onPress={() => {
              if (this.state.activeIndex == this.slides.length - 1) {
                NavigationService.navigate('Welcome');
              } else {
                this.goToSlide(this.state.activeIndex + 1);
                this.props.onSlideChange &&
                  this.props.onSlideChange(
                    this.state.activeIndex + 1,
                    this.state.activeIndex,
                  );
              }
            }}
          />

          <TouchableOpacity
            onPress={() => {
              NavigationService.navigate('Welcome');
            }}
            activeOpacity={0.75}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              alignSelf: 'flex-end',
            }}>
            <Text
              style={{
                fontSize: 18,
                color: Colors.primaryDark,
                fontFamily: 'Nunito-SemiBold',
                marginRight: 6,
              }}>
              Skip
            </Text>

            <Icon.FontAwesome5
              name="angle-double-right"
              size={20}
              color={Colors.primaryDark}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  rtlSafeIndex = i => (isAndroidRTL ? this.slides.length - 1 - i : i);

  onMomentumScrollEnd = e => {
    const offset = e.nativeEvent.contentOffset.x;
    // Touching very very quickly and continuous brings about
    // a variation close to - but not quite - the width.
    // That's why we round the number.
    // Also, Android phones and their weird numbers
    const newIndex = this.rtlSafeIndex(Math.round(offset / this.state.width));
    if (newIndex === this.state.activeIndex) {
      // No page change, don't do anything
      return;
    }
    const lastIndex = this.state.activeIndex;
    this.setState({activeIndex: newIndex});
    this.props.onSlideChange && this.props.onSlideChange(newIndex, lastIndex);
  };

  onLayout = () => {
    const {width, height} = Dimensions.get('window');
    if (width !== this.state.width || height !== this.state.height) {
      // Set new width to update rendering of pages
      this.setState({width, height});
      // Set new scroll position
      const func = () => {
        this.flatList.scrollToOffset({
          offset: this.rtlSafeIndex(this.state.activeIndex) * width,
          animated: false,
        });
      };
      Platform.OS === 'android' ? setTimeout(func, 0) : func();
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          ref={ref => (this.flatList = ref)}
          data={this.slides}
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          bounces={false}
          renderItem={this.renderItem}
          onMomentumScrollEnd={this.onMomentumScrollEnd}
          extraData={this.state.width}
          onLayout={this.onLayout}
          keyExtractor={(item, i) => String(i)}
        />
      </View>
    );
  }
}

export default Walkthrough;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo: {
    marginTop: 10,
    width: 140,
    height: 80,
  },
  slideImage: {
    marginVertical: -20,
    width: 500,
    height: 250,
  },
});
