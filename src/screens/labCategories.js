import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import LoadingIndicator from '../components/activityIndicator';
import NavigationService from '../route/navigationService';
import CustomTextInput from '../components/textInput';
import CustomButton from '../components/button';
import PickerComponent from '../components/PickerComponent';
import Images from '../res/images';
import Icon from '../res/icons';
import Colors from '../res/colors';
import Header from '../components/headerDashboard';
import {saveLabData} from '../actions/LabAction';
import {connect} from 'react-redux';
import PickerMultiple from '../components/PickerMultiple';
const categoryItemList = [
  {
    image: require('../res/images/ico_blood.png'),
    cat_name: 'Blood Sugar',
  },
  {
    image: require('../res/images/ico_lipid.png'),
    cat_name: 'Liquid Profile',
  },
  {
    image: require('../res/images/ico_surgery.png'),
    cat_name: 'Liver Profile',
  },
  {
    image: require('../res/images/ico_surgery.png'),
    cat_name: 'Aurveda',
  },
];

const TrusteDiagonsisData = [
  {
    image: require('../res/images/ico_service.png'),
    title: 'Assured Home Service',
    body:
      'Medlife expert technician visits your home to collect blood sample. All samples have a Unique-ID. You can track your sample in real time.',
  },
  {
    image: require('../res/images/ico_like.png'),
    title: 'High Quality Reports',
    body:
      'Medlife expert technician visits your home to collect blood sample. All samples have a Unique-ID. You can track your sample in real time.',
  },
  {
    image: require('../res/images/ico_message.png'),
    title: 'Expert Review',
    body:
      'Medlife expert technician visits your home to collect blood sample. All samples have a Unique-ID. You can track your sample in real time.',
  },
];

const LANGUAGES = [
  'Arabic',
  'Azerbaijani',
  'Belorussian',
  'Bulgarian',
  'Bosnian',
  'Catalan ',
  'Czech',
  'Danish',
  'German',
  'English',
  'Spanish',
  'Estonian',
  'Persian',
  'Finnish',
  'French',
];

const TESTS = ['Colestrol Test', 'Kidney Test', 'Lungs Test', 'Lado Test'];
const CategoryItem = ({item}) => {
  return (
    <View
      style={{
        elevation: 5,
        backgroundColor: Colors.white,
        borderRadius: 12,
        marginVertical: 13,
        height: 95,
        width: 95,
        marginRight: 4,
        marginLeft: 8,
      }}>
      <TouchableOpacity
        activeOpacity={0.75}
        onPress={() => {
          NavigationService.navigate('Category');
        }}
        style={{
          borderRadius: 12,
          padding: 12,
          alignItems: 'center',
        }}>
        <Image
          source={{uri: `${global.imageUri}${item.photo}`}}
          style={{
            width: 40,
            height: 40,
            backgroundColor: Colors.white,
          }}
          resizeMode="contain"
        />
        <Text
          style={{
            fontFamily: 'Nunito-SemiBold',
            fontSize: 12,
            color: Colors.grayDark,
            marginTop: 8,
          }}>
          {item.cat_name}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const TrustedDiagnosticsItem = ({item}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 25,
        marginBottom: 24,
        borderRadius: 15,
        backgroundColor: Colors.white,
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 25,
        elevation: 5,
      }}>
      <Image
        style={{width: 35, height: 35}}
        source={item.image}
        resizeMode="contain"
      />

      <View
        style={{
          justifyContent: 'center',
          width: global.screenWidth / 1.5,
          marginLeft: 13,
        }}>
        <Text
          style={{
            fontFamily: 'Nunito-Bold',
            fontSize: 15,
          }}>
          {item.method}
        </Text>
        <Text
          style={{
            fontFamily: 'Nunito-Regular',
            fontSize: 10,
            color: Colors.grayDarkest,
          }}>
          {item.body}
        </Text>
      </View>
    </View>
  );
};
class LabCategories extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showMultiple: false,
      show: false,
      selectedText: '',
      selectedData: [],
    };
  }

  flatListHeader = () => {
    return (
      <View>
        <View style={{marginHorizontal: 25}}>
          <TouchableOpacity
            activeOpacity={0.75}
            onPress={() => {
              NavigationService.navigate('ProductDetail');
            }}
            style={{borderRadius: 20}}>
            <Image
              source={require('../res/images/banner2.png')}
              style={{
                width: global.screenWidth - 50,
                height: 180,
                borderRadius: 20,
              }}
            />
          </TouchableOpacity>
          <Text style={[styles.subHeadingText, {marginTop: 34}]}>
            Mero Health Lab
          </Text>

          <View
            style={{
              marginTop: 20,
              borderBottomWidth: 1,
              borderColor: 'rgba(102,118,133,1)',
            }}>
            <PickerMultiple
              data={TESTS}
              show={this.state.showMultiple}
              selectedData={this.state.selectedData}
              placeholder="Search For Lab Test"
              deleteSingleItem={index => {
                this.setState({
                  selectedData: this.state.selectedData.splice(!index, 1),
                });
              }}
              showDropdown={() => {
                this.setState({
                  showMultiple: true,
                });
              }}
              hideDropdown={() => {
                this.setState({
                  showMultiple: false,
                });
              }}
              renderItems={item => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        selectedData: [...this.state.selectedData, item],
                        showMultiple: false,
                      });
                    }}
                    style={styles.dropDownElementStyle}>
                    <Text>{item}</Text>
                  </TouchableOpacity>
                );
              }}
            />
          </View>

          <View
            style={{borderBottomWidth: 1, borderColor: 'rgba(102,118,133,1)'}}>
            <PickerComponent
              data={LANGUAGES}
              show={this.state.show}
              selectedText={this.state.selectedText}
              placeholder="Select Location"
              showDropdown={() => {
                this.setState({
                  show: true,
                });
              }}
              hideDropdown={() => {
                this.setState({
                  show: false,
                });
              }}
              renderItems={item => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        selectedText: item,
                        show: false,
                      });
                    }}
                    style={styles.dropDownElementStyle}>
                    <Text>{item}</Text>
                  </TouchableOpacity>
                );
              }}
            />
          </View>

          <CustomButton.SecondaryButton
            style={{
              paddingHorizontal: 20,
              paddingVertical: 5,
              alignSelf: 'flex-end',
              marginTop: 10,
            }}
            title="Search"
            onPress={() => {
              NavigationService.navigate('LabResult');
            }}
          />
          <Text style={[styles.subHeadingText, {marginTop: 25}]}>
            Categories
          </Text>
        </View>
        <FlatList
          contentContainerStyle={{
            paddingTop: 10,
            paddingLeft: 8,
            paddingRight: 12,
          }}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={this.props.labData !== null && this.props.labData.labcategories}
          renderItem={({item, index}) => {
            return <CategoryItem item={item} />;
          }}
          keyExtractor={(item, index) => index.toString()}
        />
        <Text
          style={[
            styles.subHeadingText,
            {marginTop: 22, marginHorizontal: 25, marginBottom: 15},
          ]}
        />
      </View>
    );
  };

  componentDidMount() {
    this.props.saveLabData();
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            backgroundColor: Colors.primaryMedium,
            height: 110,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingBottom: 20,
            paddingHorizontal: 16,
          }}>
          <TouchableOpacity
            activeOpacity={0.75}
            onPress={() => {
              NavigationService.toggleDrawerBar();
            }}>
            <Icon.SimpleLineIcons name="grid" size={22} color={Colors.white} />
          </TouchableOpacity>
          <Image
            style={styles.logo}
            source={Images.logo}
            resizeMode="contain"
          />
          <TouchableOpacity
            activeOpacity={0.75}
            onPress={() => {
              NavigationService.navigate('Cart');
            }}>
            <Icon.FontAwesome5
              name="shopping-cart"
              size={22}
              color={Colors.white}
            />
            <View
              style={{
                position: 'absolute',
                height: 18,
                width: 18,
                top: -8,
                right: -10,
                borderRadius: 9,
                backgroundColor: Colors.redDark,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'Nunito-Regular',
                  fontSize: 12,
                  color: Colors.white,
                }}>
                {global.cartData.length}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View
          style={{
            marginTop: -20,
            borderRadius: 25,
            width: global.screenWidth,
            backgroundColor: Colors.white,
            paddingTop: 35,
          }}>
          <FlatList
            ListHeaderComponent={this.flatListHeader}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              paddingBottom: 100,
            }}
            renderItem={({item, index}) => {
              return <TrustedDiagnosticsItem item={item} />;
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.labs.loading,
    labData: state.labs.labList,
  };
};
export default connect(
  mapStateToProps,
  {
    saveLabData,
  },
)(LabCategories);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  logo: {
    width: 110,
  },
  subHeadingText: {
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 18,
  },
  dropDownElementStyle: {
    margin: 2,
    paddingVertical: 5,
    paddingHorizontal: 10,
  },

  itemStyle: {},
});
