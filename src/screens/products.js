import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
  BackHandler,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import LoadingIndicator from '../components/activityIndicator';
import {calculatePricePercentage} from '../utlis/calculatePricePercentage';
import NavigationService from '../route/navigationService';
import {BarIndicator} from 'react-native-indicators';
import HeaderDashboard from '../components/headerDashboard';

import {connect} from 'react-redux';
import {
  getProducts,
  searchProducts,
  searchProductsTextBox,
} from '../actions/ProductsAction';
import {clearSearch} from '../actions/shopActions';

import Images from '../res/images';
import Icons from '../res/icons';
import Colors from '../res/colors';
import CustomTextInput from '../components/textInput';
import CustomButton from '../components/button';
import SearchProductsDropdown from '../components/searchProductsDropdown';

class Products extends React.Component {
  constructor(props) {
    super(props);
    this.product = this.props.navigation.getParam('product');
  }

  componentDidMount() {
    this.props.getProducts(this.product.child_slug);
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.backHandler = BackHandler.addEventListener(
          'hardwareBackPress',
          this.handleBackPress,
        );
      },
    );

    this.willFocusSubscription = this.props.navigation.addListener(
      'didBlur',
      () => {
        if (this.backHandler) {
          this.backHandler.remove();
        }
      },
    );
  }

  handleBackPress = () => {
    this.props.clearSearch();
    return false;
  };

  render() {
    return (
      <View style={styles.container}>
        <LoadingIndicator isVisible={this.props.loading} />
        <HeaderDashboard title={this.product.child_name} />

        <View
          style={{
            top: 130,
            right: 0,
            left: 0,
            marginBottom: 16,
            position: 'absolute',
            zIndex: 999,
          }}>
          <SearchProductsDropdown />
        </View>

        <FlatList
          contentContainerStyle={{
            paddingLeft: 8,
            paddingRight: 12,
            paddingBottom: 8,
            paddingTop: 54,
          }}
          numColumns={2}
          data={
            this.props.productsList != ''
              ? this.props.productsList.products.data
              : []
          }
          ListEmptyComponent={() => (
            <View>
              {!this.props.loading && (
                <View
                  style={{
                    elevation: 5,
                    backgroundColor: Colors.white,
                    borderRadius: 12,
                    marginHorizontal: 16,
                    marginVertical: 10,
                    padding: 12,
                    alignItems: 'center',
                  }}>
                  <Icons.AntDesign
                    name="exclamationcircleo"
                    size={35}
                    color={Colors.grayDark}
                  />

                  <Text
                    numberOfLines={1}
                    style={{
                      fontFamily: 'Nunito-Bold',
                      fontSize: 16,
                      color: Colors.black,
                      marginTop: 8,
                    }}>
                    No products found
                  </Text>
                  <Text
                    numberOfLines={1}
                    style={{
                      fontFamily: 'Nunito-Regular',
                      fontSize: 12,
                      color: Colors.grayDark,
                    }}>
                    Please try searching with the product name.
                  </Text>
                </View>
              )}
            </View>
          )}
          renderItem={({item, index}) => {
            return (
              <View
                style={{
                  width: global.screenWidth / 2 - 22,
                  elevation: 5,
                  marginVertical: 10,
                  backgroundColor: Colors.white,
                  borderRadius: 12,
                  marginRight: 4,
                  marginLeft: 8,
                }}>
                <TouchableOpacity
                  activeOpacity={0.75}
                  onPress={() => {
                    NavigationService.navigate('ProductDetail', {
                      productDetail: item,
                    });
                  }}
                  style={{
                    borderRadius: 20,
                    padding: 12,
                  }}>
                  {item.pprice !== null && (
                    <View
                      style={{
                        position: 'absolute',
                        top: 16,
                        left: 16,
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: 16,
                        borderRadius: 6,
                        backgroundColor: Colors.greenLight,
                        zIndex: 1,
                        paddingHorizontal: 8,
                      }}>
                      <Text
                        numberOfLines={1}
                        style={{
                          fontFamily: 'Nunito-Bold',
                          fontSize: 10,
                          color: Colors.white,
                        }}>
                        {calculatePricePercentage(item.cprice, item.pprice)}%
                        off
                      </Text>
                    </View>
                  )}
                  <Image
                    source={{
                      uri: `https://merohealthcare.com/assets/images/${
                        item.photo
                      }`,
                    }}
                    style={{
                      height: 120,
                      paddingHorizontal: 12,
                      borderRadius: 8,
                    }}
                    resizeMode="contain"
                  />
                  <Text
                    numberOfLines={1}
                    style={{
                      fontFamily: 'Nunito-SemiBold',
                      fontSize: 12,
                      color: Colors.black,
                      marginTop: 8,
                    }}>
                    {item.name}
                  </Text>

                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      marginTop: 8,
                    }}>
                    <Text
                      style={{
                        fontFamily: 'Nunito-Bold',
                        fontSize: 14,
                        color: Colors.primaryMedium,
                      }}>
                      Rs {item.cprice}
                    </Text>
                    {item.pprice && item.cprice != item.pprice && (
                      <Text
                        style={{
                          fontFamily: 'Nunito-Bold',
                          fontSize: 9,
                          color: Colors.redDark,
                          textDecorationLine: 'line-through',
                          textDecorationStyle: 'solid',
                        }}>
                        Rs {item.pprice}
                      </Text>
                    )}
                  </View>
                </TouchableOpacity>
              </View>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    productsSearch: state.products.productsSearch,
    loading: state.products.loading,
    productsList: state.products.productsList,
    searchResult: state.products.searchResult,
    searchLoading: state.products.searchLoading,
    cartCount: state.cart.cartCount,
  };
}

export default connect(
  mapStateToProps,
  {getProducts, searchProducts, searchProductsTextBox, clearSearch},
)(Products);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  logo: {
    width: 110,
  },
});
