import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ToastAndroid,
  TextInput,
  ScrollView,
  CheckBox,
  Platform,
} from 'react-native';
import MapView from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import Modal from 'react-native-modal';
import {
  scaleHorizontal,
  scaleVertical,
  scaleAvg,
  scaledWidth,
  scaledHeight,
  moderateScale,
} from '../components/responsiveUI';
import PNGLoadingIndicator from '../components/activityIndicator';
import {connect} from 'react-redux';
import {
  postLocationData,
  fetchLocationName,
  createLocationDialog,
} from '../actions/AddLocationAction';

import Colors from '../res/colors';
import Images from '../res/images';
import Icon from '../res/icons';
import Header from '../components/headerDashboard';
class AddLocationMap extends React.Component {
  constructor(props) {
    super(props);

    // const geolocationStatus = this.props.navigation.getParam(
    //     'geolocationStatus',
    //     'NO_CATEGORY_FOUND',
    // );

    this.checkGeoLocation = 0;
    this.state = {
      region: '',

      //initial
      initialLatitude: 28.098742629896165,
      initialLongitude: 84.12360040470958,
      initialLatitudeDelta: 12.971051614105335,
      initialLongitudeDelta: 8.527054265141473,

      //animate
      currentLatitude: 28.098742629896165,
      currentLongitude: 84.12360040470958,
      currentLatitudeDelta: 12.971051614105335,
      currentLongitudeDelta: 8.527054265141473,

      marginTop: 51,

      title: '',
      name: global.userInfo.name || '',
      mobile: global.userInfo.phone || '',
      email: global.userInfo.email || '',
    };

    this.checkTitle = true;
    this.checkName = true;
    this.checkMobile = true;
    this.checkEmail = true;

    // if (geolocationStatus) {
    Geolocation.getCurrentPosition(
      info => {
        this.setState({
          currentLatitude: info.coords.latitude,
          currentLongitude: info.coords.longitude,
          currentLatitudeDelta: 0.012,
          currentLongitudeDelta: 0.0065,
        });
      },
      error => {
        this.checkCurrentLocation();
      },
    );
    // }
  }

  checkCurrentLocation() {
    if (this.checkGeoLocation < 15) {
      Geolocation.getCurrentPosition(
        info => {
          this.setState({
            currentLatitude: info.coords.latitude,
            currentLongitude: info.coords.longitude,
            currentLatitudeDelta: 0.012,
            currentLongitudeDelta: 0.0065,
          });
          this.map.animateToRegion(
            {
              latitude: this.state.currentLatitude,
              longitude: this.state.currentLongitude,
              latitudeDelta: 0.012,
              longitudeDelta: 0.0065,
            },
            1000,
          );
        },
        error => {
          setTimeout(() => {
            ++this.checkGeoLocation;
            this.checkCurrentLocation();
          }, 1000);
        },
      );
    } else {
      ToastAndroid.showWithGravityAndOffset(
        "Current location couldn't be detected.",
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        0,
        150,
      );
    }
  }

  onRegionChange = region => {
    this.setState({
      region,
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <Header title="Add Location" />
        <PNGLoadingIndicator isVisible={this.props.addLocationmap.loading} />

        <Modal
          isVisible={this.props.addLocationmap.createLocationDialog}
          animationIn={'zoomIn'}
          animationOut={'zoomOut'}
          backdropOpacity={0.5}
          useNativeDriver={true}
          onBackButtonPress={() => {
            this.props.createLocationDialog(false);
          }}>
          <View
            cardElevation={24}
            cardMaxElevation={24}
            cornerRadius={15}
            style={{
              alignItems: 'center',
              justifyContent: 'space-between',
              backgroundColor: Colors.white,
              borderRadius: 20,
            }}>
            <ScrollView
              keyboardShouldPersistTaps="handled"
              style={{width: '100%', paddingHorizontal: 20}}>
              <Text style={styles.alertTitleStyle}>
                {this.props.addLocationmap.selectedLocationName}
              </Text>

              <View style={{height: 0.5, backgroundColor: Colors.grayDark}} />

              <View style={styles.textInputView}>
                <Icon.AntDesign
                  name="tags"
                  size={20}
                  color={Colors.grayMedium}
                  style={styles.icon}
                />
                <TextInput
                  underlineColorAndroid="transparent"
                  placeholder="Title* (Home, Office etc.)"
                  style={styles.textInput}
                  value={this.state.title}
                  onChangeText={text => this.setState({title: text})}
                  keyboardType="default"
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType="next"
                />
                {!this.checkTitle && (
                  <Icon.Entypo
                    name="cross"
                    size={20}
                    color={Colors.redDark}
                    style={styles.icon}
                  />
                )}
              </View>

              <View style={{height: 0.5, backgroundColor: Colors.grayDark}} />

              <View style={styles.textInputView}>
                <Icon.Entypo
                  name="user"
                  size={20}
                  color={Colors.grayMedium}
                  style={styles.icon}
                />
                <TextInput
                  underlineColorAndroid="transparent"
                  placeholder="Full Name*"
                  style={styles.textInput}
                  value={this.state.name}
                  onChangeText={text => this.setState({name: text})}
                  keyboardType="default"
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType="next"
                />
                {!this.checkName && (
                  <Icon.Entypo
                    name="cross"
                    size={20}
                    color={Colors.redDark}
                    style={styles.icon}
                  />
                )}
              </View>

              <View style={{height: 0.5, backgroundColor: Colors.grayDark}} />

              <View style={styles.textInputView}>
                <Icon.Entypo
                  name="mobile"
                  size={20}
                  color={Colors.grayMedium}
                  style={styles.icon}
                />
                <Text
                  style={{
                    fontSize: 14,
                    color: Colors.grayDarkest,
                    marginLeft: 15,
                    marginRight: -10,
                    fontFamily: 'Nunito-Light',
                    paddingBottom: Platform.OS === 'ios' ? 5 : 0,
                  }}>
                  +977
                </Text>
                <TextInput
                  underlineColorAndroid="transparent"
                  placeholder="Mobile*"
                  maxLength={10}
                  style={styles.textInput}
                  value={this.state.mobile}
                  onChangeText={text => this.setState({mobile: text})}
                  keyboardType="number-pad"
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType="next"
                />
                {!this.checkMobile && (
                  <Icon.Entypo
                    name="cross"
                    size={20}
                    color={Colors.redDark}
                    style={styles.icon}
                  />
                )}
              </View>

              <View style={{height: 0.5, backgroundColor: Colors.grayDark}} />

              <View style={styles.textInputView}>
                <Icon.Entypo
                  name="mail"
                  size={20}
                  color={Colors.grayMedium}
                  style={styles.icon}
                />
                <TextInput
                  underlineColorAndroid="transparent"
                  placeholder="Email"
                  style={styles.textInput}
                  value={this.state.email}
                  onChangeText={text => this.setState({email: text})}
                  keyboardType="email-address"
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType="next"
                />
                {!this.checkEmail && (
                  <Icon.Entypo
                    name="cross"
                    size={20}
                    color={Colors.redDark}
                    style={styles.icon}
                  />
                )}
              </View>

              <View style={{height: 0.5, backgroundColor: Colors.grayDark}} />

              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.createLocationDialog(false);
                  }}
                  activeOpacity={0.7}
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingVertical: 15,
                  }}>
                  <Icon.Entypo
                    name="cross"
                    size={20 * scaleAvg}
                    color={Colors.redDark}
                  />
                  <Text style={styles.alertButtonStyle}>Cancel</Text>
                </TouchableOpacity>

                <View style={styles.verticalLineLocationDialogue} />

                <TouchableOpacity
                  onPress={() => {
                    this.createLocationData(
                      this.state.title,
                      this.state.name,
                      this.state.mobile,
                      this.state.email,
                    );
                  }}
                  activeOpacity={0.7}
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingVertical: 15,
                  }}>
                  <Icon.Ionicons
                    name="md-checkmark"
                    size={20 * scaleAvg}
                    color={Colors.greenLight}
                  />
                  <Text style={styles.alertButtonStyle}>Confirm</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </View>
        </Modal>

        <View
          style={{
            flex: 1,
            backgroundColor: Colors.white,
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            marginTop: -20,
          }}>
          <MapView
            ref={map => (this.map = map)}
            style={{flex: 1, marginTop: this.state.marginTop}}
            onMapReady={() => {
              this.map.animateToRegion(
                {
                  latitude: this.state.currentLatitude,
                  longitude: this.state.currentLongitude,
                  latitudeDelta: this.state.currentLatitudeDelta,
                  longitudeDelta: this.state.currentLongitudeDelta,
                },
                1000,
              );

              this.setState({marginTop: this.state.marginTop - 1});
            }}
            showsUserLocation={true}
            showsMyLocationButton={true}
            onRegionChangeComplete={this.onRegionChange}
            region={this.props.coordinate}
            zoomEnabled={true}
            zoomControlEnabled={true}
            initialRegion={{
              latitude: this.state.initialLatitude,
              longitude: this.state.initialLongitude,
              latitudeDelta: this.state.initialLatitudeDelta,
              longitudeDelta: this.state.initialLongitudeDelta,
            }}
          />

          <GooglePlacesAutocomplete
            placeholder="Search"
            minLength={2} // minimum length of text to search
            autoFocus={false}
            returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
            listViewDisplayed="undefined" // true/false/undefined
            fetchDetails={true}
            renderDescription={row => row.description} // custom description render
            onPress={(data, details = null) => {
              this.map.animateToRegion(
                {
                  latitude: details.geometry.location.lat,
                  longitude: details.geometry.location.lng,
                  latitudeDelta: 0.012,
                  longitudeDelta: 0.0065,
                },
                1000,
              );
            }}
            getDefaultValue={() => ''}
            query={{
              key: global.googleApiKey,
              language: 'en',
              components: 'country:np',
            }}
            styles={{
              container: {
                flex: 0,
                position: 'absolute',
                width: '100%',
                zIndex: 1,
              },
              textInputContainer: {
                borderTopWidth: 0,
                borderBottomWidth: 0,
                height: 50,
                overflow: 'visible',
                backgroundColor: Colors.grayLight,
                elevation: 4,
              },
              textInput: {
                backgroundColor: Colors.white,
                fontFamily: 'Nunito-Light',
                color: Colors.grayDarkest,
                fontSize: 14,
                height: 35,
                flex: 1,
              },
              listView: {
                borderRadius: 5,
                marginHorizontal: 8,
                backgroundColor: 'white',
                elevation: 4,
              },
              description: {
                color: Colors.primaryDark,
                fontFamily: 'Nunito-Light',
                fontSize: 14,
              },
            }}
            nearbyPlacesAPI="GooglePlacesSearch"
            enablePoweredByContainer={false}
          />

          <View style={styles.markerFixed}>
            <Image style={styles.marker} source={Images.deliveryMarker} />
          </View>

          <View style={styles.footer}>
            <TouchableOpacity
              onPress={() => this.confirmLocation()}
              activeOpacity={0.75}
              style={{
                width: 175,
                height: 40,
                marginTop: 34,
                borderRadius: 16,
                backgroundColor: Colors.primaryMedium,
                alignSelf: 'center',
              }}>
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'Nunito-SemiBold',
                    fontSize: 16,
                    color: Colors.white,
                  }}>
                  Confirm Location
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  confirmLocation = () => {
    if (this.state.region.latitude != null) {
      this.selectedLocationLatitude = this.state.region.latitude.toString();
      this.selectedLocationLongitude = this.state.region.longitude.toString();

      this.props.fetchLocationName(
        this.state.region.latitude,
        this.state.region.longitude,
      );
    }
  };

  createLocationData(title, name, mobile, email) {
    if (title != '') {
      this.checkTitle = true;
    } else {
      this.checkTitle = false;
    }

    if (name != '') {
      this.checkName = true;
    } else {
      this.checkName = false;
    }

    //mobile check
    if (mobile != '') {
      if (mobile.length == 10) {
        if (
          mobile.substring(0, 2) == '96' ||
          mobile.substring(0, 2) == '97' ||
          mobile.substring(0, 2) == '98'
        ) {
          this.checkMobile = true;
        } else {
          this.checkMobile = false;
        }
      } else {
        this.checkMobile = false;
      }
    } else {
      this.checkMobile = false;
    }

    //email check

    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email == '') {
      this.checkEmail = true;
    } else {
      if (reg.test(email) === true) {
        this.checkEmail = true;
      } else {
        this.checkEmail = false;
      }
    }

    if (
      this.checkTitle &&
      this.checkName &&
      this.checkMobile &&
      this.checkEmail
    ) {
      this.props.createLocationDialog(false);

      this.props.postLocationData(
        title,
        name,
        mobile,
        email,
        this.props.addLocationmap.selectedLocationName,
        this.state.region.latitude,
        this.state.region.longitude,
      );
    } else {
      this.forceUpdate();
    }
  }
}

function mapStateToProps(state) {
  return {
    addLocationmap: state.addLocationmap,
  };
}

export default connect(
  mapStateToProps,
  {postLocationData, fetchLocationName, createLocationDialog},
)(AddLocationMap);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  markerFixed: {
    left: '50%',
    marginLeft: -17,
    marginTop: -24,
    position: 'absolute',
    top: '50%',
  },
  marker: {
    width: 34,
    height: 48,
  },
  footer: {
    bottom: 20,
    alignSelf: 'center',
    position: 'absolute',
  },
  region: {
    color: '#fff',
    lineHeight: 20,
    margin: 20,
  },
  textInputView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15,
  },
  textInput: {
    flex: 1,
    fontSize: 14,
    color: Colors.grayDarkest,
    marginLeft: 15,
    fontFamily: 'Nunito-Regular',
    marginBottom: 8,
    paddingBottom: Platform.OS === 'ios' ? 5 : 0,
  },
  icon: {
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  alertTitleStyle: {
    fontFamily: 'Nunito-SemiBold',
    color: Colors.grayDark,
    textAlign: 'center',
    fontSize: 14,
    marginTop: 16,
    marginBottom: 10,
  },
  alertButtonStyle: {
    fontFamily: 'Nunito-SemiBold',
    color: Colors.grayDark,
    textAlign: 'center',
    fontSize: 14,
    marginLeft: 10,
  },
  verticalLineLocationDialogue: {
    height: 35 * scaleVertical,
    width: 0.5,
    backgroundColor: Colors.grayDark,
    alignSelf: 'center',
  },
});
