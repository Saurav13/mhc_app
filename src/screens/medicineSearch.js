import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';

import {connect} from 'react-redux';
import {searchMedicine} from '../actions/MedicineSearchAction';
import {addToCart} from '../actions/CartAction';

import NavigationService from '../route/navigationService';
import CustomButton from '../components/button';
import HeaderDashboard from '../components/headerDashboard';
import Images from '../res/images';
import Icons from '../res/icons';
import Colors from '../res/colors';
import LoadingIndicator from '../components/activityIndicator';
import {calculatePricePercentage} from '../utlis/calculatePricePercentage';

class MedicineSearch extends Component {
  constructor(props) {
    super(props);
    this.searchedText = this.props.navigation.getParam('searchedText');
  }

  componentDidMount() {
    this.props.searchMedicine(this.searchedText);
  }

  SearchResult = ({item}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.75}
        onPress={() => {
          NavigationService.navigate('ProductDetail', {
            productDetail: item,
          });
        }}
        style={{
          padding: 16,
          marginBottom: 16,
          backgroundColor: Colors.white,
          elevation: 3,
          borderRadius: 15,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Image
          source={{
            uri: `https://merohealthcare.com/assets/images/${item.photo}`,
          }}
          style={{height: 77, width: 77}}
          resizeMode="contain"
        />
        {item.cprice != item.pprice && item.pprice != null && (
          <View
            style={{
              position: 'absolute',
              justifyContent: 'center',
              alignItems: 'center',
              top: 16,
              left: 16,
              height: 16,
              borderRadius: 10,
              backgroundColor: Colors.greenDark,
              paddingHorizontal: 9,
              paddingVertical: 1,
            }}>
            <Text
              style={{
                fontFamily: 'Nunito-SemiBold',
                fontSize: 10,
                color: Colors.white,
              }}>
              {calculatePricePercentage(item.cprice, item.pprice)}% Off
            </Text>
          </View>
        )}
        <View style={{flex: 1, marginLeft: 8}}>
          <Text
            style={{
              fontFamily: 'Nunito-SemiBold',
              fontSize: 12,
            }}>
            {item.name}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'flex-end',
              marginTop: 4,
            }}>
            <Text
              style={{
                fontFamily: 'Nunito-Bold',
                fontSize: 16,
                color: Colors.primaryMedium,
                marginBottom: -2,
              }}>
              Rs {item.cprice}
            </Text>
            <Text
              style={{
                fontSize: 10,
                color: Colors.grayDarkest,
                marginLeft: 7,
                fontFamily: 'Nunito-SemiBold',
              }}>
              {item.product_quantity}
            </Text>
            {item.cprice != item.pprice && item.pprice != null && (
              <Text
                style={{
                  fontFamily: 'Nunito-Bold',
                  fontSize: 11,
                  color: Colors.redDark,
                  textDecorationLine: 'line-through',
                  textDecorationStyle: 'solid',
                  marginLeft: 7,
                }}>
                Rs {item.pprice}
              </Text>
            )}
          </View>

          <Text
            style={{
              color: Colors.greenDarkest,
              fontFamily: 'Nunito-SemiBold',
              fontSize: 12,
              marginTop: 4,
            }}>
            {item.company_name}
          </Text>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginTop: 4,
            }}>
            <Text
              style={{
                fontFamily: 'Nunito-SemiBold',
                fontSize: 10,
                color: Colors.grayDarkest,
              }}>
              {item.sub_title}
            </Text>
            <CustomButton.SecondaryButton
              style={{
                height: 25,
                paddingHorizontal: 18,
                paddingVertical: 7,
                backgroundColor: Colors.primaryMedium,
              }}
              textStyle={{
                color: Colors.white,
                fontFamily: 'Nunito-Regular',
                fontSize: 10,
                paddingBottom: 0,
              }}
              title="Add to Cart"
              onPress={() => {
                this.props.addToCart(item, 1);
              }}
            />
          </View>
        </View>

        {/* <TouchableOpacity
          onPress={() => {}}
          activeOpacity={0.7}
          style={{
            position: 'absolute',
            height: 25,
            bottom: 16,
            right: 16,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: Colors.primaryMedium,
            borderRadius: 16,
            paddingHorizontal: 18,
          }}>
          <Text
            style={{
              color: Colors.white,
              fontFamily: 'Nunito-Regular',
              fontSize: 8,
            }}>
            Add to Cart
          </Text>
        </TouchableOpacity> */}
      </TouchableOpacity>
    );
  };

  SearchButton = ({title, onPress}) => {
    return (
      <TouchableOpacity
        onPress={onPress}
        activeOpacity={0.7}
        style={styles.searchButtonStyle}>
        <Text
          style={{
            color: Colors.white,
            fontFamily: 'Nunito-Regular',
            fontSize: 11,
            marginRight: 7,
          }}>
          {title}
        </Text>
        <TouchableOpacity activeOpacity={0.7} onPress={() => {}}>
          <Icons.Entypo name="cross" size={11} color={Colors.white} />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <LoadingIndicator isVisible={this.props.medicineSearch.loading} />

        <HeaderDashboard title="Medicines" />
        <View
          style={{
            flex: 1,
            marginTop: -20,
            borderRadius: 25,
            width: global.screenWidth,
            backgroundColor: Colors.white,
          }}>
          <View style={{marginHorizontal: 16, marginTop: 22}}>
            <Text style={[styles.subHeadingText, {marginBottom: 10}]}>
              Showing Results For :
            </Text>
            <View
              style={{
                flexDirection: 'row',
                paddingBottom: 11,
                borderBottomWidth: 1,
                borderBottomColor: Colors.grayDarkest,
              }}>
              <this.SearchButton title={this.searchedText} onPress={() => {}} />
            </View>
          </View>
          <FlatList
            ListHeaderComponent={this.flatListHeader}
            contentContainerStyle={{
              paddingTop: 16,
              paddingHorizontal: 16,
              paddingBottom: 16,
            }}
            ListEmptyComponent={() => (
              <View>
                {!this.props.medicineSearch.loading && (
                  <View
                    style={{
                      elevation: 5,
                      backgroundColor: Colors.white,
                      borderRadius: 12,
                      marginHorizontal: 16,
                      marginBottom: 10,
                      padding: 12,
                      alignItems: 'center',
                    }}>
                    <Icons.FontAwesome
                      name="search"
                      size={35}
                      color={Colors.grayDark}
                    />

                    <Text
                      numberOfLines={1}
                      style={{
                        fontFamily: 'Nunito-Bold',
                        fontSize: 16,
                        color: Colors.black,
                        marginTop: 8,
                      }}>
                      No products found
                    </Text>
                    <Text
                      numberOfLines={1}
                      style={{
                        fontFamily: 'Nunito-Regular',
                        fontSize: 12,
                        color: Colors.grayDark,
                      }}>
                      Please search with exact product name.
                    </Text>
                  </View>
                )}
              </View>
            )}
            data={this.props.medicineSearch.medicineList}
            renderItem={({item, index}) => {
              return <this.SearchResult item={item} />;
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    medicineSearch: state.medicineSearch,
  };
};

export default connect(
  mapStateToProps,
  {
    searchMedicine,
    addToCart,
  },
)(MedicineSearch);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  logo: {
    width: 110,
  },
  subHeadingText: {
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 18,
  },
  searchButtonStyle: {
    height: 26,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.primaryMedium,
    borderRadius: 16,
    paddingHorizontal: 12,
    paddingVertical: 6,
    marginRight: 11,
  },
  priceText: {
    fontFamily: 'Nunito-Bold',
    fontSize: 30,
    color: Colors.primaryDark,
  },
  priceTextCut: {
    fontFamily: 'Nunito-Bold',
    fontSize: 11,
    color: Colors.redDark,
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  priceTextDiscount: {
    fontFamily: 'Nunito-Bold',
    fontSize: 11,
    color: Colors.greenDark,
    textDecorationStyle: 'solid',
  },
});
