import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import NavigationService from '../route/navigationService';
import CustomTextInput from '../components/textInput';
import Images from '../res/images';
import Icon from '../res/icons';
import Colors from '../res/colors';
import {connect} from 'react-redux';
import {calculatePricePercentage} from '../utlis/calculatePricePercentage';
import {searchMedicineTextBox} from '../actions/MedicineAction';
import HeaderDashboard from '../components/headerDashboard';
import AlertToast from '../components/alertToast';

class Medicine extends Component {
  render() {
    return (
      <View style={styles.container}>
        <HeaderDashboard />

        <View
          style={{
            flex: 1,
            marginTop: -20,
            borderRadius: 25,
            width: global.screenWidth,
            backgroundColor: Colors.white,
            paddingHorizontal: 25,
          }}>
          <View
            style={{
              marginTop: 25,
              marginBottom: 39,
              backgroundColor: Colors.yellowLight,
              borderRadius: 15,
              paddingVertical: 28,
              paddingHorizontal: 14,
              elevation: 5,
            }}>
            <Text
              style={{
                fontFamily: 'Nunito-Bold',
                fontSize: 15,
                marginBottom: 5,
              }}>
              Order Quickly with prescription
            </Text>
            <Text
              style={{
                fontFamily: 'Nunito-Bold',
                fontSize: 14,
                color: Colors.grayDarkest,
              }}>
              Upload prescription and tell us what you need. We do all rest !
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontFamily: 'Nunito-ExtraBold',
                  fontSize: 17,
                  color: Colors.greenDark,
                  marginTop: 12,
                }}>
                Save 20%
              </Text>
              <TouchableOpacity
                onPress={() => {}}
                activeOpacity={0.7}
                style={{
                  height: 32,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: Colors.primaryMedium,
                  borderRadius: 16,
                  paddingHorizontal: 22,
                }}>
                <Text
                  style={{
                    color: Colors.white,
                    fontFamily: 'Nunito-Bold',
                    fontSize: 12,
                  }}>
                  Upload
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <Text
            style={{
              fontFamily: 'Nunito-ExtraBold',
              fontSize: 18,
            }}>
            Find Your Medicine
          </Text>
          <CustomTextInput.MainTextInput
            label="Search Medicine"
            value={this.props.medicineSearch}
            onChangeText={val => this.props.searchMedicineTextBox(val)}
            keyboardType="default"
          />

          <TouchableOpacity
            onPress={() => {
              if(this.props.medicineSearch != ''){


              NavigationService.navigate('MedicineSearch', {
                searchedText: this.props.medicineSearch,
              });
            }else{
              AlertToast("Enter the text for the search.",120)
            }
            }}
            activeOpacity={0.7}
            style={{
              height: 32,
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              backgroundColor: Colors.primaryMedium,
              borderRadius: 16,
              paddingHorizontal: 22,
              paddingVertical: 9,
              marginTop: 15,
            }}>
            <Text
              style={{
                color: Colors.white,
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
              }}>
              Search
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    medicineList: state.medicine.medicineList,
    medicineSearch: state.medicine.medicineSearch,
    loading: state.medicine.loading,
  };
};

export default connect(
  mapStateToProps,
  {
    searchMedicineTextBox,
  },
)(Medicine);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  logo: {
    width: 110,
  },
  subHeadingText: {
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 18,
  },
  searchButtonStyle: {
    height: 26,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.primaryMedium,
    borderRadius: 16,
    paddingHorizontal: 12,
    paddingVertical: 6,
    marginRight: 11,
  },
});
