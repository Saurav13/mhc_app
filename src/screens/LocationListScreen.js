import React, {Component} from 'react';
import {View, Text, FlatList, StyleSheet, TouchableOpacity} from 'react-native';
import Colors from '../res/colors';
import Icons from '../res/icons';
import HeaderDashboard from '../components/headerDashboard';
import NavigationService from '../route/navigationService';
import {addSingleLocation, getLocationData} from '../actions/AddLocationAction';
import {connect} from 'react-redux';
class LocationListScreen extends Component {
  componentDidMount() {
    this.props.getLocationData(global.userLocation);
  }
  render() {
    return (
      <View style={styles.container}>
        <HeaderDashboard title="Location List" />

        <View
          style={{
            flex: 1,
            backgroundColor: Colors.white,
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            marginTop: -20,
          }}>
          <FlatList
            data={this.props.locationDataList}
            ListEmptyComponent={() => (
              <View
                style={{
                  elevation: 5,
                  marginTop: 26,
                  backgroundColor: Colors.white,
                  borderRadius: 12,
                  marginHorizontal: 16,
                  marginBottom: 10,
                  padding: 12,
                  alignItems: 'center',
                }}>
                <Icons.Entypo
                  name="location"
                  color={Colors.grayDark}
                  size={35}
                />

                <Text
                  numberOfLines={1}
                  style={{
                    fontFamily: 'Nunito-Bold',
                    fontSize: 16,
                    color: Colors.black,
                    marginTop: 8,
                  }}>
                  No location found
                </Text>
                <Text
                  numberOfLines={1}
                  style={{
                    fontFamily: 'Nunito-Regular',
                    fontSize: 12,
                    color: Colors.grayDark,
                  }}>
                  Please add a new location.
                </Text>
              </View>
            )}
            contentContainerStyle={{paddingTop: 16}}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => {
              return (
                <View
                  style={{
                    elevation: 5,
                    backgroundColor: Colors.white,
                    borderRadius: 12,
                    marginHorizontal: 16,
                    marginBottom: 16,
                  }}>
                  <TouchableOpacity
                    activeOpacity={0.75}
                    onPress={() => {
                      this.props.addSingleLocation(item);
                      NavigationService.back();
                    }}
                    style={{
                      borderRadius: 20,
                      padding: 12,
                    }}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <View style={{flex: 1}}>
                        <Text
                          numberOfLines={1}
                          style={{
                            fontFamily: 'Nunito-Bold',
                            fontSize: 12,
                            color: Colors.primaryMedium,
                          }}>
                          {item.title}
                        </Text>

                        <Text
                          style={{
                            fontFamily: 'Nunito-Bold',
                            fontSize: 12,
                            color: Colors.black,
                            marginTop: 4,
                          }}>
                          {item.locationName}
                        </Text>
                        <Text
                          numberOfLines={3}
                          style={{
                            fontFamily: 'Nunito-SemiBold',
                            fontSize: 12,
                            color: Colors.grayMedium,
                            marginTop: 4,
                          }}>
                          {item.name}
                        </Text>
                        <Text
                          numberOfLines={3}
                          style={{
                            fontFamily: 'Nunito-SemiBold',
                            fontSize: 12,
                            color: Colors.grayMedium,
                            marginTop: 4,
                          }}>
                          {item.mobile}
                        </Text>
                        <Text
                          numberOfLines={3}
                          style={{
                            fontFamily: 'Nunito-SemiBold',
                            fontSize: 12,
                            color: Colors.grayMedium,
                            marginTop: 4,
                          }}>
                          {item.email}
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              );
            }}
          />
        </View>
        <TouchableOpacity
          activeOpacity={0.75}
          onPress={() => {
            NavigationService.navigate('Map');
          }}
          style={styles.floatingButtonStyle}>
          <Icons.AntDesign name="plus" color={Colors.white} size={25} />
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    locationDataList: state.addLocationmap.locationDataList,
  };
};
export default connect(
  mapStateToProps,
  {
    addSingleLocation,
    getLocationData,
  },
)(LocationListScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  floatingButtonStyle: {
    backgroundColor: Colors.primaryMedium,
    width: 60,
    height: 60,
    borderRadius: 30,
    elevation: 5,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    right: 30,
    bottom: 30,
  },
});
