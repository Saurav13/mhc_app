import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
  BackHandler,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import LoadingIndicator from '../components/activityIndicator';

import NavigationService from '../route/navigationService';
import {BarIndicator} from 'react-native-indicators';
import HeaderDashboard from '../components/headerDashboard';

import {connect} from 'react-redux';
import {
  saveCategoriesData,
  searchCategoriesTextBox,
  searchCategories,
} from '../actions/CategoriesAction';
import {clearSearch} from '../actions/shopActions';
import Images from '../res/images';
import Icon from '../res/icons';
import Colors from '../res/colors';
import CustomTextInput from '../components/textInput';
import CustomButton from '../components/button';
import SearchProductsDropdown from '../components/searchProductsDropdown';

class Category extends React.Component {
  constructor(props) {
    super(props);
    this.categoryData = this.props.navigation.getParam('categoryData');
    this.slug = this.categoryData.cat_slug;
  }

  componentDidMount() {
    this.props.saveCategoriesData(this.slug);
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.backHandler = BackHandler.addEventListener(
          'hardwareBackPress',
          this.handleBackPress,
        );
      },
    );

    this.willFocusSubscription = this.props.navigation.addListener(
      'didBlur',
      () => {
        if (this.backHandler) {
          this.backHandler.remove();
        }
      },
    );
  }

  handleBackPress = () => {
    this.props.clearSearch();
    return false;
  };

  render() {
    return (
      <View style={styles.container}>
        <LoadingIndicator isVisible={this.props.loading} />
        <HeaderDashboard title={this.categoryData.cat_name} />
        <View
          style={{
            top: 130,
            right: 0,
            left: 0,
            marginBottom: 16,
            position: 'absolute',
            zIndex: 999,
          }}>
          <SearchProductsDropdown />
        </View>

        <FlatList
          contentContainerStyle={{
            paddingBottom: 12,
            paddingTop: 44,
            paddingBottom: 16
          }}
          data={this.props.categories.sub_categories}
          renderItem={({item, index}) => {
            console.log(item);
            return (
              <View style={{marginTop: 16}}>
                <Text
                  numberOfLines={1}
                  style={{
                    fontFamily: 'Nunito-SemiBold',
                    fontSize: 18,
                    color: Colors.black,
                    marginLeft: 16,
                  }}>
                  {item.sub_name}
                </Text>

                <FlatList
                  contentContainerStyle={{
                    paddingLeft: 8,
                    paddingRight: 12,
                  }}
                  showsHorizontalScrollIndicator={false}
                  horizontal
                  data={item.childs}
                  renderItem={({item, index}) => {
                    return (
                      <View
                        style={{
                          width: 90,
                          alignItems: 'center',
                          marginRight: 4,
                          marginLeft: 8,
                        }}>
                        <View
                          style={{
                            width: 90,
                            height: 90,
                            elevation: 5,
                            marginVertical: 10,
                            backgroundColor: Colors.white,
                            borderRadius: 12,
                          }}>
                          <TouchableOpacity
                            activeOpacity={0.75}
                            onPress={() => {
                              NavigationService.navigate('Products', {
                                product: item,
                              });
                            }}
                            style={{
                              borderRadius: 20,
                              padding: 12,
                            }}>
                            {
                              <Image
                                source={item.image}
                                style={{
                                  width: 66,
                                  height: 66,
                                  paddingHorizontal: 12,
                                  borderRadius: 4,
                                  backgroundColor: Colors.grayLightest,
                                }}
                              />
                            }
                          </TouchableOpacity>
                        </View>

                        <Text
                          numberOfLines={2}
                          style={{
                            fontFamily: 'Nunito-SemiBold',
                            fontSize: 12,
                            color: Colors.grayMedium,
                            textAlign: 'center',
                          }}>
                          {item.child_name}
                        </Text>
                      </View>
                    );
                  }}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.categories.loading,
    categoriesSearch: state.categories.categoriesSearch,
    categories: state.categories.categoriesList,
    cartCount: state.cart.cartCount,
  };
}

export default connect(
  mapStateToProps,
  {saveCategoriesData, searchCategoriesTextBox, searchCategories, clearSearch},
)(Category);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  logo: {
    width: 110,
  },
});
