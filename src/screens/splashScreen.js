import React from 'react';
import {StyleSheet, View, ImageBackground, Text, Image} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {BarIndicator} from 'react-native-indicators';

import {connect} from 'react-redux';
import {fetchAllSplashData} from '../actions/splashScreenActions';

import LoadingIndicator from '../components/activityIndicator';
import NavigationService from '../route/navigationService';
import Images from '../res/images';
import Colors from '../res/colors';

class SplashScreen extends React.Component {
  componentDidMount() {
    AsyncStorage.getItem('KeyAccessToken').then(accessToken => {
      global.accessToken = accessToken;
    });

    AsyncStorage.getItem('KeyExpiresIn').then(expiresIn => {
      global.expiresIn = expiresIn;
    });

    AsyncStorage.getItem('KeyRefreshToken').then(refreshToken => {
      global.refreshToken = refreshToken;
    });

    AsyncStorage.getItem('cart').then(cartData => {
      global.cartData = JSON.parse(cartData);
      if (!global.cartData) {
        global.cartData = [];
      }
    });

    AsyncStorage.getItem('userLocationInfo').then(userLocation => {
      global.userLocation = JSON.parse(userLocation);
      if (!global.userLocation) {
        global.userLocation = [];
      }
    });

    AsyncStorage.getItem('KeyUserInfo').then(userInfo => {
      global.userInfo = JSON.parse(userInfo);
      if (!global.userInfo) {
        global.userInfo = {};
      }
      global.googleApiKey = 'AIzaSyBl1EuEKw10881MSRV_SNNQvfkJvBSuZPU';
      axios.defaults.headers.common.Authorization =
        'Bearer ' + global.accessToken;

      //  if (global.userInfo !== null) {
      this.props.fetchAllSplashData();
      // } else {
      //   setTimeout(() => {
      //     NavigationService.navigateReset('Walkthrough');
      //   }, 1000);
      // }
    });
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
        <View style={styles.logo}>
          <Image
            style={{width: global.screenWidth - 130, height: 150}}
            source={Images.logo}
            resizeMode="contain"
          />

          <Text
            style={{
              fontSize: 15,
              color: Colors.grayDark,
              fontFamily: 'Nunito-SemiBold',
              marginTop: 12,
            }}>
            Version: 0.0.1 (1)
          </Text>
          <View style={{height: 35, marginTop: 30}}>
            <BarIndicator color={Colors.primaryDark} size={22} count={5} />
          </View>
        </View>

        <View style={{position: 'absolute', bottom: -40, right: -75}}>
          <Image
            style={{width: global.screenWidth, height: 250}}
            source={Images.splashMeds}
            resizeMode="contain"
          />
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    splashScreen: state.splashScreen,
  };
}

export default connect(
  mapStateToProps,
  {fetchAllSplashData},
)(SplashScreen);

const styles = StyleSheet.create({
  imageBackground: {
    flex: 1,
  },
  logo: {
    flex: 1,
    top: 0,
    bottom: 80,
    left: 0,
    right: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
