import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import LoadingIndicator from '../components/activityIndicator';
import Modal from 'react-native-modal';

import NavigationService from '../route/navigationService';
import {BarIndicator} from 'react-native-indicators';

import {connect} from 'react-redux';
import {fetchCartData, searchTextbox} from '../actions/shopActions';

import Images from '../res/images';
import Icon from '../res/icons';
import Colors from '../res/colors';
import CustomTextInput from '../components/textInput';
import CustomButton from '../components/button';
import HeaderDashboard from '../components/headerDashboard';

class Checkout extends React.Component {
  constructor(props) {
    super(props);
    this.itemsArray = [];
    this.orderData = this.props.navigation.getParam('order').order;

    var items = JSON.parse(this.orderData.cart).items;
    var key = '';

    for (var i = 0; i < 1000; i++) {
      key = Object.keys(items)[i];

      if (key) {
        this.itemsArray.push(items[key]);
      } else {
        break;
      }
    }
  }
  flatListHeader = () => {
    return (
      <View>
        <Text
          style={{
            fontFamily: 'Nunito-ExtraBold',
            fontSize: 18,
            color: Colors.black,
            marginTop: 20,
            marginLeft: 16,
          }}>
          Order Details
        </Text>

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginHorizontal: 16,
            marginTop: 12,
          }}>
          <Text
            numberOfLines={2}
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 12,
              color: Colors.black,
            }}>
            Product Name
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <Text
              numberOfLines={2}
              style={{
                width: 50,
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.black,
                textAlign: 'center',
              }}>
              Unit
            </Text>
            <Text
              numberOfLines={2}
              style={{
                width: 50,
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.black,
                textAlign: 'center',
              }}>
              Qty.
            </Text>
            <Text
              numberOfLines={2}
              style={{
                width: 60,
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.black,
                textAlign: 'center',
              }}>
              Sub Total
            </Text>
          </View>
        </View>
        <View
          style={{
            height: 0.5,
            backgroundColor: Colors.border,
            marginVertical: 8,
            marginHorizontal: 16,
          }}
        />
      </View>
    );
  };

  flatListFooter = () => {
    return (
      <View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginHorizontal: 16,
          }}>
          <Text
            numberOfLines={2}
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 12,
              color: Colors.black,
            }}>
            Price Discount
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <Text
              numberOfLines={2}
              style={{
                width: 50,
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.black,
                textAlign: 'center',
              }}>
              -
            </Text>
            <Text
              numberOfLines={2}
              style={{
                width: 50,
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.black,
                textAlign: 'center',
              }}>
              -
            </Text>
            <Text
              numberOfLines={2}
              style={{
                width: 60,
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.black,
                textAlign: 'center',
              }}>
              Rs 30
            </Text>
          </View>
        </View>
        <View
          style={{
            height: 0.5,
            backgroundColor: Colors.border,
            marginVertical: 8,
            marginHorizontal: 16,
          }}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginHorizontal: 16,
          }}>
          <Text
            numberOfLines={2}
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 12,
              color: Colors.black,
            }}>
            Delivery Charge
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <Text
              numberOfLines={2}
              style={{
                width: 50,
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.black,
                textAlign: 'center',
              }}>
              -
            </Text>
            <Text
              numberOfLines={2}
              style={{
                width: 50,
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.black,
                textAlign: 'center',
              }}>
              -
            </Text>
            <Text
              numberOfLines={2}
              style={{
                width: 60,
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.black,
                textAlign: 'center',
              }}>
              Rs {this.orderData.shipping_cost}
            </Text>
          </View>
        </View>
        <View
          style={{
            height: 0.5,
            backgroundColor: Colors.border,
            marginVertical: 8,
            marginHorizontal: 16,
          }}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginHorizontal: 16,
          }}>
          <Text
            numberOfLines={2}
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 16,
              color: Colors.black,
            }}>
            Total
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <Text
              numberOfLines={2}
              style={{
                width: 50,
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.black,
                textAlign: 'center',
              }}>
              -
            </Text>
            <Text
              numberOfLines={2}
              style={{
                width: 50,
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.black,
                textAlign: 'center',
              }}>
              -
            </Text>
            <Text
              numberOfLines={2}
              style={{
                width: 60,
                fontFamily: 'Nunito-Bold',
                fontSize: 16,
                color: Colors.black,
                textAlign: 'center',
              }}>
              Rs {this.orderData.pay_amount}
            </Text>
          </View>
        </View>
        <View
          style={{
            height: 0.5,
            backgroundColor: Colors.border,
            marginVertical: 8,
            marginHorizontal: 16,
          }}
        />

        <Text
          style={{
            fontFamily: 'Nunito-ExtraBold',
            fontSize: 18,
            color: Colors.black,
            marginHorizontal: 16,
            marginTop: 20,
          }}>
          Prescription Details
        </Text>

        <FlatList
          contentContainerStyle={{
            paddingTop: 10,
            paddingLeft: 8,
            paddingRight: 12,
          }}
          ListEmptyComponent={() => (
            <Text
              numberOfLines={2}
              style={{
                fontFamily: 'Nunito-SemiBold',
                fontSize: 12,
                color: Colors.grayDark,
                marginLeft: 8,
              }}>
              No Prescription is required.
            </Text>
          )}
          showsHorizontalScrollIndicator={false}
          horizontal
          data={[]}
          renderItem={({item, index}) => {
            return (
              <View
                style={{
                  width: 90,
                  elevation: 5,
                  marginVertical: 10,
                  backgroundColor: Colors.white,
                  borderRadius: 12,
                  marginRight: 4,
                  marginLeft: 8,
                }}>
                <TouchableOpacity
                  activeOpacity={0.75}
                  onPress={() => {
                    NavigationService.navigate('ProductDetail');
                  }}
                  style={{
                    borderRadius: 20,
                    padding: 12,
                  }}>
                  <Image
                    source={item.image}
                    style={{
                      width: '100%',
                      height: 80,
                      paddingHorizontal: 12,
                      borderRadius: 4,
                    }}
                  />
                </TouchableOpacity>
              </View>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <LoadingIndicator isVisible={this.props.shop.loading} />

        <HeaderDashboard title="Checkout" noCart={true} />

        <View
          style={{
            flex: 1,
            backgroundColor: Colors.white,
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            marginTop: -20,
          }}>
          <FlatList
            ListHeaderComponent={this.flatListHeader}
            ListFooterComponent={this.flatListFooter}
            data={this.itemsArray}
            renderItem={({item, index}) => {
              return (
                <View
                  style={{
                    marginHorizontal: 16,
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}>
                    <Text
                      numberOfLines={2}
                      style={{
                        flex: 1,
                        fontFamily: 'Nunito-Bold',
                        fontSize: 12,
                        color: Colors.grayMedium,
                      }}>
                      {item.item.name}
                    </Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        numberOfLines={2}
                        style={{
                          width: 50,
                          fontFamily: 'Nunito-Bold',
                          fontSize: 12,
                          color: Colors.grayMedium,
                          textAlign: 'center',
                        }}>
                        {item.price}
                      </Text>
                      <Text
                        numberOfLines={2}
                        style={{
                          width: 50,
                          fontFamily: 'Nunito-Bold',
                          fontSize: 12,
                          color: Colors.grayMedium,
                          textAlign: 'center',
                        }}>
                        {item.qty}
                      </Text>
                      <Text
                        numberOfLines={2}
                        style={{
                          width: 60,
                          fontFamily: 'Nunito-Bold',
                          fontSize: 12,
                          color: Colors.grayMedium,
                          textAlign: 'center',
                        }}>
                        {parseFloat(item.qty * item.price)}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      height: 0.5,
                      backgroundColor: Colors.border,
                      marginVertical: 8,
                    }}
                  />
                </View>
              );
            }}
            keyExtractor={(item, index) => index.toString()}
          />

          <View
            style={{
              position: 'absolute',
              bottom: 0,
              left: 0,
              right: 0,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: Colors.white,
              elevation: 3,
              paddingVertical: 20,
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              height: 90,
            }}>
            <CustomButton.PrimaryButton
              title="Pay Now"
              onPress={() => {
                NavigationService.navigate('PayNow', {order: this.orderData});
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    shop: state.shop,
  };
}

export default connect(
  mapStateToProps,
  {fetchCartData, searchTextbox},
)(Checkout);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.redDark,
  },
  logo: {
    width: 110,
  },
});
