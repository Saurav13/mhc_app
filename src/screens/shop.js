import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
  RefreshControl,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import LoadingIndicator from '../components/activityIndicator';
import {BarIndicator} from 'react-native-indicators';

import {connect} from 'react-redux';
import {
  fetchShopData,
  searchTextbox,
  searchProducts,
  clearSearch,
} from '../actions/shopActions';
import {calculatePricePercentage} from '../utlis/calculatePricePercentage';
import {addToCart} from '../actions/CartAction';

import Images from '../res/images';
import Icons from '../res/icons';
import Colors from '../res/colors';
import CustomTextInput from '../components/textInput';
import CustomButton from '../components/button';
import HeaderDashboard from '../components/headerDashboard';
import SearchProductsDropdown from '../components/searchProductsDropdown';
import NavigationService from '../route/navigationService';

class Shop extends React.Component {
  onRefresh = () => {
    this.props.fetchShopData();
  };

  flatListHeader = () => {
    return (
      <View>
        <FlatList
          contentContainerStyle={{paddingLeft: 8, paddingRight: 12}}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={global.homeData.sliders}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                activeOpacity={0.75}
                onPress={() => {}}
                style={{marginRight: 4, marginLeft: 8, borderRadius: 20}}>
                <Image
                  source={{uri: global.imageUri + item.photo}}
                  style={{
                    width: global.screenWidth - 60,
                    height: 180,
                    borderRadius: 20,
                  }}
                />
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
        <Text
          style={{
            fontFamily: 'Nunito-ExtraBold',
            fontSize: 18,
            color: Colors.black,
            marginTop: 20,
            marginLeft: 16,
          }}>
          Categories
        </Text>
        <FlatList
          contentContainerStyle={{
            paddingTop: 4,
            paddingLeft: 8,
            paddingRight: 12,
          }}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={global.homeData.categories}
          renderItem={({item, index}) => {
            return (
              <CustomButton.SecondaryButton
                title={item.cat_name}
                style={{marginVertical: 8, marginLeft: 8}}
                textStyle={{fontSize: 14}}
                onPress={() => {
                  NavigationService.navigate('Category', {
                    categoryData: item,
                  });
                }}
              />
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
        <View
          style={{
            borderRadius: 20,
            marginHorizontal: 16,
            marginTop: 20,
            backgroundColor: Colors.primaryLightest,
            padding: 16,
          }}>
          <Text
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 16,
              color: Colors.black,
            }}>
            {global.homeData.ads[0].client}
          </Text>
          <Text
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 14,
              color: Colors.grayDark,
              lineHeight: 24,
              marginTop: 8,
            }}>
            {global.homeData.ads[0].review}
          </Text>
          {/*        <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
              marginTop: 8,
            }}>

            <CustomButton.SecondaryButton title="Upload" onPress={() => {}} />
          </View> */}
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginTop: 20,
            marginHorizontal: 16,
          }}>
          <Text
            style={{
              fontFamily: 'Nunito-ExtraBold',
              fontSize: 18,
              color: Colors.black,
            }}>
            Featured Products
          </Text>
          <TouchableOpacity activeOpacity={0.75} onPress={() => {}}>
            <Text
              style={{
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.grayMedium,
              }}>
              See All
            </Text>
          </TouchableOpacity>
        </View>

        {/**Featured Products */}
        <FlatList
          contentContainerStyle={{
            paddingTop: 10,
            paddingLeft: 8,
            paddingRight: 12,
          }}
          showsHorizontalScrollIndicator={false}
          horizontal
          data={global.homeData.featured_products}
          renderItem={({item, index}) => {
            return (
              <View
                style={{
                  width: 140,
                  elevation: 5,
                  marginVertical: 10,
                  backgroundColor: Colors.white,
                  borderRadius: 12,
                  marginRight: 4,
                  marginLeft: 8,
                }}>
                <TouchableOpacity
                  activeOpacity={0.75}
                  onPress={() => {
                    NavigationService.navigate('ProductDetail', {
                      productDetail: item,
                    });
                  }}
                  style={{
                    borderRadius: 20,
                    padding: 12,
                  }}>
                  {item.pprice && item.cprice != item.pprice && (
                    <View
                      style={{
                        position: 'absolute',
                        top: 16,
                        left: 16,
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: 16,
                        borderRadius: 6,
                        backgroundColor: Colors.greenLight,
                        zIndex: 1,
                        paddingHorizontal: 8,
                      }}>
                      <Text
                        numberOfLines={1}
                        style={{
                          fontFamily: 'Nunito-Bold',
                          fontSize: 10,
                          color: Colors.white,
                        }}>
                        {calculatePricePercentage(item.cprice, item.pprice)}%
                        off
                      </Text>
                    </View>
                  )}
                  <Image
                    source={{uri: global.imageUri + item.photo}}
                    style={{
                      width: '100%',
                      height: 120,
                      paddingHorizontal: 12,
                      borderRadius: 8,
                    }}
                    resizeMode="contain"
                  />
                  <Text
                    numberOfLines={1}
                    style={{
                      fontFamily: 'Nunito-SemiBold',
                      fontSize: 12,
                      color: Colors.black,
                      marginTop: 8,
                    }}>
                    {item.name}
                  </Text>

                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      marginTop: 8,
                    }}>
                    <Text
                      style={{
                        fontFamily: 'Nunito-Bold',
                        fontSize: 14,
                        color: Colors.primaryMedium,
                      }}>
                      Rs {item.cprice}
                    </Text>
                    {item.cprice != item.pprice && (
                      <TouchableOpacity activeOpacity={0.75} onPress={() => {}}>
                        <Text
                          style={{
                            fontFamily: 'Nunito-Bold',
                            fontSize: 9,
                            color: Colors.redDark,
                            textDecorationLine: 'line-through',
                            textDecorationStyle: 'solid',
                          }}>
                          Rs {item.pprice}
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                </TouchableOpacity>
              </View>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginTop: 20,
            marginHorizontal: 16,
          }}>
          <Text
            style={{
              fontFamily: 'Nunito-ExtraBold',
              fontSize: 18,
              color: Colors.black,
            }}>
            Best Sellers
          </Text>
          <TouchableOpacity activeOpacity={0.75} onPress={() => {}}>
            <Text
              style={{
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.grayMedium,
              }}>
              See All
            </Text>
          </TouchableOpacity>
        </View>

        {/** Best products */}
        <FlatList
          contentContainerStyle={{
            paddingTop: 10,
            paddingLeft: 8,
            paddingRight: 12,
          }}
          showsHorizontalScrollIndicator={false}
          horizontal
          data={global.homeData.best_products}
          renderItem={({item, index}) => {
            return (
              <View
                style={{
                  width: 140,
                  elevation: 5,
                  marginVertical: 10,
                  backgroundColor: Colors.white,
                  borderRadius: 12,
                  marginRight: 4,
                  marginLeft: 8,
                }}>
                <TouchableOpacity
                  activeOpacity={0.75}
                  onPress={() => {
                    NavigationService.navigate('ProductDetail', {
                      productDetail: item,
                    });
                  }}
                  style={{
                    borderRadius: 20,
                    padding: 12,
                  }}>
                  {item.pprice && item.cprice != item.pprice && (
                    <View
                      style={{
                        position: 'absolute',
                        top: 16,
                        left: 16,
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: 16,
                        borderRadius: 6,
                        backgroundColor: Colors.greenLight,
                        zIndex: 1,
                        paddingHorizontal: 8,
                      }}>
                      <Text
                        numberOfLines={1}
                        style={{
                          fontFamily: 'Nunito-Bold',
                          fontSize: 10,
                          color: Colors.white,
                        }}>
                        {calculatePricePercentage(item.cprice, item.pprice)}%
                        off
                      </Text>
                    </View>
                  )}
                  <Image
                    source={{uri: global.imageUri + item.photo}}
                    style={{
                      width: '100%',
                      height: 120,
                      paddingHorizontal: 12,
                      borderRadius: 8,
                    }}
                    resizeMode="contain"
                  />
                  <Text
                    numberOfLines={1}
                    style={{
                      fontFamily: 'Nunito-SemiBold',
                      fontSize: 12,
                      color: Colors.black,
                      marginTop: 8,
                    }}>
                    {item.name}
                  </Text>

                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      marginTop: 8,
                    }}>
                    <Text
                      style={{
                        fontFamily: 'Nunito-Bold',
                        fontSize: 14,
                        color: Colors.primaryMedium,
                      }}>
                      Rs {item.cprice}
                    </Text>
                    {item.cprice != item.pprice && (
                      <TouchableOpacity activeOpacity={0.75} onPress={() => {}}>
                        <Text
                          style={{
                            fontFamily: 'Nunito-Bold',
                            fontSize: 9,
                            color: Colors.redDark,
                            textDecorationLine: 'line-through',
                            textDecorationStyle: 'solid',
                          }}>
                          Rs {item.pprice}
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                </TouchableOpacity>
              </View>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginTop: 20,
            marginHorizontal: 16,
          }}>
          <Text
            style={{
              fontFamily: 'Nunito-ExtraBold',
              fontSize: 18,
              color: Colors.black,
            }}>
            Top Rated
          </Text>
          <TouchableOpacity activeOpacity={0.75} onPress={() => {}}>
            <Text
              style={{
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.grayMedium,
              }}>
              See All
            </Text>
          </TouchableOpacity>
        </View>

        {/**Top Rated */}
        <FlatList
          contentContainerStyle={{
            paddingTop: 10,
            paddingLeft: 8,
            paddingRight: 12,
          }}
          showsHorizontalScrollIndicator={false}
          horizontal
          data={global.homeData.top_products}
          renderItem={({item, index}) => {
            return (
              <View
                style={{
                  width: 140,
                  elevation: 5,
                  marginVertical: 10,
                  backgroundColor: Colors.white,
                  borderRadius: 12,
                  marginRight: 4,
                  marginLeft: 8,
                }}>
                <TouchableOpacity
                  activeOpacity={0.75}
                  onPress={() => {
                    NavigationService.navigate('ProductDetail', {
                      productDetail: item,
                    });
                  }}
                  style={{
                    borderRadius: 20,
                    padding: 12,
                  }}>
                  {item.pprice && item.cprice != item.pprice && (
                    <View
                      style={{
                        position: 'absolute',
                        top: 16,
                        left: 16,
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: 16,
                        borderRadius: 6,
                        backgroundColor: Colors.greenLight,
                        zIndex: 1,
                        paddingHorizontal: 8,
                      }}>
                      <Text
                        numberOfLines={1}
                        style={{
                          fontFamily: 'Nunito-Bold',
                          fontSize: 10,
                          color: Colors.white,
                        }}>
                        {calculatePricePercentage(item.cprice, item.pprice)}%
                        off
                      </Text>
                    </View>
                  )}
                  <Image
                    source={{uri: global.imageUri + item.photo}}
                    style={{
                      width: '100%',
                      height: 120,
                      paddingHorizontal: 12,
                      borderRadius: 8,
                    }}
                    resizeMode="contain"
                  />
                  <Text
                    numberOfLines={1}
                    style={{
                      fontFamily: 'Nunito-SemiBold',
                      fontSize: 12,
                      color: Colors.black,
                      marginTop: 8,
                    }}>
                    {item.name}
                  </Text>

                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      marginTop: 8,
                    }}>
                    <Text
                      style={{
                        fontFamily: 'Nunito-Bold',
                        fontSize: 14,
                        color: Colors.primaryMedium,
                      }}>
                      Rs {item.cprice}
                    </Text>
                    {item.cprice != item.pprice && (
                      <TouchableOpacity activeOpacity={0.75} onPress={() => {}}>
                        <Text
                          style={{
                            fontFamily: 'Nunito-Bold',
                            fontSize: 9,
                            color: Colors.redDark,
                            textDecorationLine: 'line-through',
                            textDecorationStyle: 'solid',
                          }}>
                          Rs {item.pprice}
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                </TouchableOpacity>
              </View>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginTop: 20,
            marginHorizontal: 16,
          }}>
          <Text
            style={{
              fontFamily: 'Nunito-ExtraBold',
              fontSize: 18,
              color: Colors.black,
            }}>
            Hot Sale
          </Text>
          <TouchableOpacity activeOpacity={0.75} onPress={() => {}}>
            <Text
              style={{
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.grayMedium,
              }}>
              See All
            </Text>
          </TouchableOpacity>
        </View>

        {/**Top Rated */}
        <FlatList
          contentContainerStyle={{
            paddingTop: 10,
            paddingLeft: 8,
            paddingRight: 12,
          }}
          showsHorizontalScrollIndicator={false}
          horizontal
          data={global.homeData.hot_products}
          renderItem={({item, index}) => {
            return (
              <View
                style={{
                  width: 140,
                  elevation: 5,
                  marginVertical: 10,
                  backgroundColor: Colors.white,
                  borderRadius: 12,
                  marginRight: 4,
                  marginLeft: 8,
                }}>
                <TouchableOpacity
                  activeOpacity={0.75}
                  onPress={() => {
                    NavigationService.navigate('ProductDetail', {
                      productDetail: item,
                    });
                  }}
                  style={{
                    borderRadius: 20,
                    padding: 12,
                  }}>
                  {item.pprice && item.cprice != item.pprice && (
                    <View
                      style={{
                        position: 'absolute',
                        top: 16,
                        left: 16,
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: 16,
                        borderRadius: 6,
                        backgroundColor: Colors.greenLight,
                        zIndex: 1,
                        paddingHorizontal: 8,
                      }}>
                      <Text
                        numberOfLines={1}
                        style={{
                          fontFamily: 'Nunito-Bold',
                          fontSize: 10,
                          color: Colors.white,
                        }}>
                        {calculatePricePercentage(item.cprice, item.pprice)}%
                        off
                      </Text>
                    </View>
                  )}
                  <Image
                    source={{uri: global.imageUri + item.photo}}
                    style={{
                      width: '100%',
                      height: 120,
                      paddingHorizontal: 12,
                      borderRadius: 8,
                    }}
                    resizeMode="contain"
                  />
                  <Text
                    numberOfLines={1}
                    style={{
                      fontFamily: 'Nunito-SemiBold',
                      fontSize: 12,
                      color: Colors.black,
                      marginTop: 8,
                    }}>
                    {item.name}
                  </Text>

                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      marginTop: 8,
                    }}>
                    <Text
                      style={{
                        fontFamily: 'Nunito-Bold',
                        fontSize: 14,
                        color: Colors.primaryMedium,
                      }}>
                      Rs {item.cprice}
                    </Text>
                    {item.cprice != item.pprice && (
                      <TouchableOpacity activeOpacity={0.75} onPress={() => {}}>
                        <Text
                          style={{
                            fontFamily: 'Nunito-Bold',
                            fontSize: 9,
                            color: Colors.redDark,
                            textDecorationLine: 'line-through',
                            textDecorationStyle: 'solid',
                          }}>
                          Rs {item.pprice}
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                </TouchableOpacity>
              </View>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />

        {/**  <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginTop: 20,
            marginHorizontal: 16,
            marginBottom: 12,
          }}>
          <Text
            style={{
              fontFamily: 'Nunito-ExtraBold',
              fontSize: 18,
              color: Colors.black,
            }}>
            Featured Blogs
          </Text>
          <TouchableOpacity activeOpacity={0.75} onPress={() => { }}>
            <Text
              style={{
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.grayMedium,
              }}>
              See All
            </Text>
          </TouchableOpacity>
        </View> */}
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <LoadingIndicator isVisible={this.props.shop.loading} />
        <HeaderDashboard />

        <View
          style={{
            top: 130,
            right: 0,
            left: 0,
            marginBottom: 16,
            position: 'absolute',
            zIndex: 999,
          }}>
          <SearchProductsDropdown />
        </View>

        {/**for featured blogs */}
        <FlatList
          contentContainerStyle={{paddingBottom: 16, paddingTop: 60}}
          ListHeaderComponent={this.flatListHeader}
          showsVerticleScrollIndicator={false}
          data={[]}
          refreshControl={
            <RefreshControl
              refreshing={this.props.shop.refreshing}
              onRefresh={() => this.onRefresh()}
            />
          }
          renderItem={({item, index}) => {
            return (
              <View
                style={{
                  height: 100,
                  elevation: 5,
                  marginVertical: 8,
                  backgroundColor: Colors.white,
                  borderRadius: 12,
                  marginHorizontal: 16,
                }}>
                <TouchableOpacity
                  activeOpacity={0.75}
                  onPress={() => {}}
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    borderRadius: 20,
                    padding: 12,
                  }}>
                  <Image
                    source={item.image}
                    style={{
                      width: 120,
                      height: 76,
                      paddingHorizontal: 12,
                      borderRadius: 12,
                    }}
                    resizeMode="contain"
                  />
                  <View style={{flex: 1, marginLeft: 8}}>
                    <Text
                      numberOfLines={1}
                      style={{
                        fontFamily: 'Nunito-SemiBold',
                        fontSize: 12,
                        color: Colors.black,
                      }}>
                      Accu-Check Performa
                    </Text>

                    <Text
                      style={{
                        fontFamily: 'Nunito-Bold',
                        fontSize: 9,
                        color: Colors.primaryMedium,
                      }}>
                      6th March, 2020
                    </Text>
                    <Text
                      numberOfLines={3}
                      style={{
                        fontFamily: 'Nunito-Regular',
                        fontSize: 9,
                        color: Colors.grayMedium,
                      }}>
                      Lorem ipsum, or lipsum as it is sometimes known, is dummy
                      text used in laying out print, graphic or web designs.
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    shop: state.shop,
    cartCount: state.cart.cartCount,
  };
}

export default connect(
  mapStateToProps,
  {fetchShopData, searchTextbox, searchProducts, clearSearch, addToCart},
)(Shop);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  logo: {
    width: 110,
  },
});
