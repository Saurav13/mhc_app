import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  ToastAndroid,
  Alert,
} from 'react-native';

import {connect} from 'react-redux';
import {
  emailLoginTextBox,
  passwordLoginTextBox,
  saveLoginData,
} from '../actions/loginActions';

import CustomButton from '../components/button';
import CustomTextInput from '../components/textInput';
import NavigationService from '../route/navigationService';
import LoadingIndicator from '../components/activityIndicator';
import HeaderAuth from '../components/headerAuth';
import Colors from '../res/colors';
import Icons from '../res/icons';
import Images from '../res/images';

class Login extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <LoadingIndicator isVisible={this.props.login.loading} />
        <ScrollView
          keyboardShouldPersistTaps="handled"
          style={{backgroundColor: 'white'}}>
          <HeaderAuth />

          <View
            style={{
              backgroundColor: Colors.white,
              borderTopLeftRadius: 50,
              borderTopRightRadius: 50,
              marginTop: -50,
              paddingHorizontal: 20,
              paddingTop: 32,
            }}>
            <Text
              style={{
                fontFamily: 'Nunito-Bold',
                fontSize: 26,
                color: Colors.blackDark,
                marginVertical: 15,
                textAlign: 'center',
              }}>
              Login
            </Text>
            <CustomTextInput.MainTextInput
              label="Email"
              value={this.props.login.email}
              onChangeText={text => this.props.emailLoginTextBox(text)}
              keyboardType="default"
            />
            <CustomTextInput.MainTextInput
              label="Password"
              value={this.props.login.password}
              onChangeText={text => this.props.passwordLoginTextBox(text)}
              keyboardType="default"
              typePassword={true}
            />
            <TouchableOpacity
              activeOpacity={0.75}
              onPress={() => {
                NavigationService.navigate('ForgotPassword');
              }}>
              <Text
                style={{
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 12,
                  color: Colors.primaryDark,
                }}>
                Forgot your Password?
              </Text>
            </TouchableOpacity>

            <View style={{marginTop: 32, alignItems: 'center'}}>
              <CustomButton.PrimaryButton
                title="Login"
                onPress={() => {
                  var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                  if (
                    this.props.login.email != '' &&
                    this.props.login.password != ''
                  ) {
                    if (reg.test(this.props.login.email) === true) {
                      this.props.saveLoginData(
                        this.props.login.email,
                        this.props.login.password,
                      );
                    } else {
                      ToastAndroid.showWithGravityAndOffset(
                        'Please enter the valid email.',
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        0,
                        50,
                      );
                    }
                  } else {
                    ToastAndroid.showWithGravityAndOffset(
                      'Enter valid login credentials.',
                      ToastAndroid.LONG,
                      ToastAndroid.BOTTOM,
                      0,
                      50,
                    );
                  }
                }}
              />
            </View>

            <Text
              style={{
                fontFamily: 'Nunito-SemiBold',
                fontSize: 14,
                color: Colors.grayDark,
                marginTop: 35,
                textAlign: 'center',
              }}>
              Or Connect With
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 20,
              }}>
              <View
                style={{
                  width: 42,
                  height: 42,
                  borderRadius: 21,
                  backgroundColor: 'rgba(59,89,152,1)',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginRight: 7.5,
                }}>
                <Icons.FontAwesome5 name="facebook-f" size={20} color="white" />
              </View>

              <View
                style={{
                  width: 42,
                  height: 42,
                  borderRadius: 21,
                  backgroundColor: 'rgba(180,10,2,1)',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginLeft: 7.5,
                }}>
                <Icons.FontAwesome5 name="google" size={20} color="white" />
              </View>
            </View>

            <View>
              <Text
                style={{
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 14,
                  color: Colors.grayDark,
                  marginTop: 40,
                  textAlign: 'center',
                }}>
                Don't Have An Account ?
              </Text>

              <TouchableOpacity
                onPress={() => {
                  NavigationService.navigate('Signup');
                }}
                activeOpacity={0.7}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginBottom: 30,
                }}>
                <Text
                  style={{
                    fontFamily: 'Nunito-Bold',
                    fontSize: 14,
                    color: Colors.primaryDark,
                    marginTop: 4,
                    textAlign: 'center',
                  }}>
                  Register Now
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

export default connect(
  mapStateToProps,
  {
    emailLoginTextBox,
    passwordLoginTextBox,
    saveLoginData,
  },
)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo: {
    marginTop: 10,
    width: 140,
    height: 80,
  },
});
