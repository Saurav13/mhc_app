import React from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';

import NavigationService from '../route/navigationService';
import colors from '../res/colors';
import Images from '../res/images';
import HeaderAuth from '../components/headerAuth';

export default class Partners extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <HeaderAuth noLogo={true} />
        <View
          style={{
            position: 'absolute',
            top: 200,
            right: 0,
            left: 0,
            alignItems: 'center',
          }}>
          <Image
            style={{width: global.screenWidth - 130, height: 150}}
            source={Images.logo}
            resizeMode="contain"
          />
        </View>
        <View style={{position: 'absolute', bottom: 100, right: 20, left: 20}}>
          <TouchableOpacity
            onPress={() => {
              NavigationService.navigate('Login');
            }}
            activeOpacity={0.75}
            style={{
              backgroundColor: 'white',
              height: 50,
              borderRadius: 20,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'Nunito-Bold',
                fontSize: 16,
                color: colors.primaryMedium,
              }}>
              Login
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              NavigationService.navigate('Signup');
            }}
            activeOpacity={0.75}
            style={{
              backgroundColor: 'white',
              height: 50,
              borderRadius: 20,
              marginTop: 20,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'Nunito-Bold',
                fontSize: 16,
                color: colors.primaryMedium,
              }}>
              Register Now
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primaryMedium,
  },
  logo: {
    flex: 1,
    top: 0,
    bottom: 80,
    left: 0,
    right: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
