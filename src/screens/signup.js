import React from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ToastAndroid,
  Alert,
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Moment from 'moment';
import AlertToast from '../components/alertToast';

import {connect} from 'react-redux';
import {
  emailSignupTextBox,
  NameSignupTextBox,
  dobSignupTextBox,
  genderSignupTextBox,
  addressSignupTextBox,
  phoneSignupTextBox,
  passwordSignupTextBox,
  confirmPasswordSignupTextBox,
  saveSignupData,
} from '../actions/signupActions';

import CustomButton from '../components/button';
import CustomTextInput from '../components/textInput';
import NavigationService from '../route/navigationService';
import Colors from '../res/colors';
import Images from '../res/images';
import Icon from '../res/icons';
import LoadingIndicator from '../components/activityIndicator';
import HeaderAuth from '../components/headerAuth';

class Signup extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showDateModal: false,
    };
  }

  setDate = (event, date) => {
    if (event.type == 'set') {
      var formatedDate = Moment(date).format('MM/DD/YYYY');

      this.setState({showDateModal: false});
      this.props.dobSignupTextBox(formatedDate);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <LoadingIndicator isVisible={this.props.loading} />

        {this.state.showDateModal && (
          <DateTimePicker
            value={new Date()}
            mode={global.OS == 1 ? 'date' : 'datetime'}
            is24Hour={false}
            display="default"
            onChange={this.setDate}
            style={{width: '100%', backgroundColor: Colors.white}}
          />
        )}

        <ScrollView keyboardShouldPersistTaps="handled">
          <HeaderAuth />

          <View
            style={{
              backgroundColor: Colors.white,
              borderTopLeftRadius: 50,
              borderTopRightRadius: 50,
              marginTop: -50,
              paddingHorizontal: 20,
              paddingTop: 32,
            }}>
            <Text
              style={{
                fontFamily: 'Nunito-Bold',
                fontSize: 22,
                color: Colors.blackDark,
                textAlign: 'center',
              }}>
              Register
            </Text>
            <CustomTextInput.MainTextInput
              label="Email"
              value={this.props.email}
              onChangeText={text => this.props.emailSignupTextBox(text)}
              keyboardType="default"
            />

            <CustomTextInput.MainTextInput
              label="Name"
              value={this.props.name}
              onChangeText={text => this.props.NameSignupTextBox(text)}
              keyboardType="default"
            />
            <CustomTextInput.MainTextInput
              label="Address"
              value={this.props.address}
              onChangeText={text => this.props.addressSignupTextBox(text)}
              keyboardType="default"
            />

            <CustomTextInput.MainTextInput
              label="Mobile Number"
              value={this.props.phone}
              onChangeText={text => this.props.phoneSignupTextBox(text)}
              keyboardType="number-pad"
            />

            <TouchableOpacity
              activeOpacity={0.75}
              onPress={() => {
                this.setState({showDateModal: true});
              }}
              style={
                this.props.dob == ''
                  ? {marginTop: 16, paddingTop: 8, marginBottom: 8}
                  : {marginTop: 4, paddingTop: 8, marginBottom: 8}
              }>
              <Text
                style={{
                  fontFamily: 'Nunito-Regular',
                  fontSize: 12,
                  color: Colors.grayDark,
                }}>
                Date of Birth
              </Text>
              <Text
                style={
                  this.props.dob == ''
                    ? {
                        fontFamily: 'Nunito-SemiBold',
                        fontSize: 12,
                        color: Colors.grayDarkest,
                      }
                    : {
                        fontFamily: 'Nunito-SemiBold',
                        fontSize: 12,
                        color: Colors.grayDarkest,
                        marginTop: 4,
                      }
                }>
                {this.props.dob}
              </Text>
              <View
                style={
                  this.props.dob == ''
                    ? {
                        height: 1,
                        backgroundColor: 'rgba(110,110,110,1)',
                        marginTop: 0,
                      }
                    : {
                        height: 1,
                        backgroundColor: 'rgba(110,110,110,1)',
                        marginTop: 8,
                      }
                }
              />
            </TouchableOpacity>

            <CustomTextInput.MainTextInput
              label="Password"
              value={this.props.password}
              typePassword={true}
              onChangeText={text => this.props.passwordSignupTextBox(text)}
              keyboardType="default"
            />

            <CustomTextInput.MainTextInput
              label="Confirm Password"
              typePassword={true}
              value={this.props.confirmPassword}
              onChangeText={text =>
                this.props.confirmPasswordSignupTextBox(text)
              }
              keyboardType="default"
            />
            <Text
              style={{
                fontFamily: 'Nunito-SemiBold',
                fontSize: 10,
                color: Colors.grayMedium,
              }}>
              Must be 10 or more characters
            </Text>
            <Text
              style={{
                fontFamily: 'Nunito-Bold',
                fontSize: 12,
                color: Colors.grayMedium,
                marginTop: 20,
              }}>
              Gender
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 12,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.props.genderSignupTextBox([true, false, false]);
                }}
                activeOpacity={0.7}
                style={{
                  flexDirection: 'row',
                }}>
                <View>
                  <View
                    style={{
                      width: 16,
                      height: 16,
                      borderWidth: 1,
                      borderRadius: 8,
                      borderColor: 'rgba(34,41,47,1)',
                    }}
                  />
                  {this.props.gender[0] && (
                    <View
                      style={{
                        position: 'absolute',
                        top: 5,
                        left: 5,
                        width: 6,
                        height: 6,
                        borderRadius: 3,
                        backgroundColor: 'rgba(34,41,47,1)',
                      }}
                    />
                  )}
                </View>
                <Text
                  style={{
                    fontFamily: 'Nunito-SemiBold',
                    fontSize: 12,
                    color: 'rgba(34,41,47,1)',
                    marginLeft: 12,
                  }}>
                  Male
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.genderSignupTextBox([false, true, false]);
                }}
                activeOpacity={0.7}
                style={{
                  flexDirection: 'row',
                }}>
                <View>
                  <View
                    style={{
                      width: 16,
                      height: 16,
                      borderWidth: 1,
                      borderRadius: 8,
                      borderColor: 'rgba(34,41,47,1)',
                    }}
                  />
                  {this.props.gender[1] && (
                    <View
                      style={{
                        position: 'absolute',
                        top: 5,
                        left: 5,
                        width: 6,
                        height: 6,
                        borderRadius: 3,
                        backgroundColor: 'rgba(34,41,47,1)',
                      }}
                    />
                  )}
                </View>

                <Text
                  style={{
                    fontFamily: 'Nunito-SemiBold',
                    fontSize: 12,
                    color: 'rgba(34,41,47,1)',
                    marginLeft: 12,
                  }}>
                  Female
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.genderSignupTextBox([false, false, true]);
                }}
                activeOpacity={0.7}
                style={{
                  flexDirection: 'row',
                }}>
                <View>
                  <View
                    style={{
                      width: 16,
                      height: 16,
                      borderWidth: 1,
                      borderRadius: 8,
                      borderColor: 'rgba(34,41,47,1)',
                    }}
                  />
                  {this.props.gender[2] && (
                    <View
                      style={{
                        position: 'absolute',
                        top: 5,
                        left: 5,
                        width: 6,
                        height: 6,
                        borderRadius: 3,
                        backgroundColor: 'rgba(34,41,47,1)',
                      }}
                    />
                  )}
                </View>

                <Text
                  style={{
                    fontFamily: 'Nunito-SemiBold',
                    fontSize: 12,
                    color: 'rgba(34,41,47,1)',
                    marginLeft: 12,
                  }}>
                  Others
                </Text>
              </TouchableOpacity>
              <View />
            </View>
            <Text
              style={{
                fontFamily: 'Nunito-Regular',
                fontSize: 10,
                color: Colors.grayMedium,
                marginTop: 20,
                lineHeight: 20,
              }}>
              By creating your account, you agree to our{' '}
              <Text
                style={{
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 10,
                  color: Colors.blackLight,
                  marginTop: 20,
                  marginLeft: 3,
                  lineHeight: 20,
                  textDecorationLine: 'underline',
                }}>
                Terms and Conditions{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'Nunito-Regular',
                  fontSize: 10,
                  color: Colors.grayMedium,
                  marginTop: 20,
                  marginLeft: 3,
                  lineHeight: 20,
                }}>
                &{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 10,
                  color: Colors.blackLight,
                  marginTop: 20,
                  marginLeft: 3,
                  lineHeight: 20,
                  textDecorationLine: 'underline',
                }}>
                Privacy Policy
              </Text>
            </Text>

            <View style={{marginVertical: 30}}>
              <CustomButton.PrimaryButton
                title="Register Now"
                onPress={() => {
                  var valid = true;
                  var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                  var genderValue = '';

                  if (this.props.gender[0]) {
                    genderValue = 'Male';
                  } else if (this.props.gender[1]) {
                    genderValue = 'Female';
                  } else {
                    genderValue = 'Others';
                  }

                  if (!this.props.email) {
                    valid = false;
                    AlertToast('Please enter the email.');
                  } else {
                    if (!this.props.name) {
                      valid = false;
                      AlertToast('Please enter the name.');
                    } else {
                      if (!this.props.address) {
                        valid = false;
                        AlertToast('Please enter the address.');
                      } else {
                        if (!this.props.phone) {
                          valid = false;
                          AlertToast('Please enter the mobile number.');
                        } else {
                          if (!this.props.dob) {
                            valid = false;
                            AlertToast('Please select the date of birth.');
                          } else {
                            if (!this.props.password) {
                              valid = false;
                              AlertToast('Please enter the password.');
                            } else {
                              if (!this.props.confirmPassword) {
                                valid = false;
                                AlertToast(
                                  'Please enter the confirm password.',
                                );
                              } else {
                                if (reg.test(this.props.email) == false) {
                                  valid = false;
                                  AlertToast('Please enter the valid email.');
                                } else {
                                  if (this.props.password.length < 8) {
                                    valid = false;
                                    AlertToast(
                                      'Please enter the password at least 8 characters.',
                                    );
                                  } else {
                                    if (
                                      this.props.password !=
                                      this.props.confirmPassword
                                    ) {
                                      valid = false;
                                      AlertToast(
                                        'Please enter the same password and confirm password.',
                                      );
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }

                  if (valid) {
                    this.props.saveSignupData(
                      this.props.name,
                      this.props.dob,
                      genderValue,
                      this.props.phone,
                      this.props.address,
                      this.props.email,
                      this.props.password,
                      this.props.confirmPassword,
                    );
                  }
                }}
              />
            </View>

            <View>
              <Text
                style={{
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 14,
                  color: Colors.grayDark,
                  marginTop: 8,
                  textAlign: 'center',
                }}>
                Already Have account ?
              </Text>

              <TouchableOpacity
                onPress={() => {
                  NavigationService.navigate('Login');
                }}
                activeOpacity={0.7}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginBottom: 30,
                }}>
                <Text
                  style={{
                    fontFamily: 'Nunito-Bold',
                    fontSize: 14,
                    color: Colors.primaryDark,
                    marginTop: 4,
                    textAlign: 'center',
                  }}>
                  Login Now
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    email: state.signup.email,
    name: state.signup.name,
    dob: state.signup.dob,
    gender: state.signup.gender,
    address: state.signup.address,
    phone: state.signup.phone,
    password: state.signup.password,
    confirmPassword: state.signup.confirmPassword,
    loading: state.signup.loading,
  };
}

export default connect(
  mapStateToProps,
  {
    emailSignupTextBox,
    NameSignupTextBox,
    genderSignupTextBox,
    addressSignupTextBox,
    dobSignupTextBox,
    phoneSignupTextBox,
    passwordSignupTextBox,
    confirmPasswordSignupTextBox,
    saveSignupData,
  },
)(Signup);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  logo: {
    marginTop: 10,
    width: 140,
    height: 80,
  },
});
