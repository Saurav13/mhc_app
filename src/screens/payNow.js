import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import LoadingIndicator from '../components/activityIndicator';
import Modal from 'react-native-modal';

import NavigationService from '../route/navigationService';
import {BarIndicator} from 'react-native-indicators';
import * as Animatable from 'react-native-animatable';

import {connect} from 'react-redux';
import {createOrder} from '../actions/PayNowAction';

import {WebView} from 'react-native-webview';
import Images from '../res/images';
import Icon from '../res/icons';
import Colors from '../res/colors';
import CustomTextInput from '../components/textInput';
import CustomButton from '../components/button';
import HeaderDashboard from '../components/headerDashboard';

const INJECTEDJAVASCRIPT =
  "const meta = document.createElement('meta'); meta.setAttribute('content', 'width=device-width, initial-scale=1, maximum-scale=0.99, user-scalable=0'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta); ";

class PayNow extends React.Component {
  constructor(props) {
    super(props);

    this.orderData = this.props.navigation.getParam('order');

    this.state = {
      checked1: true,
      checked2: false,
      checked3: false,
      totalAmount: 300,
      userMobile: global.userInfo.phone || '',
      esewaUiStatus: false,
      khaltiUiStatus: false,
      headerTitle: 'Pay Now',
    };
  }

  render() {
    this.khaltiUI = `<html>
        <head>
          <script src="https://khalti.com/static/khalti-checkout.js"></script>
        </head>
        <body>
          <script>
            var config = {
              // replace the publicKey with yours
              "publicKey": "test_public_key_5e1e56887efc40d69fb2f88eff1628c7",
              "productIdentity": "123456789",
              "productName": "SajiloMEDS",
              "productUrl": "http://sajilomeds.test/checkout",
              "eventHandler": {
                onSuccess (payload) {
                  window.ReactNativeWebView.postMessage(JSON.stringify(payload));
                },
                onError (error) {
                  window.ReactNativeWebView.postMessage(JSON.stringify(error));
                },
                onClose () {
                  window.ReactNativeWebView.postMessage('onClose');
                }
              }
            };
            var checkout = new KhaltiCheckout(config);
            checkout.show({amount: ${this.state.totalAmount}, mobile: ${
      this.state.userMobile
    }});
          </script>
        </body>
        </html>`;

    this.esewaUI = `<html>
        <head>
        </head>
        <body>
          <form action = "https://uat.esewa.com.np/epay/main" method="POST">
            <input value=${this.state.totalAmount} name="tAmt" type="hidden">
            <input value=${this.state.totalAmount} name="amt" type="hidden">
            <input value="0" name="txAmt" type="hidden">
            <input value="0" name="psc" type="hidden">
            <input value="0" name="pdc" type="hidden">
            <input value="epay_payment" name="scd" type="hidden">
            <input value=${1} name="pid" type="hidden">
            <input value="https://controlplus.000webhostapp.com/success.php" type="hidden" name="su">
            <input value="https://controlplus.000webhostapp.com/failure.php" type="hidden" name="fu">
            <input value="Pay Now" type="submit">
          </form>
        </body>
        </html>`;

    return (
      <View style={styles.container}>
        <LoadingIndicator isVisible={this.props.paynow.loading} />
        <View
          style={{
            width: '100%',
            height: 130,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            backgroundColor: Colors.primaryMedium,
          }}>
          {!this.state.esewaUiStatus && !this.state.khaltiUiStatus ? (
            <TouchableOpacity
              activeOpacity={0.75}
              onPress={() => {
                NavigationService.back();
              }}
              style={{padding: 16}}>
              <Icon.Ionicons
                name="md-arrow-round-back"
                size={26}
                color={Colors.white}
              />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              activeOpacity={0.75}
              onPress={() => {
                this.setState({
                  esewaUiStatus: false,
                  khaltiUiStatus: false,
                  headerTitle: 'Pay Now',
                });
              }}
              style={{padding: 16}}>
              <Icon.Entypo name="cross" size={26} color={Colors.white} />
            </TouchableOpacity>
          )}

          <Text
            numberOfLines={1}
            style={{
              fontFamily: 'Nunito-SemiBold',
              fontSize: 20,
              color: Colors.white,
            }}>
            {this.state.headerTitle}
          </Text>
          <View style={{width: 50}} />
        </View>

        <ScrollView
          keyboardShouldPersistTaps="handled"
          style={
            this.state.esewaUiStatus || this.state.khaltiUiStatus
              ? {
                  backgroundColor: Colors.white,
                  backgroundColor: Colors.white,
                  borderTopLeftRadius: 20,
                  borderTopRightRadius: 20,
                  marginTop: -20,
                }
              : {
                  backgroundColor: Colors.white,
                  backgroundColor: Colors.white,
                  borderTopLeftRadius: 20,
                  borderTopRightRadius: 20,
                  marginTop: -20,
                  marginBottom: 90,
                }
          }>
          {this.state.esewaUiStatus && (
            <Animatable.View
              animation="zoomIn"
              duration={800}
              useNativeDriver={true}
              style={{
                width: '100%',
                height: '100%',
              }}>
              <WebView
                source={{
                  html: this.esewaUI,
                }}
                originWhitelist={['*']}
                // onLoadProgress={({nativeEvent}) => {
                //   this.props.loadingState(nativeEvent.progress);
                // }}
                scalesPageToFit={false}
                style={{
                  position: 'absolute',
                  top: 0,
                  bottom: 0,
                  right: 0,
                  left: 0,
                  marginBottom: -8,
                }}
                onMessage={event => {
                  if (event.nativeEvent.data == 'SUCCESS') {
                    this.setState({checked3: false});

                    // this.props.createOrder(2, '', this.totalPrice);
                  } else {
                    this.setState({checked3: false});
                    ToastAndroid.showWithGravityAndOffset(
                      'Error: Esewa transaction failed.',
                      ToastAndroid.LONG,
                      ToastAndroid.BOTTOM,
                      0,
                      50,
                    );
                  }
                }}
                startInLoadingState={true}
              />
            </Animatable.View>
          )}

          {this.state.khaltiUiStatus && (
            <Animatable.View
              animation="zoomIn"
              duration={800}
              useNativeDriver={true}
              style={{
                width: '100%',
                height: '100%',
              }}>
              <WebView
                source={{html: this.khaltiUI}}
                originWhitelist={['*']}
                scalesPageToFit={false}
                injectedJavaScript={INJECTEDJAVASCRIPT}
                style={{
                  position: 'absolute',
                  top: 0,
                  bottom: 0,
                  right: 0,
                  left: 0,
                  marginBottom: -8,
                }}
                onMessage={event => {
                  if (event.nativeEvent.data == 'onClose') {
                    this.setState({checked2: false});
                  } else {
                    this.reponse = JSON.parse(event.nativeEvent.data);

                    if (this.reponse.token) {
                      this.setState({checked3: false});

                      if (
                        this.reponse.token != null &&
                        this.reponse.token != '' &&
                        this.reponse.token != undefined
                      ) {
                        // this.props.createOrder(
                        //   3,
                        //   this.reponse.token,
                        //   this.totalPrice,
                        // );
                      } else {
                        ToastAndroid.showWithGravityAndOffset(
                          'Confirm the Payment Process before proceeding',
                          ToastAndroid.LONG,
                          ToastAndroid.BOTTOM,
                          0,
                          50,
                        );
                      }
                    } else {
                    }
                  }
                }}
                startInLoadingState={true}
              />
            </Animatable.View>
          )}
          <Text
            style={{
              fontFamily: 'Nunito-ExtraBold',
              fontSize: 18,
              color: Colors.black,
              marginHorizontal: 16,
              marginTop: 20,
            }}>
            Select Payment Method
          </Text>

          <View
            style={{
              elevation: 5,
              marginVertical: 10,
              backgroundColor: Colors.white,
              borderRadius: 12,
              marginHorizontal: 16,
            }}>
            <TouchableOpacity
              activeOpacity={0.75}
              onPress={() => {
                this.setState({
                  checked1: true,
                  checked2: false,
                  checked3: false,
                });
              }}
              style={{
                borderRadius: 20,
                padding: 12,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View>
                <Image
                  source={Images.cod}
                  style={{
                    width: 150,
                    height: 80,
                    paddingHorizontal: 12,
                    borderRadius: 4,
                  }}
                  resizeMode="contain"
                />
                <Text
                  numberOfLines={2}
                  style={{
                    fontFamily: 'Nunito-SemiBold',
                    fontSize: 10,
                    color: Colors.grayDark,
                  }}>
                  Cash On Delivery
                </Text>
              </View>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <View style={styles.circle} />
                {this.state.checked1 && <View style={styles.checkedCircle} />}
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{
              elevation: 5,
              marginVertical: 10,
              backgroundColor: Colors.white,
              borderRadius: 12,
              marginHorizontal: 16,
            }}>
            <TouchableOpacity
              activeOpacity={0.75}
              onPress={() => {
                this.setState({
                  checked1: false,
                  checked2: true,
                  checked3: false,
                });
              }}
              style={{
                borderRadius: 20,
                padding: 12,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View>
                <Image
                  source={Images.esewa}
                  style={{
                    width: 150,
                    height: 80,
                    paddingHorizontal: 12,
                    borderRadius: 4,
                  }}
                  resizeMode="contain"
                />
                {global.userInfo.email && (
                  <Text
                    numberOfLines={2}
                    style={{
                      fontFamily: 'Nunito-SemiBold',
                      fontSize: 10,
                      color: Colors.grayDark,
                    }}>
                    {global.userInfo.email}
                  </Text>
                )}
                {global.userInfo.phone && (
                  <Text
                    numberOfLines={2}
                    style={{
                      fontFamily: 'Nunito-SemiBold',
                      fontSize: 10,
                      color: Colors.grayDark,
                    }}>
                    {global.userInfo.phone}
                  </Text>
                )}
              </View>
              <View style={{alignItems: 'flex-end'}}>
                <View
                  style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <View style={styles.circle} />
                  {this.state.checked2 && <View style={styles.checkedCircle} />}
                </View>
                <Text
                  numberOfLines={2}
                  style={{
                    fontFamily: 'Nunito-Bold',
                    fontSize: 14,
                    color: Colors.primaryMedium,
                  }}>
                  Get 30% off
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{
              elevation: 5,
              marginVertical: 10,
              backgroundColor: Colors.white,
              borderRadius: 12,
              marginHorizontal: 16,
            }}>
            <TouchableOpacity
              activeOpacity={0.75}
              onPress={() => {
                this.setState({
                  checked1: false,
                  checked2: false,
                  checked3: true,
                });
              }}
              style={{
                borderRadius: 20,
                padding: 12,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View>
                <Image
                  source={Images.khalti}
                  style={{
                    width: 150,
                    height: 80,
                    paddingHorizontal: 12,
                    borderRadius: 4,
                  }}
                  resizeMode="contain"
                />
                {global.userInfo.email && (
                  <Text
                    numberOfLines={2}
                    style={{
                      fontFamily: 'Nunito-SemiBold',
                      fontSize: 10,
                      color: Colors.grayDark,
                    }}>
                    {global.userInfo.email}
                  </Text>
                )}
                {global.userInfo.phone && (
                  <Text
                    numberOfLines={2}
                    style={{
                      fontFamily: 'Nunito-SemiBold',
                      fontSize: 10,
                      color: Colors.grayDark,
                    }}>
                    {global.userInfo.phone}
                  </Text>
                )}
              </View>
              <View style={{alignItems: 'flex-end'}}>
                <View
                  style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <View style={styles.circle} />
                  {this.state.checked3 && <View style={styles.checkedCircle} />}
                </View>
                <Text
                  numberOfLines={2}
                  style={{
                    fontFamily: 'Nunito-Bold',
                    fontSize: 14,
                    color: Colors.primaryMedium,
                  }}>
                  Get 20% off
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{marginHorizontal: 16, marginBottom: 8}}>
            <Text
              style={{
                fontFamily: 'Nunito-ExtraBold',
                fontSize: 18,
                color: Colors.black,
                marginTop: 16,
              }}>
              Payments Details
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 8,
              }}>
              <View
                style={{
                  width: 6,
                  height: 6,
                  borderRadius: 3,
                  backgroundColor: Colors.black,
                }}
              />
              <Text
                style={{
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 12,
                  color: Colors.grayDark,
                  marginHorizontal: 16,
                }}>
                Detail 1
              </Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View
                style={{
                  width: 6,
                  height: 6,
                  borderRadius: 3,
                  backgroundColor: Colors.black,
                }}
              />
              <Text
                style={{
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 12,
                  color: Colors.grayDark,
                  marginHorizontal: 16,
                }}>
                Detail 2
              </Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View
                style={{
                  width: 6,
                  height: 6,
                  borderRadius: 3,
                  backgroundColor: Colors.black,
                }}
              />
              <Text
                style={{
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 12,
                  color: Colors.grayDark,
                  marginHorizontal: 16,
                }}>
                Detail 3
              </Text>
            </View>
          </View>
        </ScrollView>
        {!this.state.esewaUiStatus && !this.state.khaltiUiStatus && (
          <View
            style={{
              position: 'absolute',
              bottom: 0,
              left: 0,
              right: 0,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: Colors.white,
              elevation: 3,
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              height: 90,
            }}>
            <CustomButton.PrimaryButton
              title="Confirm Order"
              onPress={() => {
                if (this.state.checked1) {
                  this.props.createOrder(this.orderData.id);
                } else if (this.state.checked2) {
                  this.setState({
                    esewaUiStatus: true,
                    headerTitle: 'Esewa Payment',
                  });
                } else {
                  this.setState({
                    khaltiUiStatus: true,
                    headerTitle: 'Khalti Payment',
                  });
                }
              }}
            />
          </View>
        )}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    paynow: state.paynow,
  };
}

export default connect(
  mapStateToProps,
  {createOrder},
)(PayNow);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  checkedCircle: {
    width: 12,
    height: 12,
    borderRadius: 6,
    position: 'absolute',
    backgroundColor: Colors.primaryDark,
  },
  circle: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Colors.grayMedium,
  },
});
