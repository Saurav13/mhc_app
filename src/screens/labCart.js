import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import NavigationService from '../route/navigationService';

import Images from '../res/images';
import Icon from '../res/icons';
import Colors from '../res/colors';
import CustomTextInput from '../components/textInput';
import CustomButton from '../components/button';

const Header = () => {
  return (
    <View
      style={{
        backgroundColor: Colors.primaryMedium,
        height: 110,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingBottom: 20,
      }}>
      <TouchableOpacity
        activeOpacity={0.75}
        onPress={() => {
          NavigationService.back();
        }}
        style={{padding: 16}}>
        <Icon.Ionicons
          name="md-arrow-round-back"
          size={26}
          color={Colors.white}
        />
      </TouchableOpacity>

      <Text
        numberOfLines={2}
        style={{
          fontFamily: 'Nunito-SemiBold',
          fontSize: 20,
          color: Colors.white,
        }}>
        My Cart
      </Text>
      <View style={{width: 42, height: 42}} />
    </View>
  );
};

const FlatListItem = ({item}) => {
  return (
    <View
      style={{
        marginBottom: 29,
        width: global.screenWidth - 50,
        backgroundColor: Colors.white,
        elevation: 5,
        padding: 16,
        borderRadius: 15,
        alignSelf: 'center',
      }}>
      <Text
        style={{
          fontFamily: 'Nunito-SemiBold',
          fontSize: 13,
        }}>
        {item.title}
      </Text>
      <Text
        style={{
          fontFamily: 'Nunito-SemiBold',
          fontSize: 12,
          color: Colors.grayDarkest,
        }}>
        {item.prescriber}
      </Text>
      <Text
        style={{
          fontFamily: 'Nunito-Bold',
          fontSize: 15,
          color: Colors.primaryMedium,
        }}>
        Rs {item.price}
      </Text>
      <TouchableOpacity
        activeOpacity={0.75}
        onPress={() => {}}
        style={{
          height: 24,
          position: 'absolute',
          bottom: 16,
          right: 16,
          backgroundColor: Colors.redDark,
          borderRadius: 14,
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: 16,
          elevation: 3,
        }}>
        <Text
          numberOfLines={3}
          style={{
            fontFamily: 'Nunito-SemiBold',
            fontSize: 9,
            color: Colors.white,
            marginRight: 3,
          }}>
          Remove
        </Text>
        <Icon.MaterialCommunityIcons
          name="delete-outline"
          size={13}
          color={Colors.white}
        />
      </TouchableOpacity>
    </View>
  );
};
class LabCart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Header />
        <View
          style={{
            flex: 1,
            backgroundColor: Colors.white,
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            marginTop: -20,
          }}>
          <CustomTextInput.SearchTextInput
            style={{marginHorizontal: 24, marginBottom: 24, marginTop: 24}}
            placeholder="Add more your Desired Test"
            value={this.state.searchText}
            onChangeText={text => {
              this.setState({searchText: text});
            }}
          />
          <FlatList
            contentContainerStyle={{paddingTop: 4, paddingBottom: 260}}
            showsVerticalScrollIndicator={false}
            data={data}
            renderItem={({item, index}) => {
              return <FlatListItem item={item} />;
            }}
            keyExtractor={(item, index) => index.toString()}
          />
          <View
            style={{
              position: 'absolute',
              bottom: 0,
              height: global.screenHeight / 3,
              width: global.screenWidth,
              backgroundColor: Colors.white,
              paddingHorizontal: 25,
              elevation: 5,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingTop: 10,
              }}>
              <Text
                style={{
                  fontFamily: 'Nunito-Bold',
                  fontSize: 13,
                }}>
                This Test Is For
              </Text>
              <Text
                style={{
                  fontFamily: 'Nunito-Bold',
                  fontSize: 12,
                  color: Colors.grayDarkest,
                }}>
                Add Family Memeber
              </Text>
            </View>
            <CustomTextInput.MainTextInput
              label="Self ( dropdown ho milaunna baki xa)"
              value={this.state.search}
              onChangeText={val => this.setState({search: val})}
              keyboardType="default"
            />
          </View>
          <View style={{bottom: 30, alignItems: 'center'}}>
            <CustomButton.PrimaryButton
              title="Checkout"
              onPress={() => {
                NavigationService.navigate('Checkout');
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default LabCart;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  logo: {
    width: 110,
  },
});
