import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import LoadingIndicator from '../components/activityIndicator';
import NavigationService from '../route/navigationService';
import CustomTextInput from '../components/textInput';
import CustomButton from '../components/button';
import Header from '../components/headerDashboard';
import Images from '../res/images';
import Icon from '../res/icons';
import Colors from '../res/colors';

const categoryItemList = [
  {
    image: require('../res/images/ico_blood.png'),
    cat_name: 'Blood Sugar',
  },
  {
    image: require('../res/images/ico_lipid.png'),
    cat_name: 'Liquid Profile',
  },
  {
    image: require('../res/images/ico_surgery.png'),
    cat_name: 'Liver Profile',
  },
  {
    image: require('../res/images/ico_surgery.png'),
    cat_name: 'Aurveda',
  },
];

const TrusteDiagonsisData = [
  {
    image: require('../res/images/ico_service.png'),
    title: 'Assured Home Service',
    body:
      'Medlife expert technician visits your home to collect blood sample. All samples have a Unique-ID. You can track your sample in real time.',
  },
  {
    image: require('../res/images/ico_like.png'),
    title: 'High Quality Reports',
    body:
      'Medlife expert technician visits your home to collect blood sample. All samples have a Unique-ID. You can track your sample in real time.',
  },
  {
    image: require('../res/images/ico_message.png'),
    title: 'Expert Review',
    body:
      'Medlife expert technician visits your home to collect blood sample. All samples have a Unique-ID. You can track your sample in real time.',
  },
];

const CategoryItem = ({item}) => {
  return (
    <View
      style={{
        elevation: 5,
        backgroundColor: Colors.white,
        borderRadius: 12,
        marginVertical: 13,
        height: 95,
        width: 95,
        marginRight: 4,
        marginLeft: 8,
      }}>
      <TouchableOpacity
        activeOpacity={0.75}
        onPress={() => {
          NavigationService.navigate('Category');
        }}
        style={{
          borderRadius: 12,
          padding: 12,
          alignItems: 'center',
        }}>
        <Image
          source={item.image}
          style={{
            width: 40,
            height: 40,
            backgroundColor: Colors.white,
          }}
          resizeMode="contain"
        />
        <Text
          style={{
            fontFamily: 'Nunito-SemiBold',
            fontSize: 12,
            color: Colors.grayDark,
            marginTop: 8,
          }}>
          {item.cat_name}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const SearchButton = ({title, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.7}
      style={styles.searchButtonStyle}>
      <Text
        style={{
          color: Colors.white,
          fontFamily: 'Nunito-Regular',
          fontSize: 11,
          marginRight: 7,
        }}>
        {title}
      </Text>
      <TouchableOpacity activeOpacity={0.7} onPress={() => {}}>
        <Icon.Entypo name="cross" size={11} color={Colors.white} />
      </TouchableOpacity>
    </TouchableOpacity>
  );
};

class SelectLabResult extends Component {
  state = {
    search: '',
    searchText: '',
  };

  render() {
    return (
      <View style={styles.container}>
        <Header />
        <View
          style={{
            flex: 1,
            marginTop: -20,
            borderRadius: 25,
            width: global.screenWidth,
            backgroundColor: Colors.white,
          }}>
          <CustomTextInput.SearchTextInput
            style={{marginHorizontal: 24, marginBottom: 21}}
            placeholder="Search medicine, healthcare"
            value={this.state.searchText}
            onChangeText={text => {
              this.setState({searchText: text});
            }}
          />
          <ScrollView
            keyboardShouldPersistTaps="handled"
            showsVerticalScrollIndicator={false}>
            <View style={{marginHorizontal: 25}}>
              <TouchableOpacity
                activeOpacity={0.75}
                onPress={() => {
                  NavigationService.navigate('ProductDetail');
                }}
                style={{borderRadius: 20}}>
                <Image
                  source={require('../res/images/banner2.png')}
                  style={{
                    width: global.screenWidth - 50,
                    height: 180,
                    borderRadius: 20,
                  }}
                />
              </TouchableOpacity>
              <Text
                style={[
                  styles.subHeadingText,
                  {marginTop: 34, marginBottom: 14},
                ]}>
                Mero Health Lab
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  paddingBottom: 11,
                  borderBottomWidth: 1,
                  borderBottomColor: Colors.grayDarkest,
                }}>
                <SearchButton
                  title="Total Cholestrol Test"
                  onPress={() => {}}
                />
                <SearchButton
                  title="Total Cholestrol Test"
                  onPress={() => {}}
                />
              </View>
              <CustomTextInput.MainTextInput
                label="Showing Location ( dropdown ho milaunna baki xa)"
                value={this.state.search}
                onChangeText={val => this.setState({search: val})}
                keyboardType="default"
              />
              <CustomButton.SecondaryButton
                style={{
                  paddingHorizontal: 20,
                  paddingVertical: 5,
                  alignSelf: 'flex-end',
                }}
                title="Search"
                onPress={() => {}}
              />
              <Text style={[styles.subHeadingText, {marginTop: 25}]}>
                Categories
              </Text>
            </View>
            <FlatList
              contentContainerStyle={{
                paddingTop: 10,
                paddingLeft: 8,
                paddingRight: 12,
              }}
              showsHorizontalScrollIndicator={false}
              horizontal={true}
              data={categoryItemList}
              renderItem={({item, index}) => {
                return <CategoryItem item={item} />;
              }}
              keyExtractor={(item, index) => index.toString()}
            />
          </ScrollView>
        </View>
      </View>
    );
  }
}
export default SelectLabResult;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  logo: {
    width: 110,
  },
  subHeadingText: {
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 18,
  },
  searchButtonStyle: {
    height: 26,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.primaryMedium,
    borderRadius: 16,
    paddingHorizontal: 12,
    paddingVertical: 6,
    marginRight: 11,
  },
});
