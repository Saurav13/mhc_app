import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  StatusBar,
  FlatList,
  Platform,
  ScrollView,
  Dimensions,
} from 'react-native';
import NavigationService from '../route/navigationService';
import CustomButton from '../components/button';
import Colors from '../res/colors';
import Icons from '../res/icons';
import {calculatePricePercentage} from '../utlis/calculatePricePercentage';
import {connect} from 'react-redux';
import {addToCart} from '../actions/CartAction';
import WebView from 'react-native-webview';
import TimerCountdown from '../components/timerCountdown';

const TopHeadingImage = ({indicator, image}) => {
  return (
    <View
      style={{
        height: '100%',
        width: global.screenWidth,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        overflow: 'hidden',
        marginBottom: 18,
      }}>
      <Image
        style={{height: '75%', width: '75%', alignSelf: 'center'}}
        source={image}
        resizeMode="contain"
      />
      <View style={{flexDirection: 'row'}}>
        {indicator.map((value, idx) => {
          const backgroundColor = value ? Colors.white : Colors.blueLight;
          return (
            <View
              key={idx.toString()}
              style={{
                width: 13,
                height: 13,
                borderRadius: 7,
                borderWidth: 2,
                marginRight: 5,
                borderColor: Colors.white,
                backgroundColor,
              }}
            />
          );
        })}
      </View>
    </View>
  );
};

const ComboPacks = ({item}) => {
  const {title, discount, price} = item;
  return (
    <View
      style={{
        width: '100%',
        backgroundColor: Colors.white,
        borderRadius: 15,
        elevation: 5,
        padding: 16,
        paddingTop: 18,
        marginBottom: 20,
      }}>
      <Text
        style={{
          fontFamily: 'Nunito-Bold',
          fontSize: 12,
        }}>
        {title}
      </Text>
      <View
        style={{
          flexDirection: 'row',
          marginTop: 5,
          width: '100%',
          alignItems: 'center',
        }}>
        <Text style={[styles.priceText, {fontSize: 16, marginRight: 4}]}>
          Rs {Math.round(price - (price * discount) / 100)}
        </Text>
        <Text style={[styles.priceTextCut, {marginRight: 4}]}>Rs {price}</Text>
        <Text style={styles.priceTextDiscount}>{discount}% off</Text>

        <CustomButton.SecondaryButton
          style={{
            position: 'absolute',
            right: 0,
            paddingHorizontal: 24,
            paddingVertical: 7,
            marginRight: 15,
            height: 25,
            backgroundColor: Colors.primaryMedium,
          }}
          textStyle={{
            fontSize: 10,
            fontFamily: 'Nunito-Bold',
            color: Colors.white,
            paddingBottom: 0,
          }}
          title="Buy Pack"
          onPress={() => {}}
        />
      </View>
    </View>
  );
};

const ProductReview = ({item}) => {
  const {name, rating, body} = item;
  const ratingList = [0, 0, 0, 0, 0];
  return (
    <View style={{marginBottom: 17}}>
      <Text
        style={{
          fontSize: 14,
          fontFamily: 'Nunito-SemiBold',
          color: Colors.primaryDark,
        }}>
        {name}
      </Text>
      <View style={{flexDirection: 'row', marginVertical: 4}}>
        {ratingList.map((_, idx) => {
          const ratingName = idx + 1 <= rating ? 'star' : 'star-o';
          return (
            <View key={ratingName + idx.toString()} style={{marginRight: 4}}>
              <Icons.FontAwesome
                name={ratingName}
                size={14}
                color={Colors.yellowDark}
              />
            </View>
          );
        })}
      </View>
      <Text
        style={{
          fontSize: 14,
          fontFamily: 'Nunito-Regular',
          color: Colors.grayDarkest,
        }}>
        {body}
      </Text>
    </View>
  );
};

const RelatedProduct = () => {
  return (
    <View
      style={{
        width: global.screenWidth / 2 - 30,
        elevation: 5,
        marginVertical: 10,
        backgroundColor: Colors.white,
        borderRadius: 12,
        marginLeft: 20,
      }}>
      <TouchableOpacity
        activeOpacity={0.75}
        onPress={() => {
          NavigationService.navigate('ProductDetail');
        }}
        style={{
          borderRadius: 20,
          padding: 12,
        }}>
        <View
          style={{
            position: 'absolute',
            top: 16,
            left: 16,
            alignItems: 'center',
            justifyContent: 'center',
            height: 16,
            borderRadius: 6,
            backgroundColor: Colors.greenLight,
            zIndex: 1,
            paddingHorizontal: 8,
          }}>
          <Text
            numberOfLines={1}
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 10,
              color: Colors.white,
            }}>
            30% Off
          </Text>
        </View>
        <Image
          source={require('../res/images/banner.png')}
          style={{
            width: '100%',
            height: 120,
            paddingHorizontal: 12,
            borderRadius: 4,
          }}
        />
        <Text
          numberOfLines={1}
          style={{
            fontFamily: 'Nunito-SemiBold',
            fontSize: 12,
            color: Colors.black,
            marginTop: 8,
          }}>
          Accu-Check Performa
        </Text>

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginTop: 8,
          }}>
          <Text
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 14,
              color: Colors.primaryMedium,
            }}>
            Rs 465
          </Text>
          <TouchableOpacity activeOpacity={0.75} onPress={() => {}}>
            <Text
              style={{
                fontFamily: 'Nunito-Bold',
                fontSize: 9,
                color: Colors.redDark,
                textDecorationLine: 'line-through',
                textDecorationStyle: 'solid',
              }}>
              Rs 480
            </Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    </View>
  );
};

class ProductDetails extends Component {
  constructor(props) {
    super(props);
    this.productDetail = this.props.navigation.getParam('productDetail');

    this.state = {
      quantity: 1,
      webViewHighlightsHeight: 0,
      webViewDescriptionHeight: 0,
      duration: '',
    };
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        var itemIndex = global.cartData.findIndex(
          item => item.id == this.productDetail.id,
        );

        if (itemIndex == -1) {
          this.addText = 'Add to Cart';
        } else {
          this.addText = 'Update to Cart';
          this.setState({quantity: global.cartData[itemIndex].cartQuantity});
        }

        this.setState({duration: TimerCountdown(this.productDetail.sale_to)});
        this.durationInterval = setInterval(() => {
          this.setState({duration: TimerCountdown(this.productDetail.sale_to)});
        }, 1000);
      },
    );

    this.willFocusSubscription = this.props.navigation.addListener(
      'didBlur',
      () => {
        clearInterval(this.durationInterval);
      },
    );
  }

  onWebViewMessageHighlights = (event: WebViewMessageEvent) => {
    this.setState({webViewHighlightsHeight: Number(event.nativeEvent.data)});
  };

  onWebViewMessageDescription = (event: WebViewMessageEvent) => {
    this.setState({webViewDescriptionHeight: Number(event.nativeEvent.data)});
  };

  flatListHeader = () => {
    const STATUSBAR_HEIGHT =
      Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

    return (
      <View style={styles.container}>
        <View
          style={{
            backgroundColor: Colors.blueLight,
            height: global.screenHeight / 2.2,
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'space-between',
            paddingTop: STATUSBAR_HEIGHT,
          }}>
          <ScrollView
            keyboardShouldPersistTaps="handled"
            style={{
              position: 'absolute',
              height: '100%',
              width: '100%',
              marginTop: STATUSBAR_HEIGHT,
            }}
            contentContainerStyle={{
              justifyContent: 'center',
              alignItems: 'center',
            }}
            horizontal={true}
            pagingEnabled={true}
            showsHorizontalScrollIndicator={false}
            scrollEnabled={true}>
            <TopHeadingImage
              image={{
                uri: `https://merohealthcare.com/assets/images/${
                  this.productDetail.photo
                }`,
              }}
              indicator={[1, 0, 0]}
            />
            {/**   <TopHeadingImage
              image={require('../res/images/productIMG.png')}
              indicator={[0, 1, 0]}
            />
            <TopHeadingImage
              image={require('../res/images/productIMG.png')}
              indicator={[0, 0, 1]}
            /> */}
          </ScrollView>

          <TouchableOpacity
            activeOpacity={0.75}
            onPress={() => {
              NavigationService.back();
            }}
            style={{padding: 20}}>
            <Icons.Ionicons
              name="md-arrow-round-back"
              size={26}
              color={Colors.white}
            />
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.75}
            onPress={() => {
              NavigationService.navigate('Cart');
            }}
            style={{padding: 20}}>
            <Icons.FontAwesome5
              name="shopping-cart"
              size={22}
              color={Colors.white}
            />
            <View
              style={{
                position: 'absolute',
                height: 18,
                width: 18,
                top: 12,
                right: 10,
                borderRadius: 9,
                backgroundColor: Colors.redDark,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'Nunito-Regular',
                  fontSize: 12,
                  color: Colors.white,
                }}>
                {global.cartData.length}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View
          style={{
            backgroundColor: Colors.white,
            marginTop: -20,
            paddingHorizontal: 20,
            borderRadius: 20,
            paddingTop: 20,
          }}>
          {this.productDetail.pprice != null &&
            this.productDetail.cprice != this.productDetail.pprice &&
            this.state.duration != 0 && (
              <View
                style={{
                  position: 'absolute',
                  top: -25,
                  right: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Colors.white,
                  borderRadius: 10,
                  paddingHorizontal: 20,
                  elevation: 3,
                  paddingVertical: 5,
                }}>
                <Text style={styles.discountText}>Discount Valid Till</Text>
                <Text style={styles.discountTextNumber}>
                  {this.state.duration}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <Text style={styles.discountTextSmall}>Days</Text>
                  <Text style={styles.discountTextSmall}>Hours</Text>
                  <Text style={styles.discountTextSmall}>Minutes</Text>
                  <Text style={styles.discountTextSmall}>Seconds</Text>
                </View>
              </View>
            )}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'flex-end',
            }}>
            <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
              <Text
                style={[styles.priceText, {marginRight: 8, marginBottom: -4}]}>
                Rs {this.productDetail.cprice}
              </Text>
              <Text style={[styles.pricePack, {marginRight: 8}]}>
                {this.productDetail.product_quantity}
              </Text>
            </View>
            {this.productDetail.pprice != null &&
              this.productDetail.cprice != this.productDetail.pprice && (
                <Text style={[styles.priceTextCut, {marginRight: 8}]}>
                  Rs {this.productDetail.pprice}
                </Text>
              )}
            {this.productDetail.pprice != null &&
              this.productDetail.cprice != this.productDetail.pprice && (
                <Text style={styles.priceTextDiscount}>
                  {calculatePricePercentage(
                    this.productDetail.cprice,
                    this.productDetail.pprice,
                  )}
                  % off
                </Text>
              )}
          </View>
          <Text
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 18,
              marginTop: 8,
            }}>
            {this.productDetail.name}
          </Text>
          <Text
            style={{
              fontFamily: 'Nunito-SemiBold',
              fontSize: 14,
              color: Colors.redDark,
              marginTop: 5,
            }}>
            {this.productDetail.company_name}
          </Text>

          {/**  </Text> */}
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 24}}>
            <TouchableOpacity
              activeOpacity={0.75}
              onPress={() => {
                this.setState({quantity: this.state.quantity - 1});
              }}
              style={styles.incDecButton}>
              <Icons.FontAwesome name="minus" size={14} color={Colors.white} />
            </TouchableOpacity>
            <Text
              style={{
                fontFamily: 'Nunito-SemiBold',
                fontSize: 18,
                color: Colors.grayDarkest,
                textAlign: 'center',
                marginHorizontal: 10,
              }}>
              {this.state.quantity}
            </Text>
            <TouchableOpacity
              activeOpacity={0.75}
              onPress={() => {
                this.setState({quantity: this.state.quantity + 1});
              }}
              style={styles.incDecButton}>
              <Icons.FontAwesome name="plus" size={14} color={Colors.white} />
            </TouchableOpacity>
          </View>

          {this.productDetail.highlights != '' && (
            <View style={{flex: 1, marginTop: -8}}>
              <Text style={[styles.subHeadingText, {marginTop: 24}]}>
                Product Highlights
              </Text>

              <WebView
                originWhitelist={['*']}
                source={{
                  html: this.productDetail.highlights,
                }}
                scalesPageToFit={false}
                style={{
                  height: this.state.webViewHighlightsHeight,
                  marginHorizontal: -8,
                  marginTop: -8,
                }}
                onMessage={this.onWebViewMessageHighlights}
                injectedJavaScript="window.ReactNativeWebView.postMessage(document.body.scrollHeight)"
              />
            </View>
          )}

          <Text style={[styles.subHeadingText, {marginTop: 16}]}>
            Available Variants
          </Text>
          <View style={{flexDirection: 'row', marginTop: 5}}>
            {this.productDetail.sub_title.split(',').length > 0 &&
              this.productDetail.sub_title.split(',').map((title, index) => {
                return (
                  <CustomButton.SecondaryButton
                    key={index}
                    style={{
                      paddingHorizontal: 20,
                      paddingVertical: 5,
                      marginRight: 15,
                    }}
                    textStyle={{fontSize: 13, fontFamily: 'Nunito-SemiBold'}}
                    title={title}
                    onPress={() => {}}
                  />
                );
              })}
            {/* <CustomButton.SecondaryButton
              style={{
                paddingHorizontal: 20,
                paddingVertical: 5,
                marginRight: 15,
                backgroundColor: Colors.white,
              }}
              textStyle={{
                fontSize: 13,
                fontFamily: 'Nunito-SemiBold',
                color: Colors.grayDarkest,
                paddingBottom: 0,
              }}
              title={this.productDetail.sub_title}
              onPress={() => { }}
            />
            <CustomButton.SecondaryButton
              style={{
                paddingHorizontal: 20,
                paddingVertical: 5,
                marginRight: 15,
                backgroundColor: Colors.white,
              }}
              textStyle={{
                fontSize: 13,
                fontFamily: 'Nunito-SemiBold',
                color: Colors.grayDarkest,
                paddingBottom: 0,
              }}
              title="100 ml"
              onPress={() => { }}
            />*/}
          </View>

          {this.productDetail.description != '' && (
            <View style={{flex: 1}}>
              <Text style={[styles.subHeadingText, {marginTop: 24}]}>
                Full Description
              </Text>
              <WebView
                originWhitelist={['*']}
                source={{
                  html: this.productDetail.description,
                }}
                scalesPageToFit={false}
                style={{
                  height: this.state.webViewDescriptionHeight,
                  marginHorizontal: -8,
                  marginBottom: 16,
                  marginTop: -8,
                }}
                onMessage={this.onWebViewMessageDescription}
                injectedJavaScript="window.ReactNativeWebView.postMessage(document.body.scrollHeight)"
              />
            </View>
          )}
          {/** <Text style={styles.subHeadingText}>Combo Packs</Text>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={comboPackData}
            listKey={(item, index) => 'Combo' + index.toString()}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => {
              return <ComboPacks item={item} />;
            }}
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text style={[styles.subHeadingText, { marginTop: 10 }]}>
              Product Review
            </Text>
            <TouchableOpacity
              activeOpacity={0.7}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => { }}>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: 'Nunito-Regular',
                  color: Colors.grayDarkest,
                }}>
                See all
              </Text>
            </TouchableOpacity>
          </View>
          <FlatList
            contentContainerStyle={{
              paddingBottom: 8,
            }}
            data={ratingData}
            listKey={(item, index) => 'Review' + index.toString()}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => {
              return <ProductReview item={item} />;
            }}
          />
          <Text
            style={[styles.subHeadingText, { marginTop: 2, marginBottom: 15 }]}>
            Related Products
          </Text> */}
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          ListHeaderComponent={this.flatListHeader}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: 8,
          }}
          numColumns={2}
          data={[]}
          renderItem={({item, index}) => {
            return <RelatedProduct item={item} />;
          }}
          keyExtractor={(item, index) => index.toString()}
        />
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            this.props.addToCart(this.productDetail, this.state.quantity);
          }}
          style={[styles.cartButton, {width: global.screenWidth}]}>
          <Text
            style={{
              fontFamily: 'Nunito-SemiBold',
              fontSize: 14,
              color: Colors.white,
              marginRight: 15,
            }}>
            {this.addText}
          </Text>
          <Icons.FontAwesome5
            name="shopping-cart"
            size={19}
            color={Colors.white}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    cartCount: state.cart.cartCount,
  };
};
export default connect(
  mapStateToProps,
  {
    addToCart,
  },
)(ProductDetails);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  priceText: {
    fontFamily: 'Nunito-Bold',
    fontSize: 24,
    color: Colors.primaryDark,
  },
  priceTextCut: {
    fontFamily: 'Nunito-Bold',
    fontSize: 11,
    color: Colors.redDark,
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  priceTextDiscount: {
    fontFamily: 'Nunito-Bold',
    fontSize: 11,
    color: Colors.greenDark,
    textDecorationStyle: 'solid',
  },
  discountText: {
    fontSize: 11,
    color: Colors.redDark,
    fontFamily: 'Nunito-SemiBold',
  },
  discountTextNumber: {
    fontSize: 12,
    color: Colors.grayDarkest,
    fontFamily: 'Nunito-SemiBold',
  },
  discountTextSmall: {
    flex: 1,
    fontSize: 5,
    color: Colors.grayDarkest,
    fontFamily: 'Nunito-SemiBold',
    textAlign: 'center',
  },
  pricePack: {
    fontSize: 10,
    color: Colors.grayDarkest,
    fontFamily: 'Nunito-SemiBold',
  },
  incDecButton: {
    height: 25,
    width: 25,
    borderRadius: 8,
    backgroundColor: Colors.primaryMedium,
    justifyContent: 'center',
    alignItems: 'center',
  },
  subHeadingText: {
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 14,
    //marginTop: 25,
    marginBottom: 12,
  },
  cartButton: {
    flexDirection: 'row',
    bottom: 0,
    height: 56,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: Colors.primaryMedium,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
