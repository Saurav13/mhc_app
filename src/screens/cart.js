import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-community/async-storage';

import NavigationService from '../route/navigationService';

import {connect} from 'react-redux';
import {removeFromCart, getCartData, savePaynow} from '../actions/CartAction';

import Icons from '../res/icons';
import Colors from '../res/colors';
import CustomButton from '../components/button';
import ModalComponent from '../components/ModalComponent';
import HeaderDashboard from '../components/headerDashboard';
import AlertToast from '../components/alertToast';
import LoadingIndicator from '../components/activityIndicator';

class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      uploadPrescriptionVisibility: false,
      deleteModal: false,
      textInputModal: false,
      selectedItem: '',
    };
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.props.getCartData(global.cartData);
        this.forceUpdate();
      },
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <HeaderDashboard title="My Cart" noCart={true} />
        <LoadingIndicator isVisible={this.props.loading} />

        <Modal
          isVisible={this.state.uploadPrescriptionVisibility}
          animationIn={'zoomIn'}
          animationOut={'zoomOut'}
          backdropOpacity={0.5}
          useNativeDriver={true}
          onBackButtonPress={() => {
            this.setState({uploadPrescriptionVisibility: false});
          }}>
          <View
            style={{
              elevation: 5,
              borderRadius: 12,
              padding: 16,
              backgroundColor: Colors.white,
            }}>
            <FlatList
              data={[]}
              ListEmptyComponent={() => <Text>No list</Text>}
              renderItem={({item, index}) => {
                return (
                  <View
                    style={{
                      borderRadius: 5,
                    }}>
                    <Text
                      numberOfLines={2}
                      style={{
                        fontFamily: 'Nunito-SemiBold',
                        fontSize: 16,
                        color: Colors.black,
                      }}>
                      {item.name}
                    </Text>
                    <TouchableOpacity
                      activeOpacity={0.75}
                      onPress={() => {}}
                      style={{
                        height: 80,
                        width: 80,
                        borderRadius: 12,
                        borderWidth: 0.5,
                        borderColor: Colors.border,
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: 8,
                      }}>
                      <Icons.Feather
                        name="upload"
                        size={26}
                        color={Colors.grayMedium}
                      />
                      <Text
                        numberOfLines={2}
                        style={{
                          fontFamily: 'Nunito-SemiBold',
                          fontSize: 10,
                          color: Colors.grayMedium,
                        }}>
                        Upload
                      </Text>
                      <Text
                        numberOfLines={2}
                        style={{
                          fontFamily: 'Nunito-SemiBold',
                          fontSize: 10,
                          color: Colors.grayMedium,
                        }}>
                        Prescription
                      </Text>
                    </TouchableOpacity>

                    <View
                      style={{
                        height: 0.5,
                        backgroundColor: Colors.border,
                        marginVertical: 8,
                      }}
                    />
                  </View>
                );
              }}
              keyExtractor={(item, index) => index.toString()}
            />

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 8,
              }}>
              <TouchableOpacity
                activeOpacity={0.75}
                onPress={() => {
                  this.setState({uploadPrescriptionVisibility: false});
                }}
                style={{
                  height: 36,
                  backgroundColor: Colors.redDark,
                  borderRadius: 18,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  paddingHorizontal: 20,
                  elevation: 3,
                }}>
                <Text
                  style={{
                    fontFamily: 'Nunito-SemiBold',
                    fontSize: 14,
                    color: Colors.white,
                  }}>
                  Cancel
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                activeOpacity={0.75}
                onPress={() => {}}
                style={{
                  height: 36,
                  backgroundColor: Colors.primaryMedium,
                  borderRadius: 18,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  paddingHorizontal: 20,
                  elevation: 3,
                }}>
                <Text
                  style={{
                    fontFamily: 'Nunito-SemiBold',
                    fontSize: 14,
                    color: Colors.white,
                  }}>
                  Upload
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        {this.state.deleteModal && (
          <ModalComponent.ModalPrimary
            isVisible={this.state.deleteModal}
            onBackdropPress={() => {
              this.setState({
                deleteModal: false,
              });
            }}>
            <View
              style={{
                backgroundColor: Colors.white,
                borderRadius: 10,
                paddingHorizontal: 16,
              }}>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: 'Nunito-Bold',
                  color: Colors.grayDark,
                  textAlign: 'center',
                  paddingVertical: 8,
                }}>
                Are you sure you want to remove this item from the cart?
              </Text>
              <View style={{height: 1, backgroundColor: Colors.border}} />
              <TouchableOpacity
                activeOpacity={0.75}
                onPress={() => {
                  this.setState({
                    deleteModal: false,
                  });
                  this.props.removeFromCart(this.state.selectedItem.id);
                }}>
                <Text
                  style={{
                    flex: 1,
                    fontSize: 14,
                    fontFamily: 'Nunito-Bold',
                    color: Colors.redDark,
                    paddingTop: 6,
                    paddingBottom: 8,
                    textAlign: 'center',
                  }}>
                  Remove Item
                </Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                marginTop: 8,
                borderRadius: 10,
                backgroundColor: Colors.white,
              }}>
              <TouchableOpacity
                activeOpacity={0.75}
                onPress={() => {
                  this.setState({
                    deleteModal: false,
                  });
                }}>
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: 'Nunito-Bold',
                    color: Colors.black,
                    paddingVertical: 8,
                    textAlign: 'center',
                  }}>
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
          </ModalComponent.ModalPrimary>
        )}

        <ModalComponent.ModalWithInput isVisible={this.state.textInputModal} />

        <View
          style={{
            flex: 1,
            backgroundColor: Colors.white,
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            marginTop: -20,
          }}>
          <FlatList
            contentContainerStyle={{paddingTop: 8, paddingBottom: 180}}
            data={this.props.cartList}
            ListHeaderComponent={this.flatListHeader}
            ListEmptyComponent={() => (
              <View
                style={{
                  elevation: 5,
                  marginTop: 26,
                  backgroundColor: Colors.white,
                  borderRadius: 12,
                  marginHorizontal: 16,
                  marginBottom: 10,
                  padding: 12,
                  alignItems: 'center',
                }}>
                <Icons.FontAwesome5
                  name="shopping-cart"
                  size={35}
                  color={Colors.grayDark}
                />

                <Text
                  numberOfLines={1}
                  style={{
                    fontFamily: 'Nunito-Bold',
                    fontSize: 16,
                    color: Colors.black,
                    marginTop: 8,
                  }}>
                  No Items on the cart
                </Text>
                <Text
                  numberOfLines={1}
                  style={{
                    fontFamily: 'Nunito-Regular',
                    fontSize: 12,
                    color: Colors.grayDark,
                  }}>
                  Please add items into the cart to purchase.
                </Text>
              </View>
            )}
            showsVerticleScrollIndicator={false}
            renderItem={({item, index}) => {
              return (
                <View
                  style={{
                    elevation: 5,
                    marginVertical: 8,
                    backgroundColor: Colors.white,
                    borderRadius: 12,
                    marginHorizontal: 16,
                  }}>
                  <TouchableOpacity
                    activeOpacity={0.75}
                    onPress={() => {
                      NavigationService.navigate('ProductDetail', {
                        productDetail: item,
                      });
                    }}
                    style={{
                      borderRadius: 20,
                      padding: 12,
                    }}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      {item.type != 'combo' && (
                        <Image
                          source={{
                            uri: `https://merohealthcare.com/assets/images/${
                              item.photo
                            }`,
                          }}
                          style={{
                            width: 80,
                            height: 80,
                            paddingHorizontal: 12,
                            borderRadius: 12,
                            marginRight: 16,
                          }}
                        />
                      )}
                      <View style={{flex: 1}}>
                        <Text
                          style={{
                            fontFamily: 'Nunito-SemiBold',
                            fontSize: 12,
                            color: Colors.black,
                          }}>
                          {item.name}
                        </Text>

                        <Text
                          style={{
                            fontFamily: 'Nunito-SemiBold',
                            fontSize: 11,
                            color: Colors.greenDarkest,
                            marginTop: 4,
                          }}>
                          {item.company_name}
                        </Text>
                        <Text
                          numberOfLines={3}
                          style={{
                            fontFamily: 'Nunito-Bold',
                            fontSize: 15,
                            color: Colors.primaryMedium,
                            marginTop: 4,
                          }}>
                          Rs {item.cprice}
                        </Text>

                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            marginTop: 4,
                          }}>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                            }}>
                            <TouchableOpacity
                              activeOpacity={0.75}
                              onPress={() => {}}
                              style={{
                                width: 18,
                                height: 18,
                                backgroundColor: Colors.grayMedium,
                                borderRadius: 3,
                                alignItems: 'center',
                                justifyContent: 'center',
                              }}>
                              <Icons.Entypo
                                name="minus"
                                size={12}
                                color={Colors.white}
                                onPress={() => {
                                  var updatedCartData = this.props.cartList.map(
                                    cartItem => {
                                      if (cartItem.id === item.id) {
                                        return {
                                          ...cartItem,
                                          cartQuantity:
                                            cartItem.cartQuantity - 1,
                                        };
                                      }
                                      return cartItem;
                                    },
                                  );

                                  if (item.cartQuantity < 2) {
                                    return this.setState({
                                      deleteModal: true,
                                      selectedItem: item,
                                    });
                                  }

                                  global.cartData = updatedCartData;
                                  AsyncStorage.setItem(
                                    'cart',
                                    JSON.stringify(global.cartData),
                                  );

                                  this.props.getCartData(updatedCartData);
                                }}
                              />
                            </TouchableOpacity>
                            <Text
                              numberOfLines={3}
                              style={{
                                width: 30,
                                fontFamily: 'Nunito-Bold',
                                fontSize: 15,
                                color: Colors.primaryMedium,
                                textAlign: 'center',
                              }}>
                              {item.cartQuantity}
                            </Text>
                            <TouchableOpacity
                              activeOpacity={0.75}
                              onPress={() => {
                                var updatedCartData = this.props.cartList.map(
                                  cartItem => {
                                    if (cartItem.id === item.id) {
                                      return {
                                        ...cartItem,
                                        cartQuantity: cartItem.cartQuantity + 1,
                                      };
                                    }
                                    return cartItem;
                                  },
                                );

                                global.cartData = updatedCartData;
                                AsyncStorage.setItem(
                                  'cart',
                                  JSON.stringify(global.cartData),
                                );

                                this.props.getCartData(updatedCartData);
                              }}
                              style={{
                                width: 18,
                                height: 18,
                                backgroundColor: Colors.grayMedium,
                                borderRadius: 3,
                                alignItems: 'center',
                                justifyContent: 'center',
                              }}>
                              <Icons.Entypo
                                name="plus"
                                size={12}
                                color={Colors.white}
                              />
                            </TouchableOpacity>
                          </View>
                          <TouchableOpacity
                            activeOpacity={0.75}
                            onPress={() => {
                              this.setState({
                                deleteModal: true,
                                selectedItem: item,
                              });
                            }}
                            style={{
                              height: 24,
                              backgroundColor: Colors.redDark,
                              borderRadius: 14,
                              flexDirection: 'row',
                              alignItems: 'center',
                              paddingHorizontal: 16,
                              elevation: 3,
                            }}>
                            <Text
                              numberOfLines={3}
                              style={{
                                fontFamily: 'Nunito-SemiBold',
                                fontSize: 9,
                                color: Colors.white,
                                marginRight: 4,
                              }}>
                              Remove
                            </Text>
                            <Icons.MaterialIcons
                              name="delete-sweep"
                              size={16}
                              color={Colors.white}
                            />
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                    <View
                      style={{
                        height: 0.5,
                        backgroundColor: Colors.border,
                        marginVertical: 8,
                      }}
                    />
                    {/*<View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginTop: 4,
                      }}>
                      <View
                        style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text
                          numberOfLines={3}
                          style={{
                            fontFamily: 'Nunito-SemiBold',
                            fontSize: 10,
                            color: Colors.grayMedium,
                            marginRight: 8,
                          }}>
                          For Whom
                      </Text>

                        <TouchableOpacity
                          activeOpacity={0.75}
                          onPress={() => { }}
                          style={{
                            height: 18,
                            backgroundColor: Colors.primaryMedium,
                            borderRadius: 9,
                            flexDirection: 'row',
                            alignItems: 'center',
                            paddingHorizontal: 12,
                            elevation: 3,
                          }}>
                          <Text
                            style={{
                              fontFamily: 'Nunito-SemiBold',
                              fontSize: 10,
                              color: Colors.white,
                              marginRight: 4,
                            }}>
                            Self
                        </Text>
                          <Icons.Entypo
                            name="cross"
                            size={12}
                            color={Colors.white}
                          />
                        </TouchableOpacity>
                      </View>

                         <Text
                        numberOfLines={3}
                        style={{
                          fontFamily: 'Nunito-SemiBold',
                          fontSize: 10,
                          color: Colors.primaryMedium,
                          marginRight: 8,
                        }}>
                        Add Family Members
                      </Text>
                      </View>**/}
                  </TouchableOpacity>
                </View>
              );
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <View
          style={{
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: Colors.white,
            elevation: 3,
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            height: 170,
          }}>
          {/*this.props.cartList.requires_prescription ? (
            <Text
              numberOfLines={2}
              style={{
                fontFamily: 'Nunito-SemiBold',
                fontSize: 10,
                color: Colors.redDark,
              }}>
              No Prescription is required.
            </Text>
          ) : (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <Text
                  numberOfLines={2}
                  style={{
                    fontFamily: 'Nunito-SemiBold',
                    fontSize: 10,
                    color: Colors.redDark,
                  }}>
                  Prescription is required.
            </Text>

                <TouchableOpacity
                  activeOpacity={0.75}
                  onPress={() => {
                    this.setState({ uploadPrescriptionVisibility: true });
                  }}
                  style={{
                    height: 18,
                    backgroundColor: Colors.primaryMedium,
                    borderRadius: 9,
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingHorizontal: 12,
                    elevation: 3,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Nunito-SemiBold',
                      fontSize: 10,
                      color: Colors.white,
                      marginRight: 4,
                    }}>
                    Add
              </Text>
                </TouchableOpacity>


              </View>
                  )*/}
          <View style={{marginHorizontal: 16, marginTop: 16}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontFamily: 'Nunito-ExtraBold',
                  fontSize: 18,
                  color: Colors.black,
                }}>
                Shipping Details
              </Text>
              <TouchableOpacity
                activeOpacity={0.75}
                onPress={() => {
                  NavigationService.navigate('LocationList');
                }}>
                <Text
                  style={{
                    fontFamily: 'Nunito-Bold',
                    fontSize: 12,
                    color: Colors.grayMedium,
                  }}>
                  Change
                </Text>
              </TouchableOpacity>
            </View>

            {this.props.userSelectedLocation == '' ? (
              <View
                style={{
                  flex: 1,
                  marginTop: 4,
                }}>
                <Text
                  style={{
                    fontFamily: 'Nunito-SemiBold',
                    fontSize: 12,
                    color: Colors.black,
                  }}>
                  No shipping location found.
                </Text>
              </View>
            ) : (
              <View
                style={{
                  marginTop: 8,
                  backgroundColor: Colors.white,
                }}>
                <View style={{flex: 1}}>
                  <Text
                    numberOfLines={1}
                    style={{
                      fontFamily: 'Nunito-Bold',
                      fontSize: 12,
                      color: Colors.primaryMedium,
                    }}>
                    {this.props.userSelectedLocation.title}
                  </Text>

                  <Text
                    numberOfLines={1}
                    style={{
                      fontFamily: 'Nunito-Bold',
                      fontSize: 12,
                      color: Colors.black,
                      marginTop: 4,
                    }}>
                    {this.props.userSelectedLocation.locationName}
                  </Text>
                </View>
              </View>
            )}
          </View>

          {/*   <View
          style={{
            height: 0.5,
            backgroundColor: Colors.border,
            marginVertical: 8,
          }}
        />

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <Text
            numberOfLines={2}
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 12,
              color: Colors.grayMedium,
            }}>
            Sub Total
          </Text>
          <Text
            numberOfLines={2}
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 12,
              color: Colors.grayMedium,
            }}>
            Rs 240
          </Text>
        </View>

        <View
          style={{
            height: 0.5,
            backgroundColor: Colors.border,
            marginVertical: 8,
          }}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <Text
            numberOfLines={2}
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 12,
              color: Colors.grayMedium,
            }}>
            Price Discount
          </Text>
          <Text
            numberOfLines={2}
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 12,
              color: Colors.grayMedium,
            }}>
            Rs 240
          </Text>
        </View>

        <View
          style={{
            height: 0.5,
            backgroundColor: Colors.border,
            marginVertical: 8,
          }}
        />

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <Text
            numberOfLines={2}
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 12,
              color: Colors.grayMedium,
            }}>
            Delivery Charge
          </Text>
          <Text
            numberOfLines={2}
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 12,
              color: Colors.grayMedium,
            }}>
            Rs 240
          </Text>
        </View>

        <View
          style={{
            height: 0.5,
            backgroundColor: Colors.border,
            marginVertical: 8,
          }}
        />

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <Text
            numberOfLines={2}
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 14,
              color: Colors.black,
            }}>
            Total
          </Text>
          <Text
            numberOfLines={2}
            style={{
              fontFamily: 'Nunito-Bold',
              fontSize: 14,
              color: Colors.black,
            }}>
            Rs 430
          </Text>
        </View>

        <View
          style={{
            height: 0.5,
            backgroundColor: Colors.border,
            marginVertical: 8,
          }}
        />*/}

          <View
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              bottom: 20,
              alignItems: 'center',
            }}>
            <CustomButton.PrimaryButton
              title="Checkout"
              onPress={() => {
                if (this.props.cartList.length == 0) {
                  AlertToast('No any items on the cart.', 150);
                } else {
                  if (this.props.userSelectedLocation == '') {
                    AlertToast('No any shipping location.', 150);
                  } else {
                    this.props.savePaynow(
                      this.props.userSelectedLocation,
                      this.props.cartList,
                    );
                  }
                }
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    cartList: state.cart.cartList,
    loading: state.cart.loading,
    userSelectedLocation: state.addLocationmap.userSelectedLocation,
  };
}

export default connect(
  mapStateToProps,
  {removeFromCart, getCartData, savePaynow},
)(Cart);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  logo: {
    width: 110,
  },
});
