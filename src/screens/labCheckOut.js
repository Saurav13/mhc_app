import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import NavigationService from '../route/navigationService';

import Images from '../res/images';
import Icon from '../res/icons';
import Colors from '../res/colors';
import CustomTextInput from '../components/textInput';
import CustomButton from '../components/button';

const Header = () => {
  return (
    <View
      style={{
        backgroundColor: Colors.primaryMedium,
        height: 110,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingBottom: 20,
      }}>
      <TouchableOpacity
        activeOpacity={0.75}
        onPress={() => {
          NavigationService.back();
        }}
        style={{padding: 16}}>
        <Icon.Ionicons
          name="md-arrow-round-back"
          size={26}
          color={Colors.white}
        />
      </TouchableOpacity>

      <Text
        numberOfLines={2}
        style={{
          fontFamily: 'Nunito-SemiBold',
          fontSize: 20,
          color: Colors.white,
        }}>
        Checkout
      </Text>
      <View style={{width: 42, height: 42}} />
    </View>
  );
};

class LabCheckOut extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: 'Kamlesh Kunwar',
      date: '04/07/1996',
      email: 'kamleshKumar@gmail.com',
      contactNo: '9841123465',
      address: 'Sukedhara, Kathmandu ',
      gender: 'Male',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Header />
        <View
          style={{
            flex: 1,
            backgroundColor: Colors.white,
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            marginTop: -20,
            paddingHorizontal: 24,
            paddingTop: 35,
          }}>
          <ScrollView
            keyboardShouldPersistTaps="handled"
            showsVerticalScrollIndicator={false}>
            <Text style={{fontFamily: 'Nunito-ExtraBold', fontSize: 18}}>
              Tests Details
            </Text>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingBottom: 4,
                borderBottomColor: Colors.grayLight,
                borderBottomWidth: 1,
                marginTop: 10,
              }}>
              <Text style={{fontFamily: 'Nunito-Bold', fontSize: 12}}>
                Product Name
              </Text>
              <Text style={{fontFamily: 'Nunito-Bold', fontSize: 12}}>
                Unit Price
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingBottom: 4,
                borderBottomColor: Colors.grayLight,
                borderBottomWidth: 1,
                marginTop: 10,
              }}>
              <Text
                style={{
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 11,
                  color: Colors.grayDarkest,
                }}>
                Total Cholestrols Test
              </Text>
              <Text
                style={{
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 11,
                  color: Colors.grayDarkest,
                }}>
                Rs 300
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingBottom: 4,
                borderBottomColor: Colors.grayLight,
                borderBottomWidth: 1,
                marginTop: 10,
              }}>
              <Text
                style={{
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 11,
                  color: Colors.grayDarkest,
                }}>
                Fast Blood Sugar Test
              </Text>
              <Text
                style={{
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 11,
                  color: Colors.grayDarkest,
                }}>
                Rs 322
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingBottom: 4,
                borderBottomColor: Colors.grayLight,
                borderBottomWidth: 1,
                marginTop: 10,
              }}>
              <Text
                style={{
                  fontFamily: 'Bold',
                  fontSize: 15,
                }}>
                Total
              </Text>
              <Text
                style={{
                  fontFamily: 'Bold',
                  fontSize: 15,
                }}>
                Rs 622
              </Text>
            </View>
            <Text
              style={{
                fontFamily: 'Nunito-ExtraBold',
                fontSize: 18,
                marginTop: 27,
              }}>
              Patient Details
            </Text>
            <CustomTextInput.AnimatedTextInput
              label="Full Name"
              value={this.state.fullName}
              onChangeText={val => this.setState({fullName: val})}
              keyboardType="default"
            />
            <CustomTextInput.AnimatedTextInput
              label="Date"
              value={this.state.date}
              onChangeText={val => this.setState({date: val})}
              keyboardType="numeric"
            />
            <CustomTextInput.AnimatedTextInput
              label="Gender (Modal dropdown ho milauna baki xa)"
              value={this.state.gender}
              onChangeText={val => this.setState({gender: val})}
              keyboardType="default"
            />
            <Text
              style={{
                fontFamily: 'Nunito-ExtraBold',
                fontSize: 18,
                marginTop: 27,
              }}>
              User Details
            </Text>
            <CustomTextInput.AnimatedTextInput
              label="Email"
              value={this.state.email}
              onChangeText={val => this.setState({email: val})}
              keyboardType="email-address"
            />
            <CustomTextInput.AnimatedTextInput
              label="Contact No."
              value={this.state.contactNo}
              onChangeText={val => this.setState({contactNo: val})}
              keyboardType="numeric"
            />

            <Text
              style={{
                fontFamily: 'Nunito-ExtraBold',
                fontSize: 18,
                marginTop: 33,
              }}>
              Service Address
            </Text>
            <CustomTextInput.AnimatedTextInput
              label="Address"
              value={this.state.address}
              onChangeText={val => this.setState({address: val})}
              keyboardType="default"
            />
            <CustomTextInput.AnimatedTextInput
              label="Some Notes"
              value={this.state.search}
              onChangeText={val => this.setState({search: val})}
              keyboardType="default"
              style={{overflow: 'scroll'}}
            />
            <Text
              style={{
                fontFamily: 'Nunito-Regular',
                fontSize: 10,
                color: Colors.grayDarkest,
                marginTop: 15,
              }}>
              * When Is Your Preffered Time To Collect Samples.
            </Text>
            <View
              style={{marginTop: 60, marginBottom: 35, alignItems: 'center'}}>
              <CustomButton.PrimaryButton
                title="Confirm Order"
                onPress={() => {
                  NavigationService.navigate('Checkout');
                }}
              />
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default LabCheckOut;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  logo: {
    width: 110,
  },
});
