import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import NavigationService from '../route/navigationService';
import CustomButton from '../components/button';
import Header from '../components/headerDashboard';
import Images from '../res/images';
import Icon from '../res/icons';
import Colors from '../res/colors';

const searchResultData = [
  {
    name: 'Suraj Vendar',
    address: 'Lainchour, Kathamndu',
    phoneNo: '01-3412323',
    price: 350,
  },
  {
    name: 'Suraj Vendar',
    address: 'Lainchour, Kathamndu',
    phoneNo: '01-3412323',
    price: 350,
  },
  {
    name: 'Suraj Vendar',
    address: 'Lainchour, Kathamndu',
    phoneNo: '01-3412323',
    price: 350,
  },
  {
    name: 'Suraj Vendar',
    address: 'Lainchour, Kathamndu',
    phoneNo: '01-3412323',
    price: 350,
  },
];
const SearchResult = ({item}) => {
  return (
    <View
      style={{
        marginTop: 20,
        marginLeft: 15,
        paddingTop: 12,
        paddingBottom: 17,
        paddingHorizontal: 16,
        backgroundColor: Colors.white,
        elevation: 3,
        width: global.screenWidth / 2 - 30,
        alignItems: 'flex-start',
        borderRadius: 15,
      }}>
      <Text
        style={{
          fontFamily: 'Nunito-SemiBold',
          fontSize: 13,
          marginBottom: 5,
        }}>
        {item.name}
      </Text>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Icon.MaterialIcons
          name="location-on"
          size={8}
          color={Colors.grayDarkest}
        />
        <Text
          style={{
            color: Colors.grayDarkest,
            fontFamily: 'Nunito-SemiBold',
            fontSize: 10,
            marginLeft: 5,
            marginBottom: 7,
          }}>
          {item.address}
        </Text>
      </View>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Icon.FontAwesome
          name="phone-square"
          size={8}
          color={Colors.grayDarkest}
        />
        <Text
          style={{
            color: Colors.grayDarkest,
            fontFamily: 'Nunito-SemiBold',
            fontSize: 9,
            marginLeft: 5,
          }}>
          {item.phoneNo}
        </Text>
      </View>
      <Text
        style={{
          color: Colors.primaryMedium,
          fontFamily: 'Nunito-Bold',
          fontSize: 16,
          marginVertical: 15,
        }}>
        Rs. {item.price}
      </Text>
      <CustomButton.SecondaryButton
        style={{
          height: 25,
          backgroundColor: Colors.primaryMedium,
          paddingHorizontal: 25,
        }}
        textStyle={{
          fontSize: 8,
          fontFamily: 'Nunito-Regular',
          color: Colors.white,
          paddingBottom: 0,
        }}
        title="Book Now"
        onPress={() => {
          NavigationService.navigate('LabCart');
        }}
      />
      {/* <TouchableOpacity
        onPress={() => {}}
        activeOpacity={0.7}
        style={{
          height: 25,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: Colors.primaryMedium,
          borderRadius: 16,
          paddingHorizontal: 25,
        }}>
        <Text
          style={{
            color: Colors.white,
            fontFamily: 'Nunito-Regular',
            fontSize: 8,
          }}>
          Book Now
        </Text>
      </TouchableOpacity> */}
    </View>
  );
};
const SearchButton = ({title, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.7}
      style={styles.searchButtonStyle}>
      <Text
        style={{
          color: Colors.white,
          fontFamily: 'Nunito-Regular',
          fontSize: 11,
          marginRight: 7,
        }}>
        {title}
      </Text>
      <TouchableOpacity activeOpacity={0.7} onPress={() => {}}>
        <Icon.Entypo name="cross" size={11} color={Colors.white} />
      </TouchableOpacity>
    </TouchableOpacity>
  );
};

class LabResult extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Header />
        <View
          style={{
            flex: 1,
            marginTop: -20,
            borderRadius: 25,
            width: global.screenWidth,
            backgroundColor: Colors.white,
          }}>
          <View style={{marginHorizontal: 25}}>
            <Text
              style={[
                styles.subHeadingText,
                {marginTop: 25, marginBottom: 14},
              ]}>
              Showing Results For :
            </Text>

            <View
              style={{
                flexDirection: 'row',
                paddingBottom: 11,
                borderBottomWidth: 1,
                borderBottomColor: Colors.grayDarkest,
              }}>
              <SearchButton title="Total Cholestrol Test" onPress={() => {}} />
              <SearchButton title="Total Cholestrol Test" onPress={() => {}} />
            </View>
          </View>
          <FlatList
            ListHeaderComponent={this.flatListHeader}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              marginHorizontal: 9,
              paddingBottom: 8,
            }}
            numColumns={2}
            data={searchResultData}
            renderItem={({item, index}) => {
              return <SearchResult item={item} />;
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    );
  }
}
export default LabResult;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  logo: {
    width: 110,
  },
  subHeadingText: {
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 18,
  },
  searchButtonStyle: {
    height: 26,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.primaryMedium,
    borderRadius: 16,
    paddingHorizontal: 12,
    paddingVertical: 6,
    marginRight: 11,
  },
});
