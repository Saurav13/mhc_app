import React from 'react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import reducers from './src/reducers';
import thunk from 'redux-thunk';
import {StatusBar, View, Platform, Dimensions, YellowBox} from 'react-native';

import DashboardRoute from './src/route/dashboardRoute';
import InternetConnectionCheck from './src/components/internetConnectionCheck';
import NavigationService from './src/route/navigationService';

import Colors from './src/res/colors';

class app extends React.Component {
  constructor(props) {
    super(props);

    if (Platform.OS === 'android') {
      global.OS = 1;
    } else {
      global.OS = 2;
    }

    global.host = 'https://www.merohealthcare.com/api';
    global.imageUri = 'https://www.merohealthcare.com/assets/images/';
    global.appVersion = '0.0.1 (1)';
    global.screenWidth = Dimensions.get('window').width;
    global.screenHeight = Dimensions.get('window').height;
    YellowBox.ignoreWarnings([
      'VirtualizedLists should never be nested inside plain ScrollViews with the same orientation - use another VirtualizedList-backed container instead.',
      'CheckBox has been extracted from react-native core and will be removed in a future release',
    ]);
  }

  render() {
    const store = createStore(reducers, applyMiddleware(thunk));

    return (
      <Provider store={store}>

      <StatusBar
        barStyle="light-content"
        translucent={true}
        backgroundColor="transparent"
      />

        <InternetConnectionCheck />

        <DashboardRoute
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </Provider>
    );
  }
}

export default app;
